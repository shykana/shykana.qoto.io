==================================================================
https://keybase.io/shykana
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://gudzpoz.gitlab.io
  * I am shykana (https://keybase.io/shykana) on keybase.
  * I have a public key ASC_1ChTHfgjjVEXi7ThfzqxWk3hTl-SYcRDsY_GFLLkDQo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120c41ab5e706a4409ff35b6e3c2a465b9383a77aa783cb7ea2c4c18a629e934ea90a",
      "host": "keybase.io",
      "kid": "0120bfd428531df8238d51178bb4e17f3ab15a4de14e5f9261c443b18fc614b2e40d0a",
      "uid": "e871d493f82ca070c95f814fc6ef4819",
      "username": "shykana"
    },
    "merkle_root": {
      "ctime": 1597793465,
      "hash": "90fa8005c17fc477b1e6bad9c126bee684038a89700fdbb8329a19cc0955a2dfecc53f7ddc17590e4497bbea062143d636f81b8b790b7f0751228cd1545987a6",
      "hash_meta": "0f3a92b01b76669693e7efa7c58e74ea2c209e87f4d4113e304e83e1c12b19aa",
      "seqno": 17317455
    },
    "service": {
      "entropy": "0i99qePMARjAdbDpaNOu5Hkk",
      "hostname": "gudzpoz.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.5.0"
  },
  "ctime": 1597793489,
  "expire_in": 504576000,
  "prev": "36a1fe2ed17aaaf07ce18ef1be67a5f472d184777866b217dc3c8a7394f76939",
  "seqno": 16,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgv9QoUx34I41RF4u04X86sVpN4U5fkmHEQ7GPxhSy5A0Kp3BheWxvYWTESpcCEMQgNqH+LtF6qvB84Y7xvmel9HLRhHd4ZrIX3DyKc5T3aTnEIPGUjvVYAX0omzIu/mojWk3QP2O0A1hpg/gGkg9w29H/AgHCo3NpZ8RAMoedgqvQtfK4JUaKsvKFPmiWvATryLrfdRaMcCYTyCMluxCTsb1CB4I1WO5goKmaokj0P1tt+6x8GILnAoeaB6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIMrXWLX1hrkL/RbXdXFF/v0Tbsg277fk2iB5p29gJW9bo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/shykana

==================================================================
