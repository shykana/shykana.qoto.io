(function() {
    var initialized = false;
    const attributes = {
        "src": "https://utteranc.es/client.js",
        "repo": "GuDzpoz/gudzpoz.github.io",
        "issue-term": "pathname",
        "label": "comment",
        "theme": "github-light",
        "crossorigin": "anonymous",
        "async": "true"
    };
    var listener = function() {
        var parent = document.getElementById("comment");
        if(parent && !initialized) {
            initialized = true;
            var script = document.createElement("script");
            for(var i in attributes) {
                script.setAttribute(i, attributes[i]);
            }
            parent.appendChild(script);
        }
    };
    window.addEventListener("load", listener);
    listener();
})();
