---
title: 'About: Yet Another Hexo Site'
---
# 图片来源、版权信息
## 主页背景图：[Hubble's Festive View of a Grand Star-Forming Region](https://hubblesite.org/contents/media/images/2009/32/2649-Image.html)
![Hubble's Festive View of a Grand Star-Forming Region](https://ae01.alicdn.com/kf/U40655ebed98d435e9d67921681d65d98q.jpg)

## 侧边栏、搜索栏背景图：
侧边栏与搜索栏的背景是主题自带的。搜了一下大概是来自于《星宿计时》的PV，但是图片文件的具体来源不清楚。
[【洛天依原创】星宿计时【杉田朗】-哔哩哔哩](https://www.bilibili.com/video/av7036967)
[星宿计时-萌娘百科](https://zh.moegirl.org.cn/星宿计时)
### 侧边栏
![stars-timing-1](https://cdn.jsdelivr.net/gh/YunYouJun/cdn/img/bg/stars-timing-1.jpg)
### 搜索栏
![stars-timing-2](https://cdn.jsdelivr.net/gh/YunYouJun/cdn/img/bg/stars-timing-2.jpg)

