# -*- coding: utf-8 -*-

# Copyright © 2014 Roberto Alsina and others.

# Permission is hereby granted, free of charge, to any
# person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the
# Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the
# Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice
# shall be included in all copies or substantial portions of
# the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
# OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from docutils import nodes
from docutils.parsers.rst import roles
from nikola.utils import LOGGER
from nikola.plugin_categories import RestExtension
import re

def ruby_role(name, rawtext, text, lineno, inliner,
              options={}, content=[]):
    html = ''
    rt = False
    def get_text(piece, rt):
        if rt:
            return '<rp>(</rp><rt>' + piece + '</rt><rp>)</rp>'
        else:
            return piece
    for piece in re.findall(r"(?:[^ ]|(?:  ))+", text):
        html += get_text(piece.replace('  ', ' '), rt)
        rt = not rt
    return [nodes.raw('', '<ruby>' + html + '</ruby>', format='html')], []

ruby_role.options = {'class': None}
ruby_role.content = True

def kbd_role(name, rawtext, text, lineno, inliner,
              options={}, content=[]):
    return [nodes.raw('', '<kbd>' + text + '</kbd>', format='html')], []

kbd_role.options = {'class': None}
kbd_role.content = True

def del_role(name, rawtext, text, lineno, inliner,
              options={}, content=[]):
    return [nodes.raw('', '<del>' + text + '</del>', format='html')], []

kbd_role.options = {'class': None}
kbd_role.content = True

class Plugin(RestExtension):
    """Plugin for reST ruby role."""

    name = "rest_ruby"

    def set_site(self, site):
        """Set Nikola site."""
        self.site = site
        roles.register_local_role('ruby', ruby_role)
        roles.register_local_role('kbd', kbd_role)
        roles.register_local_role('del', del_role)
        return super().set_site(site)    
