$(document).ready(function() {
  var element = $('<label for="sidebarBox"><input id="sidebarBox" type="checkbox"></input>Sidebar</label>')[0]
  $('header .metadata')[0].appendChild(element)
  var checkbox = $('#sidebarBox')[0]
  var sidebar = $('#pageSide')[0]
  var main = $('#pageMain')[0]
  var url = new URL(window.location)
  if(url.searchParams.has('hide')) {
    checkbox.checked = false
  } else {
    checkbox.checked = true
  }
  var update = function() {
    if(checkbox.checked) {
      sidebar.classList.remove('hidden')
      main.classList.remove('sideHidden')
      url.searchParams.delete('hide')
    } else {
      sidebar.classList.add('hidden')
      main.classList.add('sideHidden')
      url.searchParams.set('hide', '')
    }
    window.history.replaceState('', document.title, url.href)
  }
  checkbox.onchange = update
  update()
})
