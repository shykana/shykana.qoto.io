$(document).ready(function() {
  var container = document.getElementById('search-results');
  container.classList.add('hidden');
  var search = document.getElementById('tipue_search_input');
  search.addEventListener('input', () => {
    if(search.value === '') {
      container.classList.add('hidden');
    } else {
      container.classList.remove('hidden');
    }
  });
  $(search).tipuesearch({
    'wholeWords': false,
    'showTitleCount': false,
    'minimumLength': 1,
    'imageZoom': false,
    'footerPages': 5,
  });

  var icon = document.getElementById('searchIcon');
  var close = document.getElementById('searchClose');
  icon.addEventListener('click', () => {
    if(search.classList.contains('hidden')) {
      search.classList.remove('hidden');
    } else {
      search.dispatchEvent(new KeyboardEvent('keyup', {keyCode: 13}));
    }
  });
  close.addEventListener('click', () => {
    search.value = '';
    search.classList.add('hidden');
    container.classList.add('hidden');
  });
});
