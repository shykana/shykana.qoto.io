// Preview

var summaries = document.querySelectorAll('.p-summary.entry-summary');
for(var i of summaries) {
  i.classList.add('hidden');
  var a = document.createElement('a');
  a.classList.add('preview-icon');
  a.classList.add('preview');
  a.addEventListener('click', ((link, element) => {
    return () => {
      if(link.classList.contains('preview')) {
        link.classList.remove('preview');
        element.classList.add('pre-hidden');
        element.classList.remove('hidden');
        setTimeout(() => { element.classList.remove('pre-hidden'); }, 500);
      } else {
        link.classList.add('preview');
        element.classList.add('pre-hidden');
        setTimeout(() => {
          element.classList.add('hidden');
          element.classList.remove('pre-hidden');
        }, 50);
      }
    };
  })(a, i));
  i.parentElement.insertBefore(a, i);
}
