// TOC

function countLevels(node) {
  var tree = [];
  while(node !== null && node.tagName.toUpperCase() !== 'ARTICLE') {
    if(node.tagName.toUpperCase() === 'LI') {
      var link = node.querySelector('a');
      if(link) {
        tree.unshift(link.innerText);
      }
    }
    node = node.parentElement;
  }
  return tree;
}

var tocs = document.getElementsByClassName('toc');
var hierarchy = [];
var lastPosition = null;
if(tocs.length === 1) {
  var toc = tocs[0];
  toc.id = 'toc';
  var locator = document.createElement('span');
  locator.innerHTML = '';
  locator.classList.add('locator');
  toc.parentElement.prepend(locator);
  var linkToToc = document.createElement('a');
  linkToToc.innerText = 'Table of Contents';
  linkToToc.href = '#toc';
  locator.appendChild(linkToToc);
  var link = document.createElement('a');
  locator.appendChild(link);
  const tocOut = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      if(entry.intersectionRatio === 0) {
        locator.classList.add('visible');
      } else {
        locator.classList.remove('visible');
      }
    });
  });
  tocOut.observe(toc);

  function updateSpan(tocLink) {
    lastPosition = tocLink;
    link.innerText = hierarchy.join(' > ');
    link.href = tocLink.href;
    window.history.replaceState(null, tocLink.innerText, tocLink.href);
  }

  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
	  const id = entry.target.getAttribute('id');
      var link = document.querySelector(`ul li a[href="#${id}"]`);
      if(link) {
	    if (entry.intersectionRatio > 0) {
	      link.parentElement.classList.add('active');
          hierarchy = countLevels(link);
          updateSpan(link);
	    } else {
	      link.parentElement.classList.remove('active');
	    }
      }
    });
  });

  // Track all sections that have an `id` applied
  document.querySelectorAll('h1[id],h2[id],h3[id],h4[id],h5[id],h6[id]').forEach((section) => {
    observer.observe(section);
  });
}
