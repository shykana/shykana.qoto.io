---
title: ActivityPub 协议笔记（又一新坑）
category: 笔记
date: 2020-10-28 23:54:01
updated: 2020-10-28 23:54:01
tags:
---

[TOC]

# 词汇 Activity Vocabulary

| 英文 | 本文翻译 |
| ---- | ---- |
| vocabulary | 词汇 |
| activity | 活动 |

> 原文见：[activitystreams-vocabulary/](https://www.w3.org/TR/activitystreams-vocabulary/)

Activity Streams 2.0 词汇有两部分定义：

1. 描述一个 Activity 的普遍结构的一组*核心*性质；
2. 描述 Activity 的特定类型以及常见 Artifact 的*扩展*的一组性质；

实现必须至少支持 [Activity Streams 2.0 Core Syntax](https://www.w3.org/TR/activitystreams-core/) 里的扩展性质集。

## 核心类型 Core Types

| 类型 | 说明 | 属性 |
| --- | --- | --- |
|`Object`|基本类型，与 `Link` 属并列关系|`attachment | attributedTo | audience | content | context | name | endTime | generator | icon | image | inReplyTo | location | preview | published | replies | startTime | summary | tag | updated | url | to | bto | cc | bcc | mediaType | duration`|
|`Link`|基本类型，与 `Object` 属并列关系|`href | rel | mediaType | name | hreflang | height | width | preview`|
|`Activity`|扩展 `Object`|新增 `actor | object | target | result | origin | instrument`|
|`IntransitiveActivity`|扩展 `Activity`|除去了 `object` 属性|
|`Collection`|扩展 `Object`|新增 `totalItems | current | first | last | items`|
|`OrderedCollection`|扩展 `Collection`|将 `items` 属性换为了 `orderedItems`|
|`CollectionPage`|扩展 `Collection`|新增 `partOf | next | prev`|
|`OrderedCollectionPage`|扩展 `OrderedCollection | CollectionPage`|新增 `startIndex`|

Json 格式：
```
{
    "@context": "https://www.w3.org/ns/activitystreams",
    "type": "<类型>",
    <类型对应的属性>...
}
```

## 扩展类型 Extended Types

### Activity 类型

* `Activity`
  * `Accept`
    * `TentativeAccept` 
  * `Add`
  * `Create`
  * `Delete`
  * `Follow`
  * `Ignore`
    * `Block`
  * `Join`
  * `Leave`
  * `Like`
  * `Offer`
    * `Invite`
  * `Reject`
    * `TentativeReject`
  * `Remove`
  * `Undo`
  * `Update`
  * `View`
  * `Listen`
  * `Read`
  * `Move`
  * `Announce`
  * `Flag`
  * `Dislike`

* `IntransitiveActivity`
  * `Arrive`
  * `Travel`
  * `Question`: 增加了 `oneOf | anyOf | closed` 属性

### Actor 类型

Actor 类型是能够执行 Activity 的 Object 类型，包括：

* `Application`
* `Group`
* `Organization`
* `Person`
* `Service`

### Object 以及 Link 类型

Object 类型有：

* `Ariticle`
* `Document`
  * `Audio`
  * `Image`
  * `Video`
  * `Page`
* `Event`
* `Note`
* `Place`: 新增属性 `accuracy | altitude | latitude | longitude | radius | units`
* `Profile`: 新增属性 `describes`
* `Relationship`: 新增属性 `subject | object | relationship`
* `Tombstone`: 新增属性 `formerType | deleted`

Link 类型有：

* `Mention`

## 属性汇总

属性非常之多。请参见 [activitystreams-vocabulary/#properties](https://www.w3.org/TR/activitystreams-vocabulary/#properties)。

# ActivityPub

就我理解，ActivityPub 是在上述 ActivityStreams 格式的基础上扩展而来的一种服务器与服务器（S2S）以及客户端与服务器（C2S）的通信协议。

每一个对象都需要具有 `id` 以及 `type` 属性。

用户使用 `Actor` 表示，需要有 `inbox` 以及 `outbox` 属性。
其功能如图。（来自 [W3C](https://www.w3.org/TR/activitypub) Copyright © 2018 W3C® (MIT, ERCIM, Keio, Beihang). W3C liability, trademark and [permissive document license](https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document) rules apply. ）

![inbox outbox usage with GET and POST requests](/images/activitypub-tut-2.png)

发给对方 inbox 的对象都应该是 Activity（如 `Create` `Like` 等）。

## 扩展的属性汇总

### Object

- `id` : 唯一标识符，应该是一个链接。该链接应当可以被访问，并返回对应的对象。
- `source` : 反映了 `Note` 的 `content` 的来源（如 Markdown 源码）。`source` 应为这样的对象 ``{"content": "I *really* hate JSON-LD.", "mediaType": "text/markdown"}``

### Actor

- `inbox`
- `outbox`
- `type` : 类型并不一定要是 `Person` `Service` 等在 ActivityStreams 里的类型，也可以是 `Profile` 或其它扩展类型
- `following` : SHOULD
- `followers` : SHOULD
- `liked` : MAY : 用户 `Like` 过的对象 `Collection`
- `streams` : MAY : 一系列其它 `Collection` 的 `Collection`
- `preferredUsername` : MAY
- `endpoints` : MAY : 可以有以下属性
  - proxyUrl
  - oauthAuthorizationEndpoint
  - oauthTokenEndpoint
  - provideClientKey
  - signClientKey
  - sharedInbox
- `url` : 被视为该 actor 的个人资料网页链接，不一定与 `id` 相同
- `name` : 显示名称
- `summary` : 个人简介
- `icon` : 头像

一些具有自然语言的值的属性 `name` `summary` 可能需要考虑 [natural language support defined in ActivityStreams](https://www.w3.org/TR/activitystreams-core/#naturalLanguageValues) （天哪这什么噩梦。）

### Collection

以下可以是 Collection 也可以是 OrderedCollection

- followers : SHOULD
- following : SHOULD
- liked : MAY : 某用户给出的喜欢
- likes : MAY : 可以是任何 object 受到的喜欢，例如用户，例如嘟文（原则上应该也可以给喜欢点喜欢）
- shares : MAY : 没看懂

#### OrderedCollection

必须以时间倒序（从新到旧）排序。

- outbox : MUST
- inbox : MUST

### “公开”的可见范围

ActivityStreams 只能给某些特定的人发消息。ActivityPub 扩展了一下，当你指定的“人”是 `https://www.w3.org/ns/activitystreams#Public` 的时候，消息的可见范围是公开的。（似乎根据 `cc` 和 `to` 有 PUBLIC 和 UNLISTED 的差别。）

## S2S 交互

Create, Update, Delete, Follow, Add, Remove, Like, Block, Undo 这些 activity 必须带有 object 属性，而 Add, Remove 这两个必须带有 target 属性。

请求的 HTTP 头需要相应设置 `Accept` 或 `Content-Type` 为 ``application/ld+json; profile="https://www.w3.org/ns/activitystreams"`` 或 ``application/activity+json`` 。

POST 请求的对象列表由 to, cc, bto, bcc, audience 属性决定。最终的对象列表必须去重，并去除作者（如果作者错误地进入了自己作品的受众列表的话）。

### 转递（Forwarding）机制

（诶呀看起来 Mastodon（以及 Pleroma 以及 Misskey？）都没有弄这个？）

目的是让用户不关注对话中所有的用户也能接收到（几乎）完整的对话。

当且仅当以下情况时转递：

- 第一次看到这个 activity
- to, cc, audience 里包含本服务器拥有的 collection
- inReplyTo, object, target, tag 是本服务器拥有的对象

### SharedInbox 投递

如果有很多用户来自与同一个服务器，并且这个服务器有着 sharedInbox 的话，在投递是可以直接投到 sharedInbox 里，而不用一个一个用户投 inbox。

同时，如果有 Public 的消息想要让更多服务器知道的话，也可以给已知的所有 sharedInbox 都投递一份。

## 各种 activity 的处理

### Create / Delete

收到的 activity 应在 inbox 里出现，可能还可以本地储存一份备份。

Delete 就反过来啦。

### Update

应将相同 id 的 object 更换为本 activity 的那个 object。

### Delete

删删删。可以换成 Tombstone。

### Follow

应该返回 Accept 或者 Reject。从收到到返回可以有时间间隔（等待用户处理），也可以直接永远不返回（隐形 Reject）。

Follow 完之后，如果一个关注者的 inbox 很久都不可达，那么服务器可以考虑把 TA 从 followers 中移除。

### Accept / Reject

object 一般只考虑 Follow activity，对应处理即可。其实也可以是其它 object，标准没定死。

### Add / Remove

是对 target 属性对应的 collection 的增加/移除操作，除非对应 collection 不属于本服务器或 object 不被允许添加。

（这里没有说谁添加，要验证权限的话可能可以要求 object 和 Signature 的 key owner 是同一个？）
 
### Like / Dislike

喜欢～有用属性是 object。

### Announce

就是转发啦。转发 object。

### Undo

取消某个 activity。
