---
title: 测试
tags:
  - 图像
  - 生活
id: '137'
date: 2020-04-03 16:21:15
updated: 2020-04-03 16:21:15
---

\[caption id="attachment\_144" align="alignnone" width="251"\]![](/images/754349main_butterfly_nebula_full_full-251x300.jpg) 图源：https://www.nasa.gov/multimedia/imagegallery/image\_feature\_2526.html\[/caption\] \[caption id="attachment\_143" align="alignnone" width="300"\]![](/images/butterflyblue_hst_3919-300x287.png) 图源：https://apod.nasa.gov/apod/ap141001.html\[/caption\] \[caption id="attachment\_142" align="alignnone" width="300"\]![](/images/Braid-300x177.png) Braid\[/caption\] \[caption id="attachment\_140" align="alignnone" width="300"\]![](/images/turn-a-300x188.jpg) ∀\[/caption\] \[caption id="attachment\_139" align="alignnone" width="300"\]![](/images/00-earth-300x169.jpg) 00\[/caption\] \[caption id="attachment\_138" align="alignnone" width="474"\]![](/images/00-986x1024.jpg) 00\[/caption\]