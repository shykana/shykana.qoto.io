.. title: Pleroma 从入门到精通（误）
.. slug: pleroma-tutorial-draft
.. date: 2021-01-03 11:11:25 UTC+08:00
.. tags: Pleroma, wiki
.. category: 服务器
.. link: 
.. description: 
.. type: text

现在暂时在这里放一个写作大纲::

    所以这算是什么？
    Pleroma 从安装到管理迁移
    划掉
    Linux 从入门到后端运维
    划掉
    计算机网络从入门到精通
    划掉
    操作系统原理
    划掉
    计算机文化基础及硬件组成
    划掉
    ……
    总结：
    Pleroma 教程从大纲到放弃

因为实在是太多内容了，打算建一个 wiki 来分页面放：

`现在维基页面搭在这里。`_ （是的，我买了新域名啦！但是因为想把新域名用到更公共的内容上，所以还是把这个博客赖在 qoto.io 上。）

.. _`现在维基页面搭在这里。`: https://wiki.iroiro.party

暂定是使用 DokuWiki，配合 Markdown 插件（方便编辑）、Git 插件（数据公开存档）、RSS 插件（可以做个长毛象更新 bot？）。大家也可以来写一写？
（版权（CC-BY-SA 等等）得说好了。）

####
大纲
####

#. 计算机是什么？

   - CPU、内存、硬盘
   - VPS 是什么？

#. 网络是什么？

   - 端口是什么？
   - IP 是什么？域名又是什么？
   - SSL/TLS 是什么？证书又是什么？

#. 联邦宇宙、Mastodon、Pleroma 是什么？

#. Linux 简明教程：

   #. 各个发行版？
   #. 用户是怎么回事？

      #. 所谓的多用户系统，以及用户权限问题。

   #. 命令行基本逻辑以及语法。

      - Shell

   #. $EDITOR 已死，$EDITOR 万岁！（VIM 已死，Nano 万岁！）
   #. 服务器是什么？Nginx 是什么？为什么服务器还能套服务器还能套服务器？

      - 所谓反向代理
      - Cloudflare 是怎么回事？

        - Cloudflare 的 TLS 设置 flexible 与 Let's Encrypt

   #. Swap 呀 swapfile

#. Pleroma 安装

   #. 域名选购

      - 给个其它大佬的外链

   #. 服务器选购

      #. 对服务器配置的要求

   #. Docker 是什么？我要不要用 docker？

      - 因为本人不会（ can't ）用 docker，所以教程使用手动安装

   #. Cloudflare 是什么？

#. Pleroma 管理

   #. 配置：存在文件上还是在数据库里？
   #. 备用域名？基于 Cloudflare Workers 的代理

========
域名选购
========

.. _`BGME 的域名选购的注意事项`: https://blog.bgme.me/posts/precautions-for-registering-domains/

我是在 porkbun.com 购买域名的，因为似乎便宜一些。可以参考 `BGME 的域名选购的注意事项`_ ，引用 BGME 的总结::
  不要使用中国国内的域名注册商注册域名，也不要注册域名注册局位于中国的域名。

====================
大概率在这里不更新了
====================

大概会放到 wiki 上。
