---
title: 简单服务器搭建流程
category: 服务器
tags:
  - Linux
  - 服务器
  - 网络
  - 计算机
id: '110'
date: 2019-12-11 08:46:50
updated: 2020-08-20 08:46:50
---

哎呀最近又奢侈买了一台VPS（说来话长），总之就是又要重新搭一遍。顺便回忆一下流程吧

1.  SSH登陆进去，这时一般的VPS都只有一个root帐号，马上创建用户禁止root登陆，使用密钥。
    1.  
```
$ ssh <ip> [-p port] # 一般起始的端口就是22，不用指定
# useradd -m <用户名>
# usermod -a -G sudo <用户名>
# visudo # 看看有没有 '%sudo ALL=(ALL) ALL' 这一行，没有的话自己加上，前面有 '#' 的话去掉
# passwd <用户名> # 更改密码
```
        这时本地新开一个终端，如果没有创建过密钥：```
        $ ssh-keygen # 跟着提示走
        $ ssh-copy-id \[-p port\] \<用户名\>@\<ip\>
        如果创建过密钥，直接：
        $ ssh-copy-id \[-p port\] \<用户名\>@\<ip\>
        $ ssh <用户名>@<ip> #尝试登陆，如果登陆不了不要继续！先找到问题，否则之后就进不了SSH了
        \# *注意* ：这里应该直接进入，或者显示 `Enter passphrase for key ...`
        \# 如果显示的是 `\<username\>@\<ip\>'s password:` 那这是有问题的！可能是设置错了或者是公钥没有复制对（比如你有多个公钥的时候）！
        \# ~~这里还应该有更多说明，但是暂时懒得。可以查一下 `ssh-copy-key` 的用法。~~
        ```
        这时直接回到原来的SSH：```
        \# nano /etc/ssh/sshd\_config
        更改以下几行为：
        Port <你喜欢的端口>
        PermitRootLogin no
        PubkeyAuthentication yes
        PasswordAuthentication no
        ```
        重启SSHD```
        \# systemctl restart sshd
        ```
        Ctrl+D退出SSH，以后登陆使用一下命令：```
        $ ssh <用户名>@<ip> -p <新端口>
        ```
2.  服务器搭建：
    1.  ```
$ sudo su # 不好的习惯，不要学
        \# apt update # 更新软件，不同发行版不同
        \# apt upgrade
        \# apt install
        \# apt-get install software-properties-common
        \# add-apt-repository ppa:ondrej/php
        \# add-apt-repository ppa:ondrej/nginx-mainline
        \# apt update
        \# apt install nginx
        \# apt search php7.3 # 看看想装啥，顺便之后可能PHP版本会变
        \# apt install php7.3 php7.3-fpm php7.3-mbstring php7.3-mysql
        \# apt install mariadb-server
```        
    2.  Mariadb数据库（MySQL）：
    3.  可以先试一下：```
        \# apt install mariadb-server
        反正我是出错了，版本也不够新，所以用官方的repo。
        \# apt remove -f mariadb-server
        \# apt autoremove -f
        
        \# apt install curl
        \# curl -sS https://downloads.mariadb.com/MariaDB/mariadb\_repo\_setup | bash # 不用 root 的话 bash 前加个 sudo
        \# apt install mariadb-server
        ```
        还是出错了。。。安装停住了。。。
        ```
        Setting up mariadb-server-10.4 (1:10.4.10+maria~bionic) ...
        ```
        之后就没有动。没办法，再开一个SSH，`ps aux | grep mysql`发现有几个东西。kill -9 弄掉，至少安装结束了。
        ```
        Setting up mariadb-server-10.4 (1:10.4.10+maria~bionic) ...
        /var/lib/dpkg/info/mariadb-server-10.4.postinst: line 23:  1208 Terminated      
                bash /usr/bin/mysql\_install\_db --rpm --cross-bootstrap --user=mysql --di
        sable-log-bin --skip-test-db 2>&1
              1209 Done                    | $ERR\_LOGGER
        Created symlink /etc/systemd/system/mysql.service → /lib/systemd/system/mariadb.
        service.
        Created symlink /etc/systemd/system/mysqld.service → /lib/systemd/system/mariadb
        .service.
        Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /l
        ib/systemd/system/mariadb.service.
        Job for mariadb.service failed because the control process exited with error cod
        e.
        See "systemctl status mariadb.service" and "journalctl -xe" for details.
        Job for mariadb.service failed because the control process exited with error cod
        e.
        See "systemctl status mariadb.service" and "journalctl -xe" for details.
        Setting up mariadb-server (1:10.4.10+maria~bionic) ...
        Processing triggers for man-db (2.8.3-2ubuntu0.1) ...
        Processing triggers for libc-bin (2.27-3ubuntu1) ...
        ```
        还给出了出错命令，重新试一遍：
        ```
        \# bash /usr/bin/mysql\_install\_db --user=mysql
        \# mysql\_secure\_installation # 看看如何
        ```
        似乎可以了。

