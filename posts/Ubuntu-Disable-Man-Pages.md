---
title: Ubuntu Disable Man Pages
date: 2020-11-19 20:16:22
updated: 2020-11-19 20:16:22
category: 服务器
tags:
  - 笔记
---

（只是个簡单的筆記）

原文地址：[uninstall - Remove documentation to save hard drive space - Ask Ubuntu](https://askubuntu.com/questions/129566/remove-documentation-to-save-hard-drive-space/401144#401144)

因為 VPS 上實在不需要 man pages 等的文档，但是安裝时又費时間，所以想永遠把它關掉。摘錄如下：

```
# Create /etc/dpkg/dpkg.cfg.d/01_nodoc

path-exclude /usr/share/doc/*
# we need to keep copyright files for legal reasons
path-include /usr/share/doc/*/copyright
# if you also want to remove the man pages uncomment the next line
#path-exclude /usr/share/man/*
path-exclude /usr/share/groff/*
path-exclude /usr/share/info/*
# lintian stuff is small, but really unnecessary
path-exclude /usr/share/lintian/*
path-exclude /usr/share/linda/*
```

```
find /usr/share/doc -depth -type f ! -name copyright|xargs rm || true
find /usr/share/doc -empty|xargs rmdir || true
rm -rf /usr/share/groff/* /usr/share/info/*
rm -rf /usr/share/lintian/* /usr/share/linda/* /var/cache/man/*
# If you also want to remove the man pages do:
rm -rf /usr/share/man/*
```
