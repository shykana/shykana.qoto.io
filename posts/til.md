---
title: TIL
category: 生活
tags:
  - 生活
id: '126'
date: 2020-03-22 17:21:34
updated: 2020-08-31 01:32:34
---

![xkcd [CITATION NEEDED]](https://imgs.xkcd.com/comics/wikipedian_protester.png)图片来源：[xkcd 285](https://xkcd.com/285/) （此篇文章中如无特殊说明，全部 \[citation needed\] ）

1.  TIL=Today I Learned，翻译过来大概是学到了学到了这种感觉？ ![奇怪的只是增加了.jpg](/images/strange.jpg)
2.  秦军很厉害有部分原因是因为造剑工艺好，造出的剑更加长。但这也导致了秦王在和荆轲绕柱跑的时候拔不出剑。
3.  供暖用的水需要用化学物质（EDTA 之类的？）来除去水里的钙。然后这些除钙的物质都是对人体有害的（因为人体中的钙很重要）。有的供暖会向水里加染料或是*有味道*的物质来防止人们将供暖的水用作生活用途。
4.  有的实验室按字母表顺序排列论文作者，于是有的导师就再也不收名字比自己排得前的学生了。
5.  HTTP状态码418：I'm a teapot 服务器表示自己只是个茶壶，拒绝冲咖啡。超文本咖啡壶控制协议规定，一个咖啡和茶都能泡的服务器暂时不能泡咖啡时，应该返回503状态码。 该状态码被现代所有流行浏览器完全支持。 来源：[418 - MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/418)。
6.  Mozilla温馨提醒您：密保问题的正确填法：[原文](https://blog.mozilla.org/internetcitizen/2017/01/25/better-password-security/) ![Mother's name is Ff926AKa9j6Q](/images/secure-questions.png) 
7.  [Logo (程序语言) 中文维基百科页面](https://zh.wikipedia.org/wiki/Logo_(%E7%A8%8B%E5%BA%8F%E8%AF%AD%E8%A8%80)) 摘录：

> ... 设计Logo的初衷是为了向儿童教授计算机编程技能。

> Logo的方言NetLogo、StarLogo进一步深化了Logo的海龟绘图概念，可以产生出成百上千个独立的海龟（被称为代理）来模拟诸如物理、生物或社会等复杂系统。建模者可以对众多海龟并行发送指令，来观察微观个体行为与个体之间相互影响产生的宏观样式之间的联系。

另外，在[英文页面](https://en.wikipedia.org/wiki/Logo_(programming_language))中提到了一个方言 POOL，全称是 Parallel Object-Oriented Logo，并行的面对对象的Logo。
8.  Pleroma赞助了Mastodon：![mastodon sponsors](/images/pleroma-sponsors-masto.png)（可以类比一下假如FreeBSD赞助了Linux？）
9.  `desserts` 倒过来就是 `stressed` ！压力的对立面不是放松，吃我点心啦！

