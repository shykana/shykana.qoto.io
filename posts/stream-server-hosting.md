---
title: 直播服务器搭建笔记
date: 2020-08-05 11:03:16
updated: 2020-08-05 11:03:16
category: 服务器
tags:
---
有同学在搭直播服务器，我也顺便稍微试了一下。大概比较容易搭建的是 RTMP 推流，然后在服务器分发 RTMP 流或者转换为 HTTP-FLV 或 HLS 流。网络上基本认为，服务器分发的流的延迟大小大概是：RTMP < HTTP-FLV < HLS。
服务器端软件我了解到的有两个：
* [SRS](https://github.com/ossrs/srs)
* [nginx-http-flv-module](https://github.com/winshining/nginx-http-flv-module)

### SRS 搭建
SRS现在主要用法就是 RTMP 推流，然后根据不同配置可以实现 RTMP、HTTP-FLV 或者 HLS 的分发。似乎在 SRS 4.0 里（当前版本是 3.0 ）可以实现 WebRTC 的推流以及分发，感兴趣的也可以去试一下。

#### 非Ubuntu/CentOS系统下的编译
编译流程在官方文档里有说：[https://github.com/ossrs/srs/blob/3.0release/README.md](https://github.com/ossrs/srs/blob/3.0release/README.md)
但是在 `./configure && make` 这一步可能需要加点参数修改一下，比如（未测试）：
`./configure --cubie --use-sys-ssl && make CPPFLAGS='-fpermissive'`
我个人是直接修改 `objs/Makefile`，修改 `CXXFLAGS =  -ansi -Wall -g -O0` 这一行，变成 `CXXFLAGS =  -ansi -Wall -g -O0 -fpermissive`，命令用的是
用的是 `./configure --cubie --with-ssl --use-sys-ssl && make -j 4`。
之后和官方步骤一致。

#### OBS 推流
![OBS Stream Setting](/images/OBS-stream.png)

因为我不了解直播，所以不明白 OBS 设置里的术语。总而言之，如图上的配置：
* Service: Custom...
* Server: rtmp://127.0.0.1:1935/live
* Stream Key: livestream
最后的 RTMP 播放地址为：`rtmp://127.0.0.1:1935/live/livestream`，HTTP-FLV 地址为 `http://127.0.0.1:8080/live/livestream.flv`。
~~但是**不能**在 OBS 设置里不填 Stream Key 而把 Server 填成 `rtmp://127.0.0.1:1935/live/livestream`~~

#### 低延时配置
如果测试的延迟比较高，可以尝试用 [v2\_CN\_SampleRealtime](https://github.com/ossrs/srs/wiki/v2_CN_SampleRealtime) 里的配置优化一下延迟（也即 `conf/realtime.conf` 文件里的配置）。
我在自己电脑上跑的时候大概 RTMP -> RTMP 延迟 2s，RTMP -> HTTP-FLV 延迟 5s。

### WebRTC 服务器
WebRTC 的延迟一般认为是很低的。但是之前找的时候易搭建的解决方案挺少的。了解到大概有 Janus 以及上面提到的 SRS。
