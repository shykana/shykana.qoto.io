.. title: Blender 笔记
.. slug: blender-notes
.. date: 2021-05-22 18:42:07 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Blender 版本 2.91.2

########
基本操作
########

视角操作
########

数字小键盘
==========

数字小键盘的按键和其它按键可能有不同的功能。基本都是对视角的调整。

.. csv-table:: 大概是这个样子的

    "7", "8", "9"
    "4", "5", "6"
    "1", "2", "3"
    " ", "0", "."

- `.` : 将视角缩放、移动到所已选择的物体上
- `0` : 移动到场景里的摄像头的视角
- `1`, `3`, `7`, `9` : 分别对应左视图、正视图、俯视图、右视图；（想象把小键盘旋转至 `7` 在上方会更容易记忆）
- `5` : 切换透视投影和垂直投影
- `8`, `2`, `4`, `6` : 前后左右对视角角度微调
- `+`, `-` : 缩放微调

鼠标操作
========

.. image:: /images/blender-view-buttons.png

- 那个 XYZ 的地方可以使用鼠标拖拽，也可以点击对应的轴
- 放大镜、手状图标可以拖拽放大缩小、平移视角
- 放映机图标是视角变为场景摄像头视角
- 那九个格子的图标是透视投影和垂直投影的切换

鼠标中键：

- 滚动 : 放大缩小
- 拖拽 : 旋转视角
- Shift+拖拽 : 平移视角
- Ctrl+拖拽 : 放大缩小

基本界面交互
############

- `Ctrl+Space` : 最大化当前鼠标所在面板，或是取消最大化（回到原来界面）
- 拖拽面板边框 : 看拖拽的方向可能是分一个新的面板出来，或是把一个面板删除掉
- 右键点击面板边框 : 菜单：分一个新的面板出来，或是把一个面板删除掉
- `Ctrl+PageUp/PageDown` : 切换预设面板布局（也在窗口最上方的一栏上有）

Pie menu 操作
=============

一般 pie menu 由快捷键触发：

- 按住快捷键不松手，移动鼠标到某个选项，松开快捷键自动选中
- 也可以按快捷键松手，鼠标点击选项
- 鼠标右键取消

Layout 预设布局
===============

- `t` : 打开/关闭工具栏（包括对物体移动、旋转、缩放等操作）
- `Shift+Space` : 在当前鼠标位置打开一个工具栏；也可以按住不放，把鼠标移动到打开的工具栏的某个选项上，再松开键盘按键，自动选中该选项；

- `n` : 打开一个比较细致的参数设定栏

Cursor 相关：

Cursor 是新物体放置的中心点。

- `Shift+RightClick` : 设置 cursor
- `Shift+s` : Cursor 的一些操作的菜单，例如重设 cursor 到原点
- `n` : 在打开的栏里的 View 里的 3D Cursor 里定量设置

- `z` : 打开一个 pie menu 设置显示样式（线框、渲染、实体等等）

物体操作
########

选择：

- `a` : 全选
- `Alt+a` : 取消全选
- 默认左键拖拽矩形选框选择
- `b` : 矩形选框，左键拖拽选框选择，中键拖拽选框取消选择
- `c` : 刷子选择，左键拖拽，选中刷到的物体，同样中键会取消刷到的物体
- `Shift+LeftClick` : 选中没有选中的物体，取消选中选中的物体
- 左上角的 Select 选单

物体查看：

- 右上角 Wireframe 等

物体移动：（也可以在 `t` 工具栏里找）

- `g` : 平移，按 g 以后可以配合 `x`, `y`, `z`, `Shift+x`, `Shift+y`, `Shift+z` 快捷键限制平移方向
- `r` : 旋转
- `s` : 缩放
- `Alt+g`, `Alt+r`, `Alt+s` : 还原物体最初的对应设置
- 可以在 3D Viewport 的上方找到更改旋转、缩放中心点的选项，例如可以设置成 3D Cursor（图为默认的 Active Element 选项）

  .. image:: /images/blender-transform-pivot-button.png

- 可以在 3D Viewport 的上方找到更改旋转、缩放坐标系的选项，例如可以设置成 Local，按物体本身的上下左右来移动；快捷键可以连按两次相应的轴，如 `x x`, `y y`, `z z`
- 3D Viewport 的上方还有 Snap 工具，可以用来对齐等等，快捷键是 `Shift+Tab`
- 在拖拽时按 `Shift` 可以减慢移动速度

添加物体：

- `Shift+a` : 添加物体

删除物体：

- `Delete`
- `x`

隐藏物体：

- `Shift+h` : `Alt+h` 取消

物体集合（Collection）：

- 右键菜单 -> Move to Collection -> New Collection -> 名字 -> Enter 两次
- Outliner 面板里快捷键 `c` 创建， `x` 删除

编辑物体
########

Object Mode
===========

- `Tab` : Edit Mode
- `Ctrl+Tab` : 在 active 物体为 armature 时进入 Pose Mode；否则会弹出菜单
- `Ctrl+j` : 合并物体

Edit Mode
=========

- `p` : 分离物体
- `1`, `2`, `3` : 切换选择模式（顶点、棱边、面），可与 `Shift` 联用
- `Alt+Shift+LeftClick` : 选中一圈的点/棱边/面
- `e` : Extrude，右键取消，但仍会生成重叠的面，需要按 `Ctrl+z` 来完全取消
- `Ctrl+r` : Loop cut，鼠标滚轮可以增加切割数量
- `Ctrl+b` : Bevel, `Ctrl+Shift+b` : 只在顶点处生成圆滑处理；可以使用鼠标滚轮

  也可以使用 Bevel 的 Modifier

- `k` : Knife，右键完全取消， `e` 取消当前跟随着鼠标的连线， `Space` 或 `Enter` 确认
- Bisect : 似乎没有快捷键

Shading
########

基本上就是各种连线。

- `Shift+LeftClick` : 选中多个
- `f` : 自动连接选中的两个节点
- `Ctrl+x` : 删除并自动重连
- `Alt+LeftMouseDrag` : 移走并自动重连
- `Ctrl+LeftMouseDrag` : 移除被鼠标切过的线
- `u` 或者上方 UV 选单 : 选择 unwrapping 算法
- Voronoi Texture 很漂亮

Render
######

- `F12` : 渲染图片
- `Ctrl+F12` : 渲染动画
- `Ctrl+v` : 渲染范围选框，使用 `Ctrl+Alt+v` 取消
- `F11` : 显示图片渲染结果
- `Ctrl+F11` : 显示动画渲染结果

- Cycles 引擎的取样少了的话，可以使用 Denoising 减少噪声

Rigging
#######

- `Ctrl+p` : 设置 active 的物体为所有选中物体的 parent， `Alt+p` 取消

  当选中 armature/bone 的时候会有不同的选项

- 可以安装 Rigify 插件来自动生成一个人体骨架

Pose Mode 里：

- `m` : 移动选中部分 armature 到某一 layer

Weight & Vertex Groups
======================

- 同时选中物体以及其关联 armature ，将物体设为 active 物体，按 `Ctrl+Tab` 可以出选单，可以选择进入 Weight Paint 模式，Weight Paint 模式里可以 `Ctrl+LeftClick` 选中对应 armature

Animation
#########

Timeline 面板

- `i` : 对选中的物体插入 keyframe，在弹出菜单里选择 keyframe 记录的数据
- `v` : 改变选中的 keyframe 点的插值方法

还有两个面板是 Dope Sheet 和 Graph Editor

- `Ctrl+Shift+m` : Graph Editor 里的曲线的 modifiers

Sculpt Mode
###########

- `f` : 笔刷大小， `Shift+f` 笔刷强度
- `x` : draw
- `c` : clay : flattening
- `l` : layout
- `i` : inflate
- `Shift+s` : smooth
- `p` : pinch
- `g` : grab
- `k` : snake hook
- `m` : mask, `Alt+m` to remove, `Ctrl+i` to invert mask

- 可以从普通的大致模型增加面数后再开始 sculpt，可以在 Sculpt Mode 按 `n` 在 Tool -> Remesh 里找到相关工具；也可以使用 Dyntopo （在 Remesh 上方）
