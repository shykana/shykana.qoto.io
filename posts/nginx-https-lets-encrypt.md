---
title: Nginx 启用 HTTPS (Let's Encrypt)
category: 服务器
tags:
id: '47'
date: 2019-08-04 20:43:49
updated: 2019-08-04 20:43:49
---

现在好像比前几年方便了好多, 跟着官网教程走就行了. [https://letsencrypt.org/](https://letsencrypt.org/) 甚至会自动把服务器设置改好... 还有自动 renew...
