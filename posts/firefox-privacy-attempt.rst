.. title: Firefox 隐私设置尝试
.. slug: firefox-privacy-attempt
.. date: 2021-02-01 23:54:14 UTC+08:00
.. tags: 
.. category: 工具
.. link: 
.. description: 
.. type: text

之前看到有关于隐私设置的讨论，得知了 PrivacyTools_ 这么一个网站。据网站的介绍，该网站主要提供了保护隐私的一些服务、工具以及相关知识。其中，网站上提到了 `Firefox 的一些隐私设置`_ 。

.. TEASER_END

下面直接把整个 Firefox （以及同步的一些配置）清空，重新设置一遍。一般来说不用这样做，但我喜欢 :P

.. _PrivacyTools: https://www.privacytools.io
.. _`Firefox 的一些隐私设置`: https://www.privacytools.io/browsers/#about_config

#######################
 重置 Firefox 重新设置
#######################

其实一般来说并不需要重置 Firefox，但反正我重置一下。前往 `about:support`_ 找到重置/Reset Firefox 的选项，仔细阅读重置的项目后确定重置。

.. _`about:support`: about:support

================
一些我个人的设置
================

按照 `PrivacyTools 推荐的一些插件`_ 以及 `Firefox 的一些隐私设置`_ 这里设置。

插件请自行选取，我尝试了：

- CanvasBlocker
- ClearURLs
- Decentraleyes
- Firefox Multi-Account Containers
- HTTPS Everywhere
- Terms of Service; Didn't Read
- uBlock Origin

另外还找了一些其它的插件：

- `Font Fingerprint Defender`_
- `Privacy Badger`_
- `Referer Modifier`_
  这个与下面的 ``network.http.referer.XOriginPolicy = 1`` 的设置有点关系，需要额外设置。见 `Bilibili`_ 的兼容性。
- `User-Agent Switcher and Manager`_

.. _`PrivacyTools 推荐的一些插件`: https://www.privacytools.io/browsers/#addons
.. _`Font Fingerprint Defender`: https://addons.mozilla.org/en-US/firefox/addon/font-fingerprint-defender/
.. _`Privacy Badger`: https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/
.. _`Referer Modifier`: https://addons.mozilla.org/en-US/firefox/addon/referer-modifier/
.. _`User-Agent Switcher and Manager`: https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/

.. code::

    privacy.firstparty.isolate = true
    privacy.resistFingerprinting = true
    privacy.trackingprotection.fingerprinting.enabled = true
    privacy.trackingprotection.cryptomining.enabled = true
    privacy.trackingprotection.enabled = true
    browser.send_pings = false
    browser.urlbar.speculativeConnect.enabled = false
    network.http.referer.XOriginPolicy = 1
    network.http.referer.XOriginTrimmingPolicy = 2
    browser.sessionstore.privacy_level = 2
    beacon.enabled = false
    browser.safebrowsing.downloads.remote.enabled = false
    network.dns.disablePrefetch = true
    network.dns.disablePrefetchFromHTTPS = true
    network.predictor.enabled = false
    network.predictor.enable-prefetch = false
    network.prefetch-next = false
    network.IDN_show_punycode = true
    extensions.pocket.enabled = false

一些其它设置
------------

（当作笔记顺便放在这里了。）

设置里 `Ctrl+Tab cycles through tabs in recently used order` 是默认开着的，我个人不喜欢，因为这样 `Ctrl+Shift+Tab` 就完全用不了了。

然后 `Restore previous session` 以及 `Warn you when quitting the browser` 我也一般开着。

以及下载设为 `Always ask you where to save files` 。

强制所有页面显示阅读模式的按钮：

.. code::

   reader.parse-on-load.force-enabled = true

################
使用时的一些问题
################

设置还是比较直接的，这里记录一些日常的问题。

==================
 一些不兼容的网站
==================

Apple Music (网页)
------------------

似乎开启了 ``privacy.resistFingerprinting = true`` 了之后会直接没办法登陆。我暂时的解决办法是只在登陆时稍微关掉这个选项，登陆完后再开启。但由于我并不清楚 Fingerprinting 的原理，所以还请谨慎应用。

Bilibili
--------

我个人体验是点击一些页内链接时会有问题，比如点击另一个视频时，会出现播放器的视频改变了，但是标题和评论还是上一个视频的问题。另外，播放器自动切换下一P时，播放器外部的那个分P列表的高亮条目并不会改变。（例如明明看到第三P了，但是列表高亮的还是第一P。）

另外一个问题是 ``network.http.referer.XOriginPolicy = 1`` 设置后会出现 ``MEDIA SEGMENT 下载错误`` 的问题。似乎可以尝试一些插件。例如，使用 `Referer Modifier`_ ，并设置为：（如果其它网站访问有问题，可以尝试把 ``<ANY>`` 那里的设置稍微修改一下，例如改为 ``Prune`` ；也可以尝试找到后台错误的请求，并新建对应的条目。）

.. csv-table:: 设置
    :header: "Target Domain", "Action", "Replacement Referer"

    "<ANY>", "Remove", ""
    "<SAME>", "Prune", ""
    "bilivideo.com", "Replace", "https://bilibili.com/"
    "bilivideo.cn", "Replace", "https://bilibili.com/"
    "bilibili.com", "Replace", "https://bilibili.com/"
    "hdslb.com", "Replace", "https://bilibili.com/"

似乎这里也有一些抱怨，不知道会不会是其它问题： https://mastodon.social/@loikein/105656219963631940

另外， ``privacy.resistFingerprinting = true`` 似乎会导致弹幕无法播放……

解决办法？能用就行，不用最好 :P

几乎所有用到 Canvas 的网站
--------------------------

这个大概是 CanvasBlocker 的问题，但是我也懒得测试是哪一样设置的问题了，设置后会让所有 canvas 都变成条纹状图案（还挺好看的）。（如图：Bilibili 的进度条预览）

.. image:: /images/bilibili-canvas.png

Pleroma
-------

``dom.event.clipboardevents.enabled = false`` 会导致 Pleroma 无法读取粘贴事件。Pleroma 有个功能是复制的图片（例如截图至剪切板）可以通过 Ctrl+V 直接上传为附件，但是设置了 ``dom.event.clipboardevents.enabled = false`` 后，这个功能就无法使用了。
