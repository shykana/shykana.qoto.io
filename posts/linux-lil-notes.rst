.. title: Linux 小小笔记
.. slug: linux-lil-notes
.. updated: 2021-06-12 20:02:00 UTC+08:00
.. date: 2021-01-28 11:24:00 UTC+08:00
.. tags: Linux, 笔记
.. category: 工具
.. link: 
.. description: 
.. type: text

.. contents:: 目录
    :backlinks: entry
    :class: toc

============
有关文档查询
============

（当然也可以上网查询。）

tldr
====

.. attention::
    说真的我个人认为这个命令比这篇文章好多了。

（名字来源 ``TL;DR``: Too Long; Didn't Read）
是在 https://boilingcong.ee/@gfis/105474645790577707 这里看到的。真的是大救星！ **大！救！星！**

基本上是提供了很多命令的常用用法，使用 ``tldr 命令名`` 就可以看到这种简化文档。

项目地址在这里： https://tldr.sh/
官方版本似乎是基于 NodeJS 的，但我安装失败了然后改用了 Rust 版本的 `tealdeer`_ 。想尝试其它版本的可以在 https://github.com/tldr-pages/tldr/wiki/tldr-pages-clients 找到其它版本。

.. _`tealdeer`: https://github.com/dbrgn/tealdeer

.. TEASER_END

man
===

（Manual，不知道为什么直接简写成 man 了。）
传统上 linux 可以使用 ``man 命令名`` 查询命令所对应的文档。

--help
======

如果没有 man 页面的话，一般的命令行会提供一个 ``--help`` 选项，用于查看命令的用法。有些命令用的不是 ``--help`` ，这个时候可以把这些命令都尝试一下：

.. code:: shell

    命令名 --help
    命令名 -h
    命令名 -help

有些命令在直接使用 ``命令名`` 调用时也会输出命令用法，但是对于一些比较危险的命令可能还是不要这样寻找用法比较好。

=======================
 Index by Command Name
=======================

aplay
=====

请见下方 `arecord`_ ，是配套的。

arecord
=======

`aplay` 和 `arecord` 都是 ALSA 附带的命令行工具， `aplay` 用于播放音频， `arecord` 用于录制音频。

简略用法推荐直接看 `tldr`_ 的文档。

一种特殊用法是：（来自 https://askubuntu.com/a/887658 ）

.. code:: shell

    # 将麦克风接收的声音直接播放出来，可能有延迟
    arecord -f cd - | aplay -
    # 减少延迟，可以把 1 改大一点（e.g. 40）防止 overrun
    arecord -f cd --buffer-time=1 - | aplay --buffer-time=1 -
    # 一边播放一边保存
    arecord -f cd - | tee output.wav | aplay -

可以人工引入延迟：（来自 https://stackoverflow.com/a/12852526 ）

.. code:: shell

    arecord -f cd - | dd ibs=16000 iflag=fullblock oflag=dsync | aplay -

中间的 `dd` 的 `16000` 是延迟的字节数，不能直接推出具体的延迟时间。

asciinema
=========

终端“录屏”工具，录下来的不是一般的视频而是带有文本信息的“动态终端”。（一个例子是在播放时可以直接把终端文本复制下来。）

更多请见 https://asciinema.org/ 。这里放一个例子：

.. raw:: html

    <script id="asciicast-335480" src="https://asciinema.org/a/335480.js" async></script>

BC / DC
=======

``bc`` 计算器程序，当然也可以考虑 ``dc`` 。

设置小数位
~~~~~~~~~~

设置默认（放到 ``.bashrc`` 或 ``.zshrc`` 里）：

.. code:: shell

  alias bc="BC_ENV_ARGS=<(echo "scale=5") \bc"

来源： `sjas@AskUbuntu`_ .

.. _`sjas@AskUbuntu`: https://askubuntu.com/a/1175257

brltty
======

介绍是 :ruby:`Braille  display 盲文显示机` driver for Linux/Unix，似乎是一个使用盲文的终端显示。

jq
==

想在命令行下处理 JSON？（系统大概不自带。）

.. code:: console

    $ jq '.key' somefile.json
    $ echo '{"key":"value"}' | jq '.key' -
    "value"
    $ echo '{"key":[{}, {}, {"oh":{"no":"sth"}}]}' | jq '.key[2].oh["no"]' -
    "std"

diff / diffuse
==============

diff 比较按行来比较两个文本文档之间的差异。下面我直接抄 ``tldr diff`` 的了：

.. code:: console

    $ diff old_file new_file # 比较两个文本文档
    $ diff -w old_file new_file # 比较时忽略空格
    $ diff -y old_file new_file # 输出为两栏，一栏一个文档，更容易看出差异
    $ diff -r old_directory new_directory # 比较目录的变更（文件名变更，权限变更等）
    $ diff -rq old_directory new_directory # 只比较目录的文件名变更

要可视化的话可能 ``diff -y`` 已经够用了，但有时可能有个 GUI 的更方便一些。这里随便找了一个叫 ``diffuse`` 的工具，基本就是 GUI 的 ``diff -y`` 。系统一般不自带，但包管理器里应该有。

Ncdu / du
=========

都是计算文件占用空间的程序（硬盘使用，Disk Usage，故为 du），可以用于计算整个文件夹的占用空间以及分析哪一部分占用更多空间。
（应该是）所有的 Linux 系统都有 ``du`` 命令， ``ncdu`` 是命令行可视化版本，需要另外安装（用了 ``ncurses`` 所以是 ``nc du`` = ``ncdu`` ）。

``du`` 使用：

.. code:: shell

    # 当前目录的总占用空间
    du -h | tail -n 1
    # 按占用空间排序当前目录的内容（从小到大排序，但从终端上看其实刚好）
    du -h | sort -h
    # 当然也可以对某个目录使用
    du -h 某个目录 | tail -n 1
    du -h 某个目录 | sort -h

视情况可能需要 ``sudo`` ，如::

    sudo du -h / | sort -h

``Ncdu`` 使用：

.. code:: shell

    # 可视化查看根目录的空间占用
    sudo ncdu /
    # 可视化查看某个目录的空间占用（可能需要 sudo）
    ncdu 某个目录

ocrmypdf
========

（这个需要额外安装。）
对 PDF 文件进行 OCR，简单用法::

    ocrmypdf -l chi_sim+eng input.pdf output.pdf

ocrodjvu
========

（这个也需要额外安装。）
对 djvu 文件进行 OCR，简单用法::

    ocrodjvu -o output.djvu -j 4 -l chi_sim+eng input.djvu

pdf2djvu
========

（这个当然还是需要额外安装。）
将 PDF 文件转换为 djvu 文件，简单用法：

.. code:: shell

    pdf2djvu -o output.djvu --no-page-titles --anti-alias -d 72 --monochrome --lossy --no-metadata -v -j 4 input.pdf

pspp
====

严谨一点应该叫 `GNU PSPP`_ ，是 GNU 出品的 SPSS 的自由软件替代。具体差别不太清楚，统计相关工作的可以尝试一下。

.. _`GNU PSPP`: https://www.gnu.org/software/pspp/

scrcpy
======

一个将安卓屏幕投屏到 linux 上的程序（似乎也有 Windows 和 Mac 版）： https://github.com/Genymobile/scrcpy （中文版页面： https://github.com/Genymobile/scrcpy ）

顺便一提， `scrcpy 可以同步粘贴版`_ ，一些时候会非常方便。

.. _`scrcpy 可以同步粘贴版`: https://github.com/Genymobile/scrcpy#input-control

xclip
=====

从命令行操纵粘贴版，可能需要额外安装。似乎不能应用于图片等文件类型。用法：

.. code:: shell

    # 复制某一文件的内容，使用鼠标中键的粘贴版
    xclip 文件名
    # 复制某一文件的内容，使用 Ctrl+V 的粘贴版
    xclip -sel clip 文件名
    # 通过 pipe 将其它程序的输出复制到粘贴版，如：
    echo 'blahblah' | xclip
    echo 'blahblah' | xclip -sel clip

==========
 实际应用
==========

为 Python 程序编写 SystemD Service 文件
=======================================

.. code::

    [Unit]
    Description=Some Python Program

    [Service]
    ExecStart=/bin/python3 /path/to/python_program.py
    Environment=PYTHONUNBUFFERED=1

    [Install]
    WantedBy=default.target

关键点在于 ``Environment=PYTHONUNBUFFERED=1`` ，否则日志将有很大概率丢失一部分。

如果这个 Python 程序是一个 User Unit，那么你需要 ``sudo loginctl enable-linger <username>`` 来确保它可以开机自启。

将图片生成 PDF
==============

答案改自 Martian: `https://stackoverflow.com/a/26610519`_ ：

.. code:: shell

    for i in `ls -v *.jpg`; do convert $i ${i//jpg/pdf}; done
    gs -dNOPAUSE -sDEVICE=pdfwrite -sOUTPUTFILE=merged_file.pdf -dBATCH `ls -v *.pdf`

（两个命令均需要额外安装。 ``convert`` 来自 imagemagick，而 ``gs`` 来自 ghostscript。）
这里假设图片均为 ``.jpg`` 后缀，并且文件名中没有空格，最好是 ``1.jpg 2.jpg 3.jpg ...`` 这样的。
这个可以配合上面提到的 `pdf2djvu`_  `ocrmypdf`_  `ocrodjvu`_ 来使用。

.. _`https://stackoverflow.com/a/26610519`: https://stackoverflow.com/a/26610519

从命令行操纵粘贴版
==================

请见 `xclip`_

从命令行打开文件
================

类似文件管理器双击，可以使用 ``exo-open`` 或者是 ``xdg-open`` 命令。可能需要额外安装。（在 Arch Linux 上的包分别是 ``exo`` 和 ``xdg-utils`` 。）

直接 ``exo-open 文件`` 或是 ``xdg-open 文件`` 即可，会自动使用文件类型对应的默认程序打开。

ZIP 文件解压中文乱码
====================

使用 ``unzip`` 解压的话，文件名中有中文的时候可能会有乱码，可以转而使用 ``unar`` 命令：

.. code:: shell

    unar 文件.zip

来源： https://www.cnblogs.com/tonyc/p/8059802.html

Arch Linux 下需要安装 `unarchiver` 包：

.. code:: console

    # pacman -S unarchiver

与 tar 有关的解压
=================

.. code:: shell

    # 解压 tar.gz 文件
    tar -zxf 文件名
    # 解压 tar.xz 文件
    tar -Jxf 文件名
    # 解压 tar.bz2 文件
    tar -jxf 文件名
