.. title: Licenses 笔记以及主题实现
.. slug: licenses-and-icons
.. date: 2021-01-01 09:10:52 UTC+08:00
.. tags: license, 笔记
.. category: 工具
.. link: 
.. description: 
.. type: text
.. license: ccby
.. license_mod: 测试一下。

反应过来主题应该还要加上个显示 license 的功能，但同时又不想把 license 仅仅局限于 Creative Commons 类的，所以稍微查了一下，在这里记录一下各种适用于文字作品的 license。

主要来源请见 `Various Licenses and Comments about Them`_. 中文版： `各类许可证及其评论`_.

.. _`Various Licenses and Comments about Them`: https://www.gnu.org/licenses/license-list.en.html

.. _`各类许可证及其评论`: https://www.gnu.org/licenses/license-list.zh-cn.html

#################
许可证摘选
#################

这里只摘录与 ``GPL`` / ``FDL`` 兼容的那些许可证吧。

自由文档许可证
=================

这里的文档真的就指的是专门的文档，例如软件的使用手册、课本、字典等。可能可以应用到个人写作中，但是似乎还是直接用 `实用作品的许可证`_ 会好一些。

- GNU Free Documentation License (FDL)
- FreeBSD Documentation License (FreeBSDDL)

实用作品的许可证
=================

- GNU General Public License (GPLOther)
- GNU Free Documentation License (FDLOther)
- CC0
- Creative Commons Attribution 4.0 license (a.k.a. CC BY) (ccby)

########################
主题计划的 license
########################

实现思路很简单：
1. ``conf.py`` 里需要设置一个默认的 license 选项；
2. 在 Metadata 里面有 ``license=XX`` 的就显示相应的许可证，没有就默认；
3. 有些许可证可以附加所有者自行指定的条款，这个就随便留个 ``license-mod=YY`` 的设置？

License 选项
================

首先应该有个 ``none`` 选项。

然后 CC 类的基本所有 license 都要能够兼容吧，但这里有个问题就是选项的文字怎么选。
比如是 ``ccby`` 呢还是 ``cc-by`` 呢？
可能其实 `我都要` 是最好的选项，然后又得去学 Python / Mako 的 swtich 语句了……

其次是上面列举的所有与 GPL / FDL 兼容的许可证，这个时候真的还是要纠结选项的文字。

License 显示
================

CC 类的证书都有图标，看起来观感会不错。但是其它的许可证似乎都没有？这大概只能随便显示一下了 :P
