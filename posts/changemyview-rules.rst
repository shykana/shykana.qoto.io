.. title: [翻譯] ChangeMyView 規則
.. slug: changemyview-rules
.. date: 2021-01-11 11:27:27 UTC+08:00
.. tags: 
.. category: 搬运
.. link: 
.. description: 
.. type: text

####
規則
####

1. 規則A - 不要解釋觀點
=======================

解釋您觀點背後的原因，而不要僅僅解釋觀點本身是什麼（需要 500 字以上）。

2. 規則B - 第三方/“惡魔代言”/演說
===================================

您個人必須持有相應的觀點，並展示出您願意改變這種觀點。您不能代表其它人來發表帖子，不能扮作“惡魔的代言人” [#devil]_ 或者扮作任何其它不與您相同的實體，也不能“演說”。

.. [#devil] 指的是爲了找出現有思維的毛病而專門反對現有多數人的看法的行爲。

3. 規則C - 不清晰的/不適宜的標題
================================

發表的帖子的標題必須能夠適當地總結您的觀點，並且以 `CMV:` 開頭。具有誤導性、或是過於簡化、或是含有劇透的標題有可能會導致帖子被移除。

4. 規則D - 中立/對特定人的傷害/推廣/套娃
========================================

帖子不能表達一個中立的立場，或是提倡對特定人的傷害，或是自我推廣（？），或是對這個論壇本身的討論（請到 /r/ideasforcmv 來進行這樣的討論）。

5. 規則E - 3小時內沒有來自樓主的回覆
====================================

只有在你願意和回覆的人對話，並且有着3小時的空閒時間能夠持續這麼做時，才發表帖子。如果您在3小時內沒有回覆，您的帖子將會被移除。

6. 規則1 - 沒有質疑樓主的觀點（只針對第一級回覆）
=================================================

對於 CMV 帖子的直接回覆至少需要質疑樓主的觀點的一方面（無論這方面多麼小），除非樓主問了一個澄清性的問題。

7. 規則2 - 粗魯/敵意的評論
==========================

不要對其它用戶粗魯或展現敵意。就算剩餘部分十分有說服力，您的評論還是會被移除。“是TA們先開始的”不是藉口。您應該匯報（flag）先開始的評論不是回應這種評論。

8. 規則3 - 指控不講誠信
=======================

不要指控樓主或其它任何人不願改變觀點或是在不講誠信地辯論。如果您不確信某人說的，就詢問一些澄清性的問題（參見：蘇格拉底方法）。如果你認爲TA們還是在展現不恰當的行爲，請與我們聯繫。

9. 規則4 - 濫用/誤用DELTA或是應該頒發DELTA
==========================================

如果您承認您的觀點有改變，請頒發DELTA。請不要將DELTA用作其它目的。在頒發DELTA時，您必須附上對您的改變的解釋，讓我們知道這種改變是真的。DELTA的濫用包括諷刺的DELTA、玩笑的DELTA、超級點讚的DELTA等等。

10. 規則5 - 不有意義的參與
==========================

評論必須有意義地參與到對話中。只包含鏈接、玩笑或是手動點讚的評論將會被移除。幽默或是贊同的肯定可以被包含在更實在的評論之中。

#########
DELTA系統
#########

無論您是不是樓主，請回覆對您的觀點有任何改變的用戶，並在您的評論裏包含一個 DELTA（操作請見下方），並且附上一段對改變的解釋。

.. csv-table:: 輸入 DELTA 的方法
    :header: "方法", "針對"

    "複製粘貼⇨ Δ ", "所有系統"
    "Unicode⇨ &#8710 ", "所有系統"
    "Option/Alt+J", "Mac"
    "Ctrl+Shift+u2206", "Linux"
    "!delta", "在您無法使用 Δ 時使用"

###################
DELTA系統（詳細版）
###################

概覽
====

匯報
====

爲什麼是 DELTA？
================

Delta機器人 代碼
================
