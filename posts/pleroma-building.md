---
title: 尽量细致的Pleroma搭建教程（仮）
category: 服务器
tags:
  - Pleroma
id: '183'
date: 2020-07-18 21:22:53
updated: 2021-08-16 18:57:46 UTC+08:00
---

[TOC]

预先准备
====

首先，这里先假设你已经选购了一台 VPS 并且已经能够使用 SSH 连接到这台机器。

由于 Pleroma OTP 版本的限制，本教程推荐 VPS 使用 64 位的系统（x86_64 或 amd64）。当然，Pleroma 也适用于其余系统，只是可能会麻烦一些。

本文主要基于 Ubuntu 系统进行操作，推荐使用版本号尽量高的 Ubuntu，本例中的为 Ubuntu 20。

### Arch Linux 的捷径（误）

（其实也没有那么捷径。）

我将 Pleroma 的部分安装流程包装在了 Arch Linux 的一个 AUR 包里： [pleroma-bin](https://aur.archlinux.org/packages/pleroma-bin/) 。

当然最后的配置以及域名、HTTPS 证书之类的配置还得自己来。

推荐先安装 postgresql 并初始化 postgresql：
```
sudo pacman -S postgresql
sudo su - postgres -c "initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'"
sudo systemctl start postgresql
sudo systemctl enable postgresql
```

然后使用 AUR 包：
```
git clone https://aur.archlinux.org/pleroma-bin.git
cd pleroma-bin
makepkg -s -i
```

至今 pleroma 已经安装好了，还差配置：
```
cd /opt/pleroma
sudo -Hu pleroma ./bin/pleroma_ctl instance gen --output "/etc/pleroma/config.exs" --output-psql "/tmp/setup_otp_db.psql" --uploads-dir "/var/lib/pleroma/uploads" --static-dir "/var/lib/pleroma/static" --dbname pleroma --dbuser pleroma
sudo -Hu postgres psql -f "/tmp/setup_otp_db.psql"
sudo -Hu pleroma ./bin/pleroma_ctl migrate
```

开启 pleroma：
```
sudo systemctl start pleroma
sudo systemctl enable pleroma
```

创建第一个管理员：（把下面张三 ZhangSan 李四 LiSi 替换成自己的相关资料，考虑隐私的话邮箱那里其实可以乱写。）
```
sudo -Hu pleroma ./bin/pleroma_ctl user new ZhangSan LiSi@example.com --admin
```
执行完出现的一条链接可以用来设这个管理员的密码。

之后还是需要手动设置 DNS 以及配置 HTTPS 证书。如果可以使用 cloudflare 的话可以考虑使用后面提到的一个便捷一点的方法。

### 对VPS机器的要求

Pleroma 对机器的要求较低。本教程在 256MB 内存（附加SWAP交换区间，见下）上的机器成功运行，理论上也可以在 128MB 甚至更低内存的情况下成功，但是低内存引起的卡顿可能会影响使用。

### 选购并使用SSH连接机器

请自行上网查阅教程。 选购请三思而后行。使用SSH连接，Windows用户请上网查阅 PuTTY 或其他类似软件的教程，Linux或MacOS用户请查阅SSH命令或相关软件的教程。

### 提醒

关于教程中使用的邮箱的提醒，引自：奈卜拉：[https://i.nebula.moe/posts/2019-09-13-mastodon/](https://i.nebula.moe/posts/2019-09-13-mastodon/)

> 这里推荐使用国际邮箱注册，例如 [Gmail](https://gmail.com/)，请**不要使用国内的邮箱服务**，如 163、QQ 等。如果无法访问 Gmail，微软的 [Outlook](https://outlook.com/) 以及来自瑞士的 [Protonmail](https://protonmail.com/) 都是不错的选择。

注册域名并配置DNS
==========

本文介绍使用 FreeDNS 免费注册域名的方法。（此处注册的是二级域名，但是不理解二级域名不影响本教程的使用。）通过其他手段注册的域名也可以用于本教程（如来自[EU.org](https://nic.eu.org/)的免费域名或其他免费或付费的域名），但对应的本小节的操作可能有所不同。

*注意！使用 FreeDNS 免费域名会使得之后无法使用 Cloudflare 加速！暂时还没有解决方法。*

### 注册账号

![FreeDNS 主页](/images/1-freedns_001.png)

前往[https://freedns.afraid.org/](https://freedns.afraid.org/)，点击页面左方（如上图所示）任意链接均会跳转至登陆界面，选择登录界面的 **Sign up Free** 按钮进行注册。

![登录界面](/images/2-freedns-pre-signup_001.png)
对于注册中信息的填写以及邮箱的验证此处不再赘述。 重新点击网页左侧任意链接，跳转至登录界面进行登陆。 ![](/images/12-freedns-registry.png)

### 申请域名

登陆后，点击左侧 “Registry” （如上图所示）进入 Domain Registry寻找可用的域名。出现的界面中，需要注意的除最左边的域名以外，还有下图所示的 Status 一列。建议选择 Status 为 public 的，因为 private 的域名持有者可能随时将你的域名删除。

![](/images/12-freedns-registry-find-warn-private_001.png)

![](/images/13-freedns-found.png)

此处教程中选择了 allowed.org，于是我们最终的域名可以是 “我们选择的词语.allowed.org”。 点击 allowed.org 或您所选择的域名进入如下图界面。我们需要填写的有 Subdomain、Destination以及最下方的验证码。Subdomain 请填写自己心仪的、符合域名规范的词语（若不了解，请尝试查找域名规范）；Destination 请填写所持有的 VPS 的 IP 地址（IPv4）。“Save!” 按钮确认。

![](/images/14-freedns-registry-confirm.png) 您选择的域名可能已被其他人占用。此时网页会弹出提示，请您更换 Subdomain 或在 “Registry” 界面选择其他域名。 教程所填写的将会得到域名 wuppo.allowed.org 。您将会得到 “子域名.主域名” 的域名，其中子域名代表您在 Subdomain 中填写的文字，主域名代表您在 Domain Registry 界面所选择的域名。 此后，您可以点击页面左侧的 Subdomains 管理或修改您所得到的域名，此处不再详述。

配置服务器并安装
========

本教程主要针对 Ubuntu/Debian 系的服务器系统。若选购的 VPS 为其他系统，可考虑通过商家的相应接口重装系统为 Ubuntu/Debian，也可上网搜索教程的各部分的对应步骤，网上应都有大量教程。

### 保持服务器的安全性

在安全性方面，可以参考 Mastodon 官方教程中的：

*   [禁止密码登录SSH（仅允许密钥登录）](https://docs.mastodon.bgme.bid/zh-cn/admin/prerequisites/#禁止密码登录ssh仅允许密钥登录)
*   [更新系统](https://docs.mastodon.bgme.bid/zh-cn/admin/prerequisites/#更新系统)
*   [安装 fail2ban 以阻止重复登录尝试](https://docs.mastodon.bgme.bid/zh-cn/admin/prerequisites/#安装-fail2ban-以阻止重复登录尝试)

Mastodon 教程仅针对 Ubuntu/Debian 系统的服务器，其他系统请搜索对应的教程。

##### 防火墙的配置

Mastodon 教程（[安装防火墙并只暴露SSH、HTTP、HTTPS端口](https://docs.mastodon.bgme.bid/zh-cn/admin/prerequisites/#安装防火墙并只暴露sshhttphttps端口)）中使用了 iptables-persistent 作为防火墙，适用于本教程。但是这样的编辑防火墙的方式较难以让人理解，若有时间可以了解使用 ufw （意为 Uncomplicated Firewall）。

###### ufw 的简单设置
（若使用 ufw 则无需按照 Mastodon 教程中的 iptables 部分设置了。）

Ubuntu 下安装 ufw：
```
sudo apt install ufw
```

ufw 的简单设置：允许 SSH（22 端口，如果 SSH 改成了其它端口请务必对应把那个端口给 allow 了）、HTTP 以及 HTTPS：
```
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 22
sudo ufw enable
```

可以看出，相较 iptables，ufw 的设置更加简明易懂。

### 安装 Pleroma

~~暂时参考这里：https://docs-develop.pleroma.social/backend/installation/debian\_based\_en/ 我现在要转移 Wordpress 留给 Pleroma ）~~
~~VPS 快到期了……索性换了一个好一点的 VPS。~~

Pleroma 有两种安装方式，一是从源代码从头编译，二是使用官方编译的 OTP 版本。后一种 OTP 版本的好处是会省下很多自己编译的时间，对于一些垃圾的 VPS 就更是如此。

总之，我们优先使用 OTP 进行服务器搭建。

#### OTP 版本搭建
官方教程：[Installing on Linux using OTP releases](https://docs-develop.pleroma.social/backend/installation/otp_en/)

首先，使用官方教程里提供的 Detecting flavour 脚本来判断自己的 VPS 是否有 OTP 支持：
```
arch="$(uname -m)";if [ "$arch" = "x86_64" ];then arch="amd64";elif [ "$arch" = "armv7l" ];then arch="arm";elif [ "$arch" = "aarch64" ];then arch="arm64";else echo "Unsupported arch: $arch">&2;fi;if getconf GNU_LIBC_VERSION>/dev/null;then libc_postfix="";elif [ "$(ldd 2>&1|head -c 9)" = "musl libc" ];then libc_postfix="-musl";elif [ "$(find /lib/libc.musl*|wc -l)" ];then libc_postfix="-musl";else echo "Unsupported libc">&2;fi;echo "$arch$libc_postfix"
```
其实，从本文写作时的教程的脚本代码里可以看出，OTP 暂时不支持 32 位（x86）的系统。所以如果出错，可以考虑到 VPS 提供商除重装一个 64 位的系统（x86_64 或 amd64），然后重新再来一遍本教程。

安装所需的包：
```
sudo apt update
sudo apt install curl unzip postgresql postgresql-contrib nginx certbot ncurses-base ncurses-bin libncurses5 libncursesw5 libtinfo5
```
对于其他发行版来说可能需要注意这里的 ncurses 相关包是 libncurses5，见：[https://git.pleroma.social/pleroma/pleroma/-/issues/1181](https://git.pleroma.social/pleroma/pleroma/-/issues/1181)

本文由于只是示范搭建一个简单的实例，所以选择跳过 RUM indexes 的安装。

在 (Optional) Performance configuration 一节，如果你选择使用 [PGTune](https://pgtune.leopard.in.ua/) 来调节 postgresql 的设置，直接将生成的配置文本粘贴到 postgresql 设置文件的最后即可。修改完成后重启 postgresql：
```
sudo systemctl restart postgresql
```
~~文中说明要修改 Ecto database 的配置，但这其实是要等到后面我们安装得差不多的时候再配置的。现在似乎没有那样的设置了？~~

随后基本上跟着官方教程走就可以了。

#### 使用 Cloudflare Tunnel 作为 Nginx+Certbot 的替代

首先这个方法需要一个 cloudflare 管理的域名，所以上面通过 FreeDNS 注册的免费域名是无法使用该方法的。

这个方法是不需要 certbot 那样担心证书过期的。

（把自己的域名添加到 cloudflare 的方法就不在这里写了。上网都有教程。添加可能费时一两天。）

首先安装 cloudflared： https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/installation

Ubuntu / Debian 可以用下面的安装方法：
```
wget -q https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64.deb
sudo dpkg -i cloudflared-linux-amd64.deb
```

Arch Linux 有相应的 AUR 库，这里不赘述。

安装后：
```
cloudflared tunnel login
```
打开程序输出的链接授权 cloudflared。（如果没有出现与 Argo Tunnel 有关的页面，可以登录 cloudflare 后再打开一次原来的链接，或者多打开几次。）

如果你在此之前已经有使用 cloudflare 管理的域名，那么你登录后看见的 Argo Tunnel 页面会由一个域名列表，选择一个之后使用的域名，点击 Authorize，不出所料应该会有一个 Success 的对话框表示已经成功。

创建一个 tunnel：（下面命令的 `pleroma` 那里可以改成其它的名字啦，之后的也要改）
```
cloudflared tunnel create pleroma
```
（如果想要删除一个 tunnel，可以使用 `cloudflared tunnel delete pleroma` 来删除。例如有多台 VPS 的话有时会有多个 tunnel 重名而无法创建的情况，这时就可以在任何一台上执行这个删除命令。）

然后将这个 tunnel 与域名绑定：（后面的 `some_name.your_domain.org` 需要改成相应的域名）
```
cloudflared tunnel route dns pleroma some_name.your_domain.org
```
（有可能会有 `... already exists with that host` 的错误，可以到 cloudflare 控制台里去把相应的 A, AAAA 或 CNAME 记录删掉。）
这时会有一行输出：
```
2021-08-16T10:25:08Z INF Added CNAME some_name.your_domain.org which will route to this tunnel tunnelID=b2404a5d-d353-407a-8ae1-983752ca0de0
```
把后面那个 `b2404a5d-d353-407a-8ae1-983752ca0de0` 复制下来。

编写一个配置文件 `/etc/cloudflared/config.yml`：（一个简单的编辑命令可以是 `nano /etc/cloudflared/config.yml`，Ctrl+S 保存，Ctrl+X 退出。）
内容为：
```
url: localhost:4000
tunnel: b2404a5d-d353-407a-8ae1-983752ca0de0
credentials-file: /home/你的用户名/.cloudflared/b2404a5d-d353-407a-8ae1-983752ca0de0.json
```
（用户名可以从命令 `echo $USER` 的输出里看。那一长串 `b2404a5d-d353-407a-8ae1-983752ca0de0` 要替换为之前自己复制的。`localhost:4000` 的 `4000` 是 pleroma 监听的端口，默认就为 4000（在安装时的提示为 `What port will the app listen to`）。）

最后直接：
```
sudo cloudflared service install
sudo systemctl start cloudflared
sudo systemctl enable cloudflared
```

记得启动 pleroma。

Troubleshooting / 各种问题
====

#### Pleroma 的邮件配置

**更新**：给 Pleroma 提了 issue，下一个版本应就会把这个问题修好了。也就是 `/etc/systemd/system/pleroma.service` 里应该已经改好了。（当然去确认一下也没问题。）

可以看这篇 [../pleroma-using-postfix-for-email/](../pleroma-using-postfix-for-email/) ，更详细一点。

> TL;DR：
> 用第三方邮件服务，用 Postfix 连接 SMTP，在 `/etc/systemd/system/pleroma.service` 里将 `NoNewPrivileges` 设置为 `false`。

邮件上比较普适的配置是使用带有 SMTP 接口的邮件提供商。这里以 Zoho 为例，你需要知道：
1. SMTP 服务器的域名以及 TLS 的端口号：
本例中分别为：`smtp.zoho.com` 以及 `587` （一把）
2. 你的邮件地址以及密码，我们这里姑且用以下来代替：
邮件地址：`testest@mydomain.org` 和密码： `pa$$word`

在了解到以上信息后我们来安装 Postfix：
```
sudo apt-get install libsasl2-modules postfix
```

在安装过程中，请在弹出来的配置界面中选择 `Internet Site` 并在后面的 `System mail name` 中填入您 Pleroma 对应的域名。

然后，我们来配置 Postfix 的 SMTP 信息：
1. 新建 `/etc/postfix/sasl/sasl_password` 文件，并在其中按以下格式填入您的 SMTP 相应信息：
```
[smtp.zoho.com]:587 testest@mydomain.org:pa$$word
```
2. 运行 `sudo postmap /etc/postfix/sasl/sasl_password` 命令来相应处理刚才创建的信息；
运行以下命令来确保密码文件的权限安全：
```
sudo chown root:root /etc/postfix/sasl/sasl_passwd /etc/postfix/sasl/sasl_passwd.db
sudo chmod 0600 /etc/postfix/sasl/sasl_passwd /etc/postfix/sasl/sasl_passwd.db
```
3. 编辑 `/etc/postfix/main.cf` 文件来相应设置 Postfix，请相应替换 `smtp.zoho.com`, `587`：
```
myhostname = 您的 Pleroma 域名
relayhost = [smtp.zoho.com]:587
smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl/sasl_passwd
smtp_tls_security_level = encrypt
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
```
4. 重启 Postfix 服务：
```
sudo systemctl restart postfix
```

这个时候不出意外您应该已经可以使用 `sendmail` 命令发送简单的邮件了。在终端里依次输入（请将 `your@mail.account` 替换为您确定可以接收邮件的邮箱地址）：
```
sendmail your@mail.account
From: testest@mydomain.org
Subject: Test
Tester. Testest. 
.
```
其中第一行输入了 `sendmail` 命令，包括了收件人地址sendmail recipient@elsewhere.com
From: you@example.com
Subject: Test mail
This is a test email
.
；其余行是邮件内容，分别包括了发信人地址（`From:`）、邮件标题（`Subject:`）、邮件内容（`Tester. Testest. `）以及标志着结束的单独的一个句号。

**但是！** Pleroma 在这个时候很可能发邮件会出错！请做以下改动：
1. 使用 `sudo postfix check` 检查，一般不会在这一步出错，若有除了 warning 以外的出错，请使用搜索引擎搜索相应解决方案；
2. 检查文件权限：使用 `ls -lah /var/spool/postfix/` 命令，检查输出中是否有两行与以下内容一致：
```
drwx-wx---  2 postfix postdrop 4.0K Jun  4 23:39 maildrop
drwx--x---  2 postfix postdrop 4.0K Jun  4 23:38 public
```
若不一致一般可以这样修复：
```
sudo chown postfix:postfix /var/spool/postfix/maildrop /var/spool/postfix/public
sudo chmod g+x /var/spool/postfix/maildrop /var/spool/postfix/public
sudo chmod g+w /var/spool/postfix/maildrop
```
3. 这里是最可能的出错：Pleroma 无法获得的发送邮件的权限。编辑 `/etc/systemd/system/pleroma.service` 文件，找到包含 `NoNewPrivileges` 的一行，不出意外应该是 `NoNewPrivileges=true`，将 `true` 改为 `false`。
修改完后：
```
sudo systemctl daemon-reload
sudo systemctl restart postfix
sudo systemctl restart pleroma
```

配置完后，进入 `/opt/pleroma` 目录，使用 `sudo -u pleroma pleroma ./bin/pleroma_ctl email test --to your@mail.account` 来发送测试邮件。若在您的 `your@mail.account` 邮箱中找到了邮件，那么即发送成功。如果没有，可能有两个原因：
1. Pleroma 服务器的邮件配置还是没成功，请打开另一个 ssh 连接使用 `sudo journalctl -u pleroma -f` 命令实时观察出错情况，再在原有 ssh 连接中重新发送测试邮件来查看报错信息；
2. 您的邮件服务商（本例中为 `testest@mydomain.org` 的提供商 Zoho）或是您的域名被您的收信邮箱直接拒收，请检查您的邮件提供商是否有显示退信通知。

（待续）
