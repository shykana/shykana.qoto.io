---
title: Misskey 搭建备忘录
category: 服务器
tags:
  - Misskey
id: '153'
date: 2020-05-28 13:01:21
updated: 2020-11-01 13:01:21
---

基本上跟着官方教程走就行了：[setup.en.md](https://github.com/syuilo/misskey/blob/develop/docs/setup.en.md)，但是由于VPS过于廉价，还是要一点额外处理。

个人不推荐官方的 [wiki 上的另外一个教程](https://join.misskey.page/en/wiki/developers/installation/ubuntu)，写得太散乱了，而且似乎还有一些 bug……

### 普通处理：Postgresql 配置

反应过来官方那个教程其实没有讲 postgresql 的配置。官方 wiki 上的教程直接使用 postgresql 的默认用户 postgres 来配置其实是不太好的，下面配置一个新用户并只给予它访问 misskey 数据库的权限。

使用以下命令进入 postgresql 控制台：
```
sudo -u postgres psql
```

在控制台里依次输入以下命令，请务必将 `你的密码` 替换：

（个人的做法是先在进入 postgresql 控制台前在终端里使用 `uuidgen` 命令生成一个随机的 uuid 当作密码。因为这个密码只在接下来的 misskey 配置中用到，配置完了就用不到了，所以不需要是有意义的或是能记住的密码。）
```
CREATE USER misskey WITH ENCRYPTED PASSWORD '你的密码';
CREATE DATABASE misskey OWNER misskey;
```

配置完后输入 `\q` 并回车来退出 postgresql 控制台。后面的 misskey 请使用以下的 postgresql 配置：（您会在 `.config/default.yml` 中配置以下内容，在配置时替换相关内容或是添加以下内容即可。）
```
db:
  host: localhost # 没有改变默认配置
  port: 5432      # 没有改变默认配置
  db: misskey     # 重点是改这行、
  user: misskey   # 这行、
  pass: 你的密码   # 以及这行
```

### 额外处理：Swap

#### 普通文件 swap

Swap 真的救我命。一个 256MB 的机器去跑编译是真的跑不起来。注意下面的命令可能不适用于一些文件系统（ext4不用担心，可以）。建议还是看一看：[Swap file - ArchWiki](https://wiki.archlinux.org/index.php/Swap#Swap_file)

\# fallocate -l 2G /swapfile
# chmod 600 /swapfile
# mkswap /swapfile
# swapon /swapfile

上面的 “2G” 可以自己调，但是建议不低于 2G。（个人总结：256MB 的机器上 1G 跑不起来，会报错，2G 可以） 但是只是有个 swap 还不够，可能每次执行命令还需要执行一下 `export "NODE_OPTIONS=--max_old_space_size=2000"`。 想要重启自动加 swap 可以在 /etc/fstab 里加一行。

/swapfile none swap defaults 0 0

#### ZRAM

Swap 的另一个思路是使用 ZRAM。总之还是记录一下。由于 ZRAM 用 CPU 会比较多，在跑编译的时候我没有用，在日常运行的时候弄了一下，但似乎对我的机器到是基本没有什么用吧 = =。总之还是记录一下。以下来自 Arch Wiki  [Zram or zswap](https://wiki.archlinux.org/index.php/Improving_performance#Zram_or_zswap)。

\# modprobe zram
# echo lz4 > /sys/block/zram0/comp\_algorithm
# echo 32G > /sys/block/zram0/disksize
# mkswap --label zram0 /dev/zram0
# swapon --priority 100 /dev/zram0

上面的操作每次开机都要手动操作一遍。开机自动 ZRAM 搜出来有挺多结果的，但是懒得了。

### 反向代理

用 nginx （或其他服务器）反向代理可能到时申请 Let's Encrypt 的证书容易一点（`sudo certbot --nginx`就行了）。网上随便找个反向代理的配置改一下。不要忘了 websocket 需要的那几行 upgrade 命令。

### 最终效果

（256MB的机器上）编译完跑起来的时候，物理内存几乎沾满，swap占了个一两百MB。只是单用户的话，服务器上没有跑其他东西的话，用起来还行，延迟有时候以秒记（比如上传图片）（不是网络问题），总体还行。可以试一下一些免费CDN，也许有些许加速。
