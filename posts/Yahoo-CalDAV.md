---
title: Yahoo邮件的CalDAV设置
date: 2020-07-30 15:59:17
updated: 2020-07-30 15:59:17
category: 工具
tags:
  - Email
  - CalDAV
---
最近全面改用 Thunderbird 作为邮件客户端了。但是因为之前使用网页版 Outlook 的时候的日历手机同步还是很方便的，就打算找找看其他同步日历的方法。
# CalDAV
反正CalDAV就是一种同步日历的机制啦，就不详细介绍了。
由于CalDAV大体上是基于WebDAV的，所以和WebDAV一样，要同步我们要先找到相应的URL。但是其实很多提供CalDAV的邮件服务都没有把自己的URL给出来，所以一些情况下可能要自己摸索。
## 思路一：使用自动抓取URL的应用/系统
据 [国内有支持CardDAV协议的通讯录服务提供商吗？-知乎](https://www.zhihu.com/question/20772163)，苹果系统自带的邮件软件以及一个叫 [EVO Collaborator for Outlook](http://cn.evo-mailserver.com.tw/product_eco.php) 的软件都可以自动抓取相应的URL。这两个方法我没有尝试。
## 思路二：看看网上有没有人找到
反正就是搜索引擎以及大神万岁。我只搜到了Yahoo的，其他的也懒得搜了。
### Yahoo的CalDAV URL
原文：[Yahoo Calendar on Mozilla Thunderbird with Lightning and on Android Devices](http://notesofaprogrammer.blogspot.com/2016/03/yahoo-calendar-on-mozilla-thunderbird.html)
例如雅虎邮箱地址是：USERNAME@yahoo.com，你创建了一个叫做“NameOfTheDay”日历，那么地址（在Thunderbird新建日历里的Location一栏）就应该是：
`https://caldav.calendar.yahoo.com/dav/USERNAME/Calendar/NameOfTheDay`
其余的信息在Thunderbird基本上按提示来或者随便填就行了。确定了之后会弹出一个框让你登陆，这个时候才要认真填上邮箱地址以及密码。
顺便一提，摸索到似乎只有最开始的默认日历有 tasks 功能，之后创建的没有？或者有其他地方设置？不知道怎么回事。
#### 注意事项：应用密码
上面所说的“密码是需要用到账号设置里去生成的。可以参考这里： [Generate third-party app passwords](https://help.yahoo.com/kb/SLN15241.html)

# iCalendar
Yahoo有提到它也支持iCalendar，但是我~~并没有找到~~ 懒得去找相关方法了。

## NextCloud 的日历！

