---
title: "[翻譯] 什麼是 /r/changemyview"
category: 搬运
date: 2020-10-25 09:54:24
updated: 2020-10-25 09:54:24
tags:
---

[TOC]

<!-- TEASER_END -->

# 什么是 /r/changemyview？ (What is /r/changemyview?)

CMV (ChangeMyView，改变我的观点) 是致力于实现文明论述的一个子 reddit 板块。它围绕着“要想解决我们的差异，首先要理解差异”这样的理念而建成。我们相信有效的沟通需要尊重及开放，而对什么确信无疑的态度是达成理解的敌人。

CMV is a subreddit dedicated to civil discourse, and is built around the idea that in order to resolve our differences, we must first understand them. We believe that productive conversation requires respect and openness, and that certitude is the enemy of understanding. 

也是因此，CMV 是您发表关于一个愿于改变的观点的最好场所。我们并不想要攻击性的辩论，也不想鼓励评判，而是希望能够帮助各人理解相互的差异面。

That's why CMV is the perfect place to post an opinion you're open to changing. We're not looking to host aggressive debates, or encourage judgement, but help each other understand different perspectives. 

## 什么是“观点”？ (What is a 'view'?)

“观点”（*名词性*的 view）的一种定义是：“一种个人的看待某事物的方式；一种态度或主张”；而“观”（*动词性*的 view）的一种定义是：“看或察看”。

One definition of 'view' (*noun*) is "a particular way of considering or regarding something; an attitude or opinion," and one definition of 'view' (*verb*) is "to look at or inspect."

两种定义都对在这里发表内容的解释有帮助，因为思考词语字面上的含义是有益的。我们不一定被我们的观点所决定，但我们的观点使我们做出的事情，以及这与观点之间的分离，还是有帮助的。

Both definitions are helpful in explaining what is posted here, as it can be useful to consider the literal meaning. We aren't necessarily defined by our views, but what our views lead us to do, and so a degree of detachment can be useful.

在物理世界中，取决于您的位置，同一物体看起来可能会有不同。相似地，您的对于某一议题的立场可能与您独特的背景以及经历有关。

In the physical world, an object can look different depending on where you stand. Similarly, your standpoint on an issue can depend on your unique background and experiences. 

一个观点可能是由说理以及结论构成的，而这两部分可以各自作为独立的部分来对待。

A view can consist of reasoning and conclusion, which can be treated as distinct parts.

## 什么是“改变”？ (What is a 'change'?)

改变（*动词*）的定义是：“（使之）变得不同。”

The definition of 'change' (*verb*) is "make or become different."

接着上一部分，我们也就相信观点的改变仅指一种新的视角。也许，在物理上观察某物的例子里，您向一旁踏了一步又或是几步，或者您走动了而今站在物体的身后了。又或许，您没有“移动”，但是这个物体对您看起来有所不同了，因为新的光照。

Following on from the previous segment, we therefore believe that a change in view simply means a new perspective. Perhaps, in the example of literally looking at something, you've taken a step to the side; or a few steps; or you've moved around and now stand behind it. Maybe you haven't 'moved', but it looks slightly different to you now; in a new light.

观点的改变不需要是一个反转。它可能是轻而间接的，或也可能是在全新的轴线上发生的。

A change in view need not be a reversal. It can be tangential, or takes place on a new axis altogether.

## 谁可以投稿？ (Who can post?)

任何人都可以在此处发帖，只要有着开放的心态并且希望考虑到其它的视角。

Anyone can post here so long as they have an open-mind and are looking to consider other perspectives.

语言可能会被误解，尤其是涉及“观点”与“改变”的时候，所以我们尽可能在此理清。您不需要*想着*转换到您观点的反面立场，但需要想着去获得什么、获得某种新的视角，才能在这里发表。

Words can become misunderstood, especially when it comes to 'view' and 'change', so we'll do our best to clarify here. It's not a requirement that you *want* to hold the 'opposite' opinion to yours, but that you wish to gain something; some kind of new perspective, by posting here. 

**若您的观点没能改变，这** **不会** **导致您的帖子被移除。但是，表现出封闭的心态的行为有可能会导致移除。**我们理解未能被说服和封闭的心态是不一样的。

**Failing to have your view 'changed'** ***won't*** **result in removal of your post. Conducting closed-minded behaviour, however, might.** We also understand that remaining unconvinced is not the same as being closed-minded.

引用我们一位仲裁员（/u/garnteller）的话：

To quote one of our mods, /u/garnteller:

> 在我的经验中我们有三种发帖用户：
>
> 1) 真正愿意改变的，希望能够认真探查自己的观点并发展出更有细微差异或是完全不同于前的理解的人。
>
> 2) 来这里临时演说，而希望着传播自己观点的人。
>
> 3) 自认为持着开放的心态，但却在面对外界时又撤回到原来的观点的人。
>
> 很明显，第一种人完全融入 CMV，而第二种人则不。
>
> 第三种就更微妙了。
>
> 我认为考虑到这点很重要：第三种人虽然不可能在观点上做到 180 度的大转弯，但 CMV 可能可以像慢性毒药一样地作用。下一次贴主在面对某事物，而 TA 又将此事物的一些反论熟记于心、打算高谈阔论、回到原先对自己观点的狂热时，TA 们可能会注意到评论者所指出的一些生活中的实例。
>
> 老生常谈，重要的观点就像货船，转弯掉头慢。

> In my experience we have three types of posters:
>
> 1) Those who are truly open to change, looking to seriously probe their view and develop a more nuanced (or completely different) understanding.
>
> 2) Those who have come to soapbox and are looking to just spread their view.
>
> 3) Those who think they are open-minded, but when confronted will retreat back to their original view.
>
> Obviously, the first type belongs in CMV, and the second doesn't.
>
> The third type is a lot trickier.
>
> I think it's important to consider that while people in that third group are unlikely to do a 180 on their view, CMV can be like a slow-acting poison. The next time the OP is about to spout off on something they very well might remember the counter arguments they heard and pull back on the fervency of their belief. They might notice real life examples that were pointed out by commenters.
>
> Long held, important opinions are like a cargo ship - slow to turn around.

## Using CMV in Education

Some teachers/professors have used CMV as a classroom assignment. We very much welcome this, and appreciate being made aware in advance. Please [contact us](http://www.reddit.com/message/compose?to=%2Fr%2Fchangemyview) if you intend to use the subreddit for this purpose. All we need to know is the name of the teacher/professor, school, and the class. Any student using a new account or a throwaway account can then message the mods, identify the class, and we'll approve the posts. This is on the condition that the purpose of the assignment is for the student to submit one of their own, genuine views, and be open to it changing. 

**We created a [PDF guide](https://drive.google.com/file/d/0B_BhzG9qFmcja0VxU01yM1l6RDQ/view?usp=sharing) that we recommend handing out or emailing to your students.**

*Please don't pretend to hold a view as OP in order to gather counter-arguments for a paper or essay. Our members assume you're here in good faith and that shouldn't be taken advantage of.*

[[More Info](https://www.reddit.com/r/changemyview/wiki/teaching)]

---

# Info

### [Rules](http://www.reddit.com/r/changemyview/wiki/rules)

* Detailed explanations for the subreddit rules can be found here. All users are strongly encouraged to familiarize themselves with these rules before posting.

### [The Delta System](https://www.reddit.com/r/changemyview/wiki/deltasystem)

* Read all about our delta system and its administrator, /u/DeltaBot.

### [Deltaboards](https://www.reddit.com/r/changemyview/wiki/deltaboards)

* Daily, Weekly, Monthly, and Yearly Deltaboards.

### [Anti-Delta Approach](http://www.reddit.com/r/changemyview/wiki/antidelta)  

* Common pitfalls that tend to work against you. This will make you better at earning deltas and genuinely changing views!

### [The CMV Podcast](https://www.reddit.com/r/changemyview/wiki/podcast)

* A list of links to all episodes of the Change My View Podcast!

### [Teacher/Professor and Student Guide](http://www.reddit.com/r/changemyview/wiki/teaching)

* We've had a number of teachers/professors and students use CMV very productively. This guide covers what we need to make the assignment work well on our forum.

### [Academic Research of CMV](https://www.reddit.com/r/changemyview/wiki/research)

* CMV has been the subject of some academic research. Links inside.

### [Meta Mondays](https://www.reddit.com/r/changemyview/wiki/metamondays)

* Sometimes, on a Monday, we host a meta post for the CMV community to discuss their experiences with the subreddit.

### [Moderator Standards and Practices](http://reddit.com/r/changemyview/wiki/ModStandards)

* What can you expect from the moderators of /r/changemyview? This page describes how we moderate this subreddit.

### [Similar Subreddits](http://www.reddit.com/r/changemyview/wiki/subreddits)  

* Take a look at subreddits that are similar in depth and content.

### [Guidelines](http://www.reddit.com/r/changemyview/wiki/guidelines)

* FAQs, etiquette, and more.

### [Argumentation Tips](http://www.reddit.com/r/changemyview/wiki/argumentation)

* A list of questions and answers to help you improve your (friendly) debating skills!

### [Project WATT](https://projectwatt.com/)

* A research project in partnership with CMV: "Project WATT (What Are They Thinking?) is an experimental publication to collect a wide variety of perspectives on a wide variety of issues. It is made up of brief yet nuanced articles written and edited by the public."
