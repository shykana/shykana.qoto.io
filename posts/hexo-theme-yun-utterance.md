---
title: Hexo Yun 主题使用 Utterance 管理评论
category: 工具
date: 2020-08-02 00:49:56
updated: 2020-08-02 00:49:56
tags:
---
转移到静态博客之后在想评论的事情，从云游君大佬的 [第三方评论系统之我见](https://www.yunyoujun.cn/share/third-party-comment-system/) 里大致了解了一点，然后后来查到个 Utterance 先用上瞧瞧。似乎还没问题。
使用 Chrome 测试没问题，但是我日用的火狐似乎有时会出 CORS 错误，访问不了 `api.github.com`。大概是插件的问题，我之后再试一试。

Utterance 的官方配置很简单：[https://utteranc.es/](https://utteranc.es/)，GitHub 上设置好了再在博客里加段东西 就行了。往 [hexo-theme-yun](https://github.com/YunYouJun/hexo-theme-yun) 里加应该也挺简单，我不熟悉 Hexo，不知道怎么静态加入内容，暂时写了段 js 在页面加载时才把代码插进页面。

~~只不过一个 GitLab 蹭 CI/CD 的站用 GitHub Issues 来管理评论怎么有种奇妙的感觉~~
