---
title: Girls Page Setup
category: 工具
date: 2020-08-04 18:27:31
updated: 2020-12-29 12:08:31
tags:
---

[hexo-theme-yun](https://github.com/YunYouJun/hexo-theme-yun) 这个主题有个特别的页面：[页面配置|Girls](https://yun.yunyoujun.cn/guide/page.html#girls)。白用白不用嘛，就打算弄个诺贝尔奖女性获奖者名单上去。
也算是一点小小的反抗。

现在从 hexo 搬迁至 nikola 了，但是并不想把这个页面抛荒，于是自己写了 html 和样式，现在看着还行。
[/pages/girls/](/pages/girls/)

（对了，今年诺奖也出了，争取找点时间来更新一下

## 抓取过程

~~但是好麻烦啊，我写代码又垃圾~~

反正没打算弄成可复用的，最后大概路径是：
1. 到官方的 [Nobel Prize awarded women](https://www.nobelprize.org/prizes/lists/nobel-prize-awarded-women) 页面；
2. 打开 F12 开发者工具，大概在控制台弄了以下的代码：
```
var article = document.getElementById("nobel-middle-col");
var links = Array.prototype.slice.call(article.getElementsByTagName("a"))
    .filter(function(link) { return link.href.includes("facts"); });
var all = [];
for(var i in links) {
  var href = links[i].href;
  if(href) {
    (function(i, href) {
      fetch(href).then(function(response) {
        return response.text();
      }).then(function(html) {
        var parser = new DOMParser();
        var doc = parser.parseFromString(html, "text/html");
        var img = doc.getElementsByTagName("picture")[0]
            .getElementsByTagName("source")[0].getAttribute("data-srcset");
        var reasons = links[i].parentElement.innerText;
        var prize = reasons;
        if(reasons.indexOf("\n") != -1) {
          prize = reasons.substring(0, reasons.indexOf("\n"));
        }
        var person = {
          name: links[i].innerText,
          avatar: img,
          reason: reasons,
          url: href,
          from: prize
        };
        all.push(person);
      });
    })(i, href);
  }
}
```
3. 然后在控制台`console.log(all)`，复制变量到在线的随便哪个 JSON 转 YAML 转换器那里转换，把转换结果放回 girls 页面里。

顺便提一句，其实诺贝尔奖官网似乎提供了一些 API 可以查询相关的很多很多信息。但是 API 查不到图片，不符合我的要求，所以就没用。

Reference:
Nobel Prize awarded women. NobelPrize.org. Nobel Media AB 2020. Tue. 4 Aug 2020. <https://www.nobelprize.org/prizes/lists/nobel-prize-awarded-women> 
