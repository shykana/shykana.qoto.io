.. title: LibGDX？
.. slug: libgdx
.. date: 2021-09-12 10:25:01 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

####
安装
####

一些兼容配置
############

（写作时的 LibGDX 版本为 1.9.10.1）

Android
=======

IntelliJ
--------

我使用的是 IntelliJ ，针对 Android 平台有一个选项可能需要更改：

-> 左上角 File -> Langauges & Frameworks -> Android (experimental) -> Gradle -> Only sync the active variant

把 `Only sync the active variant` 这个选项取消勾选。

Android SDK 31
--------------

https://stackoverflow.com/questions/68387270/android-studio-error-installed-build-tools-revision-31-0-0-is-corrupted

我用的是 ArchLinux-CN 源的 SDK，安装在 ``/opt/android-sdk`` 目录下，可以这样解决 ``dx`` 程序缺失的问题：

.. code:: console

    $ sudo ln -s /opt/android-sdk/build-tools/31/d8 /opt/android-sdk/build-tools/31/dx
    $ sudo ln -s /opt/android-sdk/build-tools/31/lib/d8.jar /opt/android-sdk/build-tools/31/lib/dx.jar

兼容 Java 8
-----------

Java 8 太好用了，而且很多库也放弃了对更老的 JDK 的兼容，但是 Android 应用还是要兼容一下老设备。可以在 ``android/build.gradle`` 里加上兼容选项：

.. code::

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

（和 ``defaultConfig`` 这类的并列。）

HTML
====

FreeType
--------

https://github.com/intrigus/gdx-freetype-gwt

使用上面的库来兼容 HTML。现在的 LibGDX 版本已经是 1.10.0 了，上面这个库还没有相应的版本。我暂时用的是 1.9.10.1 版本，有问题再来记录。

JDK 8
=====

https://stackoverflow.com/questions/55847497/how-do-i-troubleshoot-inconsistency-detected-dl-lookup-c-111-java-result-12

LibGDX 似乎只兼容 JDK 8 （JDK 16 不行，JDK 11 没法编译）。安装 OpenJDK-8 再到 `File -> Project Structure -> Project -> Project SDK` 里把 JDK 版本改过来。
