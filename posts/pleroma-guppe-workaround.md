---
title: Pleroma 使用 gup.pe 的权宜之计
category: 服务器
tags:
  - Pleroma
date: 2020-11-01 15:38:14
updated: 2020-11-01 15:38:14
---

本文是这个嘟文 [Pleroma 使用 gup.pe 的权宜之计](https://wuppo.allowed.org/notice/A0kN21E6oIfFJ9XEI4)，可能之后的一些改动会在这里更新。

现在 pleroma 帐号还是关注不上 gup.pe 的群组号，原因似乎是 \[1\] 中 umonaca 提到的 http 头的问题。刚刚测试了一下，这个问题可以通过使用 nginx 来手动设置 http 头来解决：
首先，打开 pleroma 的 nginx 配置文件（按官方教程安装的是 /etc/nginx/sites-enabled/pleroma.conf）；
文件里有一段类似这样的内容：

```
location / {
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $http_host;
        ......
}

```

把这段内容全部复制（复制的内容里应只有一个“}”），粘贴到这一段的那个“}”花括号后面，现在这部分大概看起来像是这样：

```
location / {
        ......
}

location / {
        ......
}
``` 

将其中一个 location 行修改：
```
location / {
        ......
}

location ~ inbox$ {
        ......
}
``` 

在“location ~ inbox$ {”对应的那一块的里找到 `proxy_pass_request_headers on;` 这一行，在这行下面插入：
```
proxy_set_header Accept "application/activity+json";
```

现在这一部分应该看起来像：
```
location / {
        ......
}

location ~ inbox$ {
        ......
        proxy_pass_request_headers on;
        proxy_set_header Accept "application/activity+json";
        ......
}
```

保存退出，用 `sudo nginx -s reload` 或者 `sudo systemctl restart nginx` 重新加载配置。这个时候应该就可以正常关注 gup.pe 帐号了。

（但是不知道会不会有什么副作用。 :blobcatnotlikethis: ）
（不知道什么时候 gup.pe 会把 header 改过来。）
（还有顺便安利一下 QOTO 推出了群组服务器 https://groups.qoto.org ，和 gup.pe 差不多，但是需要手动创建群组，创建人可以编辑群组的简介并对关注者正常进行 block 等操作，也可以像正常帐号一样上锁来手动批准关注。）

\[1\]: [https://github.com/wmurphyrd/guppe/issues/21#issuecomment-716452245](https://github.com/wmurphyrd/guppe/issues/21#issuecomment-716452245)
