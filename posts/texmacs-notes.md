<!--
.. title: TeXmacs 笔记
.. slug: texmacs-notes
.. date: 2020-12-30 19:44:16 UTC+08:00
.. tags: TeXmacs, LaTeX, 数学
.. category: 工具
.. link: 
.. description: 
.. type: text
-->

[TOC]

暂时是 [这个嘟文](https://wuppo.allowed.org/notice/A2itL0OHhrviJxS2XA) 的存档，之后可能不时补充一下。

<!-- TEASER_END -->

我个人是真的记不住 LaTeX 相关的东西……
向像我这样记不住的同学推荐一下 texmacs？
也有学习成本，而且十分卡顿，但是个人认为输入方法以及快捷键还是比较符合直觉的。（配合 emacs 快捷键体验还行。）就是比较废 tab 键： :ablobwobwork: 

## 排版

| 输入 | 显示 |
|:--- | ---:|
| `$` | 行内公式 |
| `Alt+$` | 单独公式 |
| `Alt+&` | 多行公式 |

另外，`Alt+1/2/3/4` 可以输入相应层级的标题。

## 公式输入

比如：（按 `$` 或 `Alt+$` 或 `Alt+&`公式输入框后）

1. 乘法相关符号基本上：`*` 再 n 个 `tab` 就可以出来
　其它形状或功能相似或英文首字母重合的符号也类似

2. 在文字上方的修饰符号： `Alt` + 相应符号
　（如：`Alt+_` 为下划线，`Alt+-` 为上划线（bar），`Alt+~`/`Alt+'`/`Alt+"`/``Alt+` `` 也相应）

3. 输入两个 `R` 会变成 ℝ ，其它同理。

4. `@` 输入一个圆，`@`再按`/` 会输入 ∅ ，`@`再按`-` 会输入 ⊖ ，`@`再按`+` 会输入 ⊕

5. `A`再`tab` `tab` 输入 ∀ ，`E`再`tab` `tab` 输入 ∃ ，`V`再`tab` `tab`输入 ∇ ，希腊字母系列的是相应的英文首字母再 `tab`（例外可能是`j`再`tab`输入 θ ）。

6. 要记忆的一些可能有：
`Alt+S`：sqrt，`Alt+F`：fraction

7. 还要记个 `Shift+F5`：一些大号符号（上标可以写到头顶上去那种），比如：
　`Shift+F5`再`I`：integral：∫
　`Shift+F5`再`I`再`I`再`I`：integral\*3：如图中1.
　`Shift+F5`再`S`：sum：Σ
　`Shift+F5`再`O`：contour integral：∮
　`Shift+F5`再`O`再`O`再`O`：\*3：如图中2.
　`Shift+F5`再`P`：pi/product：Π

如果找不到的也可以在菜单栏里翻一翻，一般的符号都会说明相应的输入方法。

但是生态肯定没有 TeX 那边那么好，可以当作 TeX 的公式前端然后导出到 TeX 里去？（有导出功能，导出出的文件里面带有一些宏，不知道会不会影响一些编辑器。）

厚着脸皮 @AcademyWiki@gup.pe （当然最后因为服务器的问题没 at 上）

## 插入其它

### 图片

似乎对普通的 `png, jpg, gif` 这种图片支持比较差，插入时需要自己手动精确输入长度宽度……

我一般能够导出为 `pdf` 的图片都导出为 `pdf`（如一些统计图），然后再插入到文档中。

## 参考文献引用

### 配合 Zotero 使用

Zotero 有一个插件： [zotexmacs](https://github.com/slowphil/zotexmacs) ，安装按着来就行。

~~但是对我来说其实最麻烦的是要搞懂这个系统。~~

**插入参考文献的 library：**

0. 准备好参考文献的 BibTeX 文件（`.bib`），并保证 BibTeX `.bib` 文件与 TeXmacs `.tm` 文件处于同一个目录；
1. 通过 `Insert -> Automatic -> Bibliography` 插入参考文献列表；
2. 插入后，你应该可以看到窗口里可以输入 BibTeX 文件的文件名，输入文件名（如图，我输入了 `lib.bib`）；![Aux: bib, Style: tm-plain, File-name: lib.bib](/images/texmacs-bib-insert.png)

**插入文献：**

使用 zotexmacs 插入文献，插入后可能会发现显示的是 `[?]`，此时通过 `Document -> Update -> Bibliography` 更新文献，然后不出意料就可以了。

## TeX 兼容

顺便学一学 TeX。一些我还真的没有找到什么除了直接 TeX 命令以外的输入方法。

- `\underbrace`: 一个向下的大括号，一般是用来说明上方的公式部分用的那种。
