---
title: Manjaro 笔记本 Nvidia 显卡折腾 (待续)
category: 工具
tags:
  - Linux
id: '99'
date: 2019-10-01 19:22:56
updated: 2020-06-26 19:22:56
---

笔记本是 Nvidia 加 Intel 核显. 但是之前安装 Manjaro 的时候无论装开源还是闭源驱动都会黑屏, 之后干脆只装了个核显驱动, 碰都不碰独显. 现在无聊 (果然赶 deadline 的时候是最想摸鱼的时候), 所以试一下. 先以防万一记录下计算机信息: 神船K650D-G4E5 开始折腾之前:

```
$ sudo inxi -Fxxxz
System:
  Host: lil-manjaro Kernel: 5.2.11-1-MANJARO x86\_64 bits: 64 compiler: gcc 
  v: 9.1.0 Desktop: Xfce 4.14.1 tk: Gtk 3.24.10 info: xfce4-panel 
  wm: Compiz 0.9.13.1 dm: LightDM 1.30.0 Distro: Manjaro Linux 
Machine:
  Type: Laptop System: HASEE product: K650D v: N/A serial: N/A Chassis: 
  type: 10 serial: N/A 
  Mobo: HASEE model: K650D serial: N/A UEFI: American Megatrends 
  v: 1.07.06RHAS2 date: 09/21/2018 
CPU:
  Topology: Dual Core model: Intel Pentium Gold G5400 bits: 64 type: MT MCP 
  arch: Kaby Lake rev: A L1 cache: 128 KiB L2 cache: 4096 KiB 
  L3 cache: 4096 KiB 
  flags: lm nx pae sse sse2 sse3 sse4\_1 sse4\_2 ssse3 vmx bogomips: 29576 
  Speed: 1135 MHz min/max: 800/3700 MHz Core speeds (MHz): 1: 1157 2: 1183 
  3: 1194 4: 1166 
Graphics:
  Device-1: Intel vendor: CLEVO/KAPOK driver: i915 v: kernel bus ID: 00:02.0 
  chip ID: 8086:3e90 
  Device-2: NVIDIA GP108M \[GeForce MX150\] vendor: CLEVO/KAPOK driver: N/A 
  bus ID: 01:00.0 chip ID: 10de:1d10 
  Display: server: X.Org 1.20.5 driver: intel compositor: compiz v: 0.9.13.1 
  resolution: 1920x1080~60Hz, 1366x768~60Hz 
  OpenGL: renderer: Mesa DRI Intel UHD Graphics 610 (Coffeelake 2x6 GT1) 
  v: 4.5 Mesa 19.1.5 compat-v: 3.0 direct render: Yes 
Audio:
  Device-1: Intel Cannon Lake PCH cAVS vendor: CLEVO/KAPOK 
  driver: snd\_hda\_intel v: kernel bus ID: 00:1f.3 chip ID: 8086:a348 
  Sound Server: ALSA v: k5.2.11-1-MANJARO 
Network:
  Device-1: Intel Wireless-AC 9560 \[Jefferson Peak\] driver: iwlwifi 
  v: kernel port: 5000 bus ID: 00:14.3 chip ID: 8086:a370 
  IF: wlo1 state: down mac: <filter> 
  Device-2: Realtek RTL8111/8168/8411 PCI Express Gigabit Ethernet 
  vendor: CLEVO/KAPOK driver: r8168 v: 8.047.02-NAPI port: 3000 
  bus ID: 02:00.1 chip ID: 10ec:8168 
  IF: enp2s0f1 state: up speed: 100 Mbps duplex: full mac: <filter> 
Drives:
  Local Storage: total: 1.14 TiB used: 63.24 GiB (5.4%) 
  ID-1: /dev/sda vendor: HGST (Hitachi) model: HTS721010A9E630 
  size: 931.51 GiB speed: 6.0 Gb/s rotation: 7200 rpm serial: <filter> 
  rev: A3U0 scheme: GPT 
  ID-2: /dev/sdb model: Phison SATA SSD size: 238.47 GiB speed: 6.0 Gb/s 
  serial: <filter> rev: 61.2 scheme: GPT 
Partition:
  ID-1: / size: 234.24 GiB used: 20.66 GiB (8.8%) fs: ext4 dev: /dev/sdb2 
  ID-2: /home size: 592.58 GiB used: 35.50 GiB (6.0%) fs: ext4 
  dev: /dev/sda8 
  ID-3: /var size: 78.24 GiB used: 7.08 GiB (9.1%) fs: ext4 dev: /dev/sda7 
Sensors:
  System Temperatures: cpu: 54.0 C mobo: N/A 
  Fan Speeds (RPM): N/A 
Info:
  Processes: 191 Uptime: 10h 59m Memory: 15.55 GiB used: 2.63 GiB (16.9%) 
  Init: systemd v: 242 Compilers: gcc: 9.1.0 Shell: bash (sudo) v: 5.0.9 
  running in: xfce4-terminal inxi: 3.0.36 

$ mhwd -li
> Installed PCI configs:
--------------------------------------------------------------------------------
                  NAME               VERSION          FREEDRIVER           TYPE
--------------------------------------------------------------------------------
           video-linux            2018.05.04                true            PCI
         network-r8168            2016.04.20                true            PCI


Warning: No installed USB configs!
```

另外, 其实笔记本还外接了个 VGA 屏幕, 但感觉没啥问题, 就先留着了. 然后就先跟着 [https://blog.csdn.net/weixin\_42205310/article/details/81905293](https://blog.csdn.net/weixin_42205310/article/details/81905293) 来走走试试. 不敢直接试, 就先重启, GRUB 里加上 `systemd.unit=multi-user.target` 的内核参数, 命令行模式下操作. 诶呀重启一下果然还是不行, 命令行模式进入之后把 `bumblebeed` 给禁用掉就开得了机了 (`sudo systemctl disable bumblebeed`). 等之后有空看看上 Manjaro 论坛上问问大佬.

### 最终选取方法：Optimus
最后也没有去问大佬，就参考了 Manjaro 论坛里的教程 [Guide: Install and configure optimus-manager for hybrid GPU setups (Intel/NVIDIA)](https://forum.manjaro.org/t/guide-install-and-configure-optimus-manager-for-hybrid-gpu-setups-intel-nvidia/92196) 自己装上好像就行了。
![no switching method](/images/optimus-no-power.png)
但是我的 optimus 在切换时里不能选取任何的电源管理措施（Switching method 以及 PCI reset 只能够选 None/No，否则就直接 kernel panic，可能和垃圾 BIOS 有关。反正切到 Nvidia 之后费多点电就费多点嘛，我也懒得继续折腾了。
