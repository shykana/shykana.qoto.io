---
title: Linux 日用软件记录 (有空再补)
category: 工具
tags:
  - Linux
id: '96'
date: 2019-09-21 00:16:27
updated: 2019-09-21 00:16:27
---

#### 学习

Marvin: ChemDraw 替代
VESTA: 绘制晶胞
TeXmacs: 写论文（但是参考文献管理有点麻烦）
GeoGebra: 简单数学绘图

#### 效率

gThumb: 管理图片、表情包
Thunderbird: 除邮件之外，使用支持iCalendar、CalDAV的提供商管理日程

最近在找管理文献的软件
