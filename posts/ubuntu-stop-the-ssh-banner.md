---
title: Ubuntu禁用登陆时显示系统信息的banner
category: 服务器
tags:
  - 笔记
id: '150'
date: 2020-05-12 18:20:04
updated: 2020-05-12 18:20:04
---

来源：[https://ubuntuforums.org/showthread.php?t=1449020](https://ubuntuforums.org/showthread.php?t=1449020)

> That irritates the heck out of me too. It is controlled by the "pam\_motd" module. You can disable the motd display (and potential delay) by editing these two files: /etc/pam.d/login, /etc/pam.d/sshd, and comment out the line that has "pam\_motd" in it. I only tested the secure shell change since gdm is running, but it worked. Tested on Lucid Beta.

也就是说，修改 /etc/pam.d/login 和 /etc/pam.d/sshd ，把里面的含有 pam\_motd 的行都注释掉就行了。
