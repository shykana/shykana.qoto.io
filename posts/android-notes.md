---
title: 安卓折腾笔记 (不定期缓慢更新)
category: 工具
tags:
  - Android
id: '90'
date: 2019-09-01 11:28:48
updated: 2019-09-01 11:28:48
---

之前折腾的基本忘了...

#### Google 全家桶

[OpenGapps.org](https://opengapps.org/) 这个要在刷好 LineageOS 之后的第一次开机之前刷好, 否则会引发一系列的报错 (eg. Google Play/Google/Photos 已停止).

#### 错误安装 Google 全家桶

首先安卓有好几个分区, vendor, system, data, sd卡. 全家桶应该主要更改了 system 分区, 而一般来说 system 分区是不会被修改的 (除非 xposed, magisk, root 等). 所以理论上重新刷一遍 ROM 就可以了, 只清 system 分区, 其他保留数据. 之后再重装一遍 magisk 之类的就可以. 但是我刷了之后现在还是有挺多问题, 基本什么应用都开始闪退了... 待解决... (作死操作请勿模仿) 补充: 后来一个一个重装过一遍好像就好了...不知原因...
