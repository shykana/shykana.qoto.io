.. title: R语言笔记
.. slug: r-notes
.. date: 2021-03-09 20:49:20 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

R语言笔记。（我讨厌脚本语言。:(

差不多是数据分析的常用语言之一？

########
语法简介
########

语句
####

一句一行，可以用 ``;`` 在同一行中分隔多句。
``#`` 后跟注释。

标识符
######

以字母开始，可以跟有 ``[A-Za-z0-9_.]`` 等。

数据
####

以向量为基础，如 ``1`` 会被理解成一维向量。

字面常量与数据构造
==================

* ``T``, ``TRUE``, ``F``, ``FALSE``
* 向量： ``c(1, 2, 3)``, ``1:3``
* 数字： ``0xff``
* 字符串： ``"string"``, ``'string'``
* data frame：

  .. code:: R

    data.frame('Col1Name'=col1Data, 'Col2Name'=col2Data, ...)
  
计算操作
========

* 加减乘除不用说了，会自动增大数据精度，但不是无限精度；
* ``%%`` 取余。
* ``>=``, ``==``, ``!=``
* ``&``, ``|``, ``&&``, ``||``

向量/矩阵操作
-------------

下标从 ``1`` 开始，用 ``[index]`` 取下标。下标可以使用向量，如 ``x[1:3, c(1, 3)]``

**从向量创建矩阵** ：默认一列一列地复制到里面去。

data frame
----------

可以取列的下标，如 ``x["Col1Name"]``.

可以用 ``$`` 符号，如 ``x$Col1Name``.

赋值语法
########

.. code:: R

    var1 = 1
    var2 <- 2
    3 -> var3

流程语法
########

* for loop: 

  .. code:: R

    for (i in 1:100) {
      print(i)
    }

* if:

  .. code:: R

    if (cond1) {
      print('do something
    }

函数定义
########

全部是通过匿名函数定义的。

.. code:: R

    func = function(arg1, arg2) {
      return(returnedValue)
    }

全局函数
########

* ``is.integer(x)``
* ``is.numeric(x)``
* ``as.integer(x)``
* ``c(x, y)`` : concatenate 连接得到向量
* ``print(x)``
* ``matrix(x [, nrow = 3 [, ncol = 3]] [, byrow = T])``
* ``typeof(object)``
* ``power.sum(x, power)``

  这个似乎有点东西。取得是向量里面的每个数据的类型。

* ``class(object)``
* ``sum(object)``


* ``getwd()`` : get working directory
* ``setwd(str)``
* ``read.csv(name)``
* ``head(data)``


* ``plot(x, y, type='o')``


* ``install.packages('ggplot2')``
* ``library(ggplot2)``

* ``curve(x^2)`` : 是的，就是这个屁语法。

  正经一点的话比如 ``curve(func, fromX, toX, n = pointNumber, xlab = label, ylab = ylabel, main = graphName)`` ，里面的 ``func`` 需要是能够处理向量参数的函数，不能的话可以套一层 ``Vectorize`` （见下）。

* ``with(data, boxplot(col1))`` : 再来一个屁语法。等效 ``boxplot(data$col1)`` 。
* ``Vectorize(func)`` : 不懂为什么这个函数就首字母大写了。接收一个只能处理单值的函数，返回对应的能够接收向量参数的函数。返回的函数会把原来的函数应用到向量的每一个分量上。

所有函数都是全局函数
====================

恩，所以不列举了。

（那个所谓的 ``.`` 看似是面对对象或者是命名空间，但它只是装饰啊（不，应该说点本身是名字的一部分，和对象啊对象的成员啊一点关系没有。））

特定功能代码集合
################

查看帮助文档
============

.. code:: R

    ? barplot

读取CSV
=======

.. code:: R

    data = read.table("data.csv", header=TRUE, sep="\t", fileEncoding="UTF-8")

保存输出图表为PDF格式
=====================

.. code:: R

    pdf(paste(col, ".hist.pdf", sep=""))
    hist(data[[col]], main=col, xlab=col, ylab="Frequency")
    dev.off()

就，R个操作系统吧，有各个“设备”的驱动。大概是按设备管理的思路来的。

绘画图表
========

.. csv-table:: 天哪

    "柱形图","
    .. code:: R

        barplot(height, width, main=""title"", xlab=""..."", ylab=""..."")"
    "直方图","
    .. code:: R

        hist(data, breaks=width, probability=TRUE, col=rainbow(20))"
    "箱型图","
    .. code:: R

        boxplot(data, names=c("""", """"))"
    "散点图","
    .. code:: R

        plot(x, y)
        plot(x, y, pch=16, col=ifelse(x==1, ""RED"", ""BLUE"")"

绘画数学图形
============

.. code:: R

    curve(sin(x))
    x = seq(0, 10, 0.01)
    y = sin(x)
    z = cos(x)
    matplot(x, cbind(y, z), type="l")
    func = function(x) {
        if(x <= 0.5) {
            return(0)
        } else {
            return(1)
        }
    }
    plot(Vectorize(func), 0, 1, n = 10001)

积分
====

.. code:: R

    integration = integrate(vectorizedFunc, from, to, subdivistions = 100000)
    print(integration$value)
    print(integration$abs.error)

随机以及抽样
============

.. code:: R

    rnorm(10, mean=0, sd=1)
    runif(10, min=5, max=10)

.. code:: R

    sample(1:10, size=5, replace=TRUE)
    sample(c(TRUE, FALSE), size=10, prob=c(0.7, 0.3), replace=TRUE)

#######
Rstudio
#######

* R console: 
* R script: 可以与 console 同步，也可以部分执行；
* R markdown: 略？
