---
title: "[翻譯] 改觀（Change My View）：反 delta 舉措（緩慢更新中）"
category: 搬运
date: 2020-11-28 12:21:07
updated: 2020-11-28 12:21:07
tags:
---

[TOC]

<!-- TEASER_END -->

原文請見：[Anti-delta Approach](https://www.reddit.com/r/changemyview/wiki/antidelta)

（渣翻，請見諒）

_____

# 反 delta 舉措
（Anti-delta Approach）

**或是說 “如何 *避免* 掙得 delta”**

---

（也見：[delta 系統詳解](http://www.reddit.com/r/changemyview/wiki/deltasystem)）

(See also: [the delta system explained](http://www.reddit.com/r/changemyview/wiki/deltasystem))

*言論自由可以實現很酷的事情，但它完全是建立在說服的基礎上的。在闡述好的思想時將辱罵與抨擊混進來就像是在烤好的完美三分熟的牛排裏埋入了刀片。我們希望反對方的觀點能夠以可口美味、讓人食慾大開的形態呈現，而這正是這些觀點* ***力量*** *的由來。*

*Free speech can do very cool things, but it's all based on persuasion. Putting insults and assaults into the delivery of a good idea is like grilling the perfect medium-rare steak and then shoving razor blades into it. We want opposing views to take the most palatable and appetising form they can, which is how they have* ***power.***

下面的所有技巧都建立在我們多年在 /r/changemyview 的仲裁的觀察上。我們見過很多的不同的方式，它們其實在解決同一個問題：怎樣讓某個人像你一樣思考。但是，比起告訴你 _要_ 做什麼才能獲得 delta，本條目從反面向你展示一些無疑會 *妨礙* 你獲得 delta 的途徑。

All of the techniques below are based on our observations after years of moderating /r/changemyview. We've seen lots of different ways of tackling what is essentially the same problem: how to get someone to think like you. But rather than tell you what you _should_ do to earn a delta, this entry takes the reverse angle and shows you what will almost certainly *prevent* you from earning one.

這些技巧裏的很多也會讓你的評論被仲裁員刪除，這些技巧會被標記爲 “ **[值得刪除]** ”。

Many of these techniques can also get your comments removed by the moderators, and these will be marked with the flag "**[Remove-worthy]**". 

少數技巧甚至會讓你在 /r/changemyview 被封禁，這些會被標記爲 “ **[值得封禁]** ”。

A very small number can even get you banned from /r/changemyview, and these are marked with the flag "**[Ban-worthy]**". 

這個教程會引用研究以及一些特定的情形。這些情形被我們用來建設你在網站側邊欄看到的短小的規則列表。我們也希望你那時可以明白我們爲何選擇了這些規則。

This guide will cite research and situations that we used to develop the short list of rules you see in our sidebar, and we hope that you'll then understand why we chose those rules.

請注意，/r/changemyview 的仲裁行爲很多，這是有意爲之的。對於每一個議題的雙方，我們都已經見過並且也移除過不守規則的發文了。我們 *永遠會恢復你修改後移除了粗魯、敵對發言的發文*，無論它的內容是政治的、宗教的、意識形態的還是話題性的。相信我們。你可以是愛德華·斯諾登、朱利安·阿桑奇或者是阿道夫·希特勒本人，你也可以提倡任何東西。*來說吧*，只要在你提起話題時做到禮貌。

Please note that /r/changemyview is a deliberately heavily moderated sub, and we've seen and removed rule-breaking posts on both sides of just about every issue there is. We will *always re-instate a post that you edit to remove any rudeness or hostility*, no matter its political, religious, ideological or topical content. Try us. You could be Edward Snowden, Julian Assange, or Adolf Hitler himself and advocate for anything. *Bring it*, just be polite when you do.

**把 CMV 看作是一次探討能否文明地改變哪怕是最極端的觀點的實驗。**如果你認爲移除評論違背了言論自由，那麼我們希望你考慮這樣一個與你的觀點成 90 度角的視角：言論自由包括了嘗試不同的言論形式的自由，以及創建用於測試這些言論形式的論壇的自由。

**Think of CMV as an experiment to see if it's possible to change even the most extreme views in a civil manner.** If you think comment removals are a violation of free speech, then we ask you consider a view that's 90^o from that: freedom of speech includes the freedom to try out different forms of speech, and the freedom to create forums that test them. 

記住，CMV 本身 *不是辯論的板塊* ，它首先是一個 *說服* 的板塊，而這裏的大多數話題以改變樓主對於主觀議題的個人觀點爲中心。辯論，對於 CMV 來說，只是可以達到這樣的目的多種工具之一。因此，我們鼓勵成員們使用 [說服的模式](http://en.wikipedia.org/wiki/Modes_of_persuasion)。在 CMV 中，使用 *訴諸權威*、*訴諸多數*、*訴諸情緒* 的論證並不被看作是謬誤，因爲這常常是成功的鑰匙，也是常常是樓主想要的——無論其是否知道。

Keep in mind that CMV is *not a debate sub* per se, it is a *persuasion* sub first, and most of the topics here centre on changing the OP's personal point of view about subjective issues. Debate, for CMV, is just one of many tools that can be used to this end. For this reason we encourage members to use the [modes of persuasion](http://en.wikipedia.org/wiki/Modes_of_persuasion). In CMV, it is not considered a fallacy to use *appeal to authority*, *appeal to popularity*, or *appeal to emotion*, since that is often the key to success and what the OP wanted--knowingly or not. 

所以如果你真的認爲樓主應該改變其觀點，但是想要讓你的努力付之一炬、讓其它人反而更加堅持己見，下面的技巧一定可以達成這個想法。

So if you genuinely think the OP should change their view, but want to sabotage your best efforts and make the other guy cling even stronger to their opinions, the following techniques are certain to accomplish this.

# 採取強硬的態度、完全對立的證據、好鬥的語調
（Come on strong and hard with contradictory evidence and a combative tone）

在你開始第一次的回應時，就馬上把你能找到的最好的課題、最好的投票、科學研究、學術論文拍對方臉上。證據越致命、越與對方觀點對立越好。此外，在你的語言裏賣弄一種好鬥的語調。例如：

   * “你明顯不知道 X……”
   * “顯然你沒讀過 Y……”
   * “小學生都知道 Z……”


As you come out of the gate for your first post, hit them with the best studies, polls, scientific research and academic papers you can find. The more damning and contradictory they are, the better. Furthermore, affect a combative tone in your language. EG:

   * "You obviously don't know about X..."
   * "Clearly you haven't read Y..."
   * "Even school-children know about Z..."

這會導致 [逆火效應](http://rationalwiki.org/wiki/Backfire_effect)，讓樓主以及其它用戶在其最好的精神防禦的掩護中埋藏得更深。雖然其判斷力可能更好，但 TA 們仍會這麼做，因爲你在使用火箭筒來回應 TA 們的噴嚏。

This will evoke the [Backfire effect](http://rationalwiki.org/wiki/Backfire_effect), making the OP or other user dive for cover behind their best mental defences. They will do this in spite of their better judgement, because you're answering a sneeze with a bazooka.

你對話的對象將是杏仁體——大腦中判斷“打還是逃”的中心——而不是更加理性的額葉，我們也注意到了更溫柔緩和地 **問問題** 來揭露對立的證據（“蘇格拉底反詰法”）的方法會更容易消化並爲新的視角所吸收。

You'll be talking to the amygdala--the "fight or flight" centre of the brain--rather than the more rational frontal cortex, and we've noticed that **asking questions** with more gentle and gradual revelation of contradictory evidence (the "Socratic Method") is easier to digest and assimilate into a new perspective.

[BBC 發佈的這篇文章](http://www.bbc.com/future/story/20141113-the-best-way-to-debunk-myths) 也提議了一種更好的方法：

> Lewandowsky 和 Cook 說理道，這些實驗顯示了與處在被錯誤引導的位置的人爭論的危險。如果你嘗試去揭穿某種想象，你可能最終反而增強了這種信念，增強了人們腦中的錯誤信息，而不是讓正確的信息去佔領。
> 你必須做的，TA 們認爲，應該是用有一定道理的可代替原解釋的另一解釋（你相信明顯是正確的一個解釋）來開始你的論證。如果你一定要提起某一個虛構想象，你只應該在明確說明你將要討論的是非真實的事情之後，才提起。

[This article published by the BBC](http://www.bbc.com/future/story/20141113-the-best-way-to-debunk-myths) also suggests a better way:

> Lewandowsky and Cook argue that experiments like these show the dangers of arguing against a misinformed position. If you try and debunk a myth, you may end up reinforcing that belief, strengthening the misinformation in people's mind without making the correct information take hold.
> What you must do, they argue, is to start with the plausible alternative (that obviously you believe is correct). If you must mention a myth, you should mention this second, and only after clearly warning people that you're about to discuss something that isn't true.

# 羞辱對方的智商
（Insult their intelligence）

   * “維基百科是你的朋友。”（言外之意：但凡查一下維基百科就懂的東西）
   * “你但凡有過孩子你都會明白。”


   * "Wikipedia is your friend."
   * "You'd understand if you ever had children."

用對方最細微的錯誤貶低對方，尤其是錯別字或者是偶然的無知，例如正在描述的是哪一代的女權運動、某一位歷史人物是阿拉伯人還是波斯人。拼命重複基本概念的解釋，例如市場供需，像是把對方當作小孩子。

Belittle them for the smallest mistakes, particularly typos or casual ignorance, such as which generation of the Feminist movement is being described, or whether a historical figure was Arab or Persian. Belabour the explanation of basic concepts, such as supply and demand, as if the other user was a child.

你也可以盤問對方並利用對方沒有經歷過的特殊經歷，如上大學、有孩子、在異國生活、參軍等等。雖然這些經歷對很多人都是很普通，但這還是會煽動你的對手去證明這，在某種程度上，是與話題無關的，尤其是重複這經歷比較困難或是比較昂貴的時候。

You can also interrogate and take advantage of any special experience they've missed, such as attending university, having children, living in a different country, enlisting in the armed forces, and so-on. Although many of these experiences are common to millions of people, it'll still incite your opponent to prove that this is irrelevant, somehow, especially if it's an experience that's difficult or expensive to replicate. 

記住 CMV 的目的是說服一個人，而不是在其它人眼中贏得一場遊戲。你可能贏得了聽衆，但輸掉了比賽，也輸掉了 CMV 建來的目的。

Remember that the point of CMV is to persuade a person, not win a game in the eyes of others. You might win the audience, but lose the match and fail at what CMV was built to do.

人們傾向認爲身爲人類的一個天賦是共情心以及對於自己未經歷的事物的理解。就比如作家們談論觀察家貓悄悄移動並想象被老虎跟蹤的感受。儘管可能從你生而優渥的視角來看很荒誕，但這不意味着你的對手不會被這種含沙射影的對其把握論證核心的能力的懷疑所侮辱。採取一種自視甚高的態度，你就會把對方變爲磚牆。

People like to think that a gift of being human is empathy and understanding of things they haven't personally experienced. Like when writers talk about watching a house-cat on the prowl and imagine what it's like to be stalked by a tiger. Even if this seems ridiculous from your privileged point of view, it doesn't mean your opponent won't be insulted by the insinuation that they're incapable of grasping whatever point lies at the heart of the argument. Cop a superior attitude, and you'll turn them into a brick wall.

# 使用敵對性的譏諷
（Use hostile sarcasm）

**[值得移除]**

**[Remove-worthy]**

這是羞辱智商的變種，但採取的是一種更不含蓄的形式。這會更有效地激怒對方，使之架起心理防禦，因爲對方基本不用思考就可以知道你是在貶低 TA。

This is a variation of insulting intelligence, but in a more explicit form. This raises hackles and mental defences even more effectively, because the other person doesn't have to think about it much to realise you're belittling them.

作爲敵對性的行爲，這符合 /r/changemyview 的規則二，很典型地，在報告後會被移除。

As hostile behaviour, it qualifies under Rule 2 of /r/changemyview, and is typically deleted upon report. 

# 說對方傻
（Call them stupid）

**[值得移除]**

**[Remove-worthy]**

   * “你就是個智障。”
   * “操，你也太蠢了。”


   * "You are completely retarded."
   * "God, you're so fucking stupid."

滿足你說對方傻的衝動就像是在公共場合自慰一樣。對你來說可能很舒服，但對其它所有人都很噁心，這也讓其它人力求與你產生更大分歧。

Satisfying your urge to call someone stupid is like masturbating in public. It may feel good to you, but it looks disgusting to everyone else and it just makes the other person work harder to find ways to disagree with you.

我們會移除這樣的評論因爲它們讀起來令人不愉快，也永遠不會對討論、目標、或是樓主有任何的積極貢獻。請私下而不是在我們的板塊下這樣做。

We remove comments like this because they're unpleasant to read and never make any positive contribution to the discussion, the target, or the OP. Please do this in private and not in our sub.

# 讓對方自殺 / 希望對方遭遇暴力
（Tell them to kill themselves/Wish violence upon them）

**[值得封禁]**

**[Ban-worthy]**



This is the ultimate expression of contempt, no matter what stage of the discussion it occurs in. If it's a top-level post, you want to suggest that even the act of coming to /r/changemyview to have the view changed is insufficient, and that just having the view in the first place should disqualify you from any further consideration by the human race.



Due to an event in the past, we will now immediately ban users who suggest or advocate violence to, or suicide of another user. Please understand that we don't have a choice: we _must_ take these very seriously.



Reddit's site-wide policy also has [this to say](http://www.redditblog.com/2014/09/every-man-is-responsible-for-his-own.html):



> 1. Actions which cause or are likely to cause imminent physical danger (e.g. suicides, instructions for self-harm, or specific threats) or which damage the integrity and ability of the site to function (e.g. spam, brigading, vote-cheating) are prohibited or enforced by “hard” policy, such as bans and rules.



# Compete to see who can out-pity the other



**[Remove-worthy]**



Use patronising language and assume an air of pity, or even remorse for the condition of your opponent. Use meta-arguments to dismiss their style, tone, or mentality. Some phrases you can use:



   * "I'll be charitable and give you the benefit of the doubt that you're not so [field-of-study] illiterate that you couldn't understand this concept."



   * "This is not uncommon for people like you. Pathetic, but normal."



   * "It must be awful to have a mind that can't give up a losing argument."



   * "I honestly feel really bad about leading you into something that you clearly can't comprehend, my apologies."



Both parties will tend to slip into the trap of trying to out-pity the other, and exchanges like this can continue for a long time without ever straying back to the original topic.



# Speak on the audience's behalf



   * "You've just disqualified yourself from arguing anymore."



   * "Now you've lost everyone's respect, you have no credibility here."



   * "We can all see through your obvious distortions."



The instinctive response by a spectator--who may be the OP or someone else who might have awarded a delta--is to reject self-appointed ambassadors of their feelings, even if they might have been accurate. Therefore, this technique can often backfire when the audience--in the act of instinctively rejecting you speaking for them--put more effort into looking for ways to agree with your opponent.



# Pretend that you are intellectually amused by their continuing responses



**[Borderline, may be removed if combined with other rudeness]**



   * "Please reply more just so I can watch some more of this."



   * "Oh do go on, I'm making more popcorn as we speak."



   * "Your posts are hilarious, please keep them coming."



   * "At this point I'm just giving you the opportunity to keep making a fool of yourself."



   * "I'm actually writing a paper on the mentality of people like you, and you're giving me so much great material." (Bonus points for pretending to be an anthropologist.)



This is so common to most people's learning experience with argumentation (from trying it themselves), that they now see straight through it. Pretending to be a scientist is even worse, and comes off as childish and flimsy.



# Declare that you're finished responding, several times



Announce, with fanfare, that you're "done" and shall no longer respond. Do this for several more exchanges. You can also announce, after an argument has gone on for several rounds, that you "realised long ago that you were a waste of time and it was pointless to continue," or that they "have ceased to be a valid counterpart." 



The more you do this in the same thread, the clearer it is that you're either easily provoked or only said it for effect. Making a habit of it can contaminate respect for your arguments on other threads.



# Say the person's persistence or attitude "proves" something about the topic



   * "That you keep this up proves that the feminist movement is intellectually bankrupt."



   * "This proves that you are stupid, and you have invalidated yourself as an opponent."



The difference between proof and evidence is brought up very frequently, especially on the Internet, so it's likely that everyone has already read articles or explainers about it several times, and are even getting tired of it. 



Plus, if you go about "proving" things left and right, especially when the connections are dubious at best, your audience will get "proof fatigue" and the word will lose what little impact it still has (outside of mathematics).



# "Find it interesting" that someone is having an argument with you



   * "I find it interesting that you're so passionate about this topic, could it be that you have a vested interest?"



   * "That you wrote about this at such length tells me something interesting about you. Are you a victim yourself?"



CMV is a sport for some of our subscribers, so of course they will dive into any topic whether they feel strongly about it or not. However, a prime way to piss them off is to allude to something about their personality or character--to "find it interesting"--just because they're replying to you with a counter-argument.



What usually follows an "interesting" discovery that someone is arguing on the Internet is an ad-hominem. This is not  against CMV's rules, but you will probably get a [Latin tutorial](https://yourlogicalfallacyis.com/) for a reply.



Worse, of course, is that this technique is intuitive for participants who have been recently blindsided or hit with a "wall of text" that makes a strong case against their weaker position, so even if this isn't actually your situation, the association in people's memory will work against you.



# Pick on the use or definition of "irony"



We've all heard English professors complain bitterly about a certain Alanis Morissette song, but lecturing about the use of the word "irony" or "ironic" is a pointless sideshow that makes you look petty. 



It tends to generate more frustration than enlightenment, since a healthy percentage of our subscribers--if not the general public--want to just use the word the way they've heard it (incorrectly) used by others, since "everybody knows what I mean".



What can we say? Language evolves, _literally_.



# Use the term "reading comprehension" anywhere



If we had one [Zimbabwean penny](http://en.wikipedia.org/wiki/Hyperinflation_in_Zimbabwe) every time the phrase "reading comprehension problems" or "lack of reading comprehension" or other "reading comprehension" constructs are used, we could buy Canada. Everybody who encounters it the first time seems to want to use it themselves, perhaps because it sounds like a fancy zinger, so the mods see it used more often than punctuation.



But wheel this one out and listen to the groans from your audience. It's normal to misinterpret something, but poor writing is even more common and harder for the author to see from another person's eyes. Instead of dismissing someone, you risk triggering a side-argument that devolves into "oh yeah?" retaliatory analysis of your interpretations, sinking into the quagmire of grammar debate and the definitions of words.



# Dismiss their education



   * "Actually this would be another example of your sub-philosophy 101 education."



"_[college course] 101_" has become a trope all over the Internet, not just reddit or CMV, but we've noticed that many people feel that it's their privilege to learn new subjects outside of the classroom and be respected for it. CMV in particular seems to attract [autodidacts](http://en.wikipedia.org/wiki/Autodidacticism) in number, and it's not unusual for people to take a shot at changing an OP's view by hitting Wikipedia and rolling up their sleeves to see what comes of it.



As a consequence, they tend to sympathise with people who suffer the mockery of those who claim to be better schooled, which is the first way this technique can backfire.



We've also seen many of these participants learn and familiarise themselves with new topics at astonishing speed, scope and capability. Ridiculing self-study actually serves as a motivator instead, and you may find yourself the target of embarrassingly accurate technical rebuttals.



Finally, this technique attracts people who really do know more about the subject than you do and fill them with a desire to put you in your place. It's not uncommon on CMV to see people with professional degrees and credentials humiliate someone who mocked a "high school" understanding. It's like seeing the [SR-71 speed-check story](http://oppositelock.jalopnik.com/favorite-sr-71-story-1079127041) play out again every week.



# Spontaneously ask them how old they are



**[Remove-worthy]**



   * "Are you, like, 14?"



   * "What grade are you in?"



When it comes out of the blue it's transparently accusing them of being young, inexperienced, under-informed, etc. It's a type of belittling, but its also used frequently by people who are too stunned to form a better response, so it now has the stigma of a weak position. Rather than making your opponent look naive, it makes you look hamstrung.



Since it's a particularly rude form of ad-hominem that's irrelevant to the mission of CMV, these comments will be removed by the mods.



# Link with "Let Me Google That For You" (lmgtfy.com)



**[Remove-worthy]**



[LMGTFY.com](http://lmgtfy.com/) might be the heavyweight king of the patronising tone, sarcastically ridiculing someone's ignorance of a tool that the rest of us take for granted.



It backfires because even when the search results are relevant and helpful, the way you delivered them fills the OP or opponent with a resolve to continue arguing anyway. You might as well just use Google yourself and simply link to the relevant result directly.



Since this is very blatantly rude and disrespectful, posts that link via LMGTFY will be removed upon report.



# Inform them of their inherent cognitive limitations



   * "You are incapable of responding to these points."



   * "You have proven that you cannot grasp this concept." (Bonus points for using "proven")



Bring the [actor-observer bias](http://en.wikipedia.org/wiki/Actor%E2%80%93observer_asymmetry) into full force and interpret a lack of response, or an _unsatisfactory_ response, as evidence of a core dysfunction.



You will encounter a problem not just with your opponent, however, but with any other spectator who--like your opponent--has a different model of how the discussion is progressing and what is being argued. You'll reveal your own inner bias on what's important to the argument, which may not be flattering in the eyes of others. It's common on CMV for people to have different ideas of what matters and what's irrelevant or tangental.



It's better to take the fight to where they are, not stand on an empty battlefield and pretend you won.



# Whine about goalposts



Spend several exchanges complaining that they've moved their goalposts (standard of evidence), make it the focus of your continuing comments. This is a special variation of the "cognitive limitations" technique from above.



It's perfectly valid and useful to point out when someone has moved the goalposts in their argument, but it's most effective when pointed out just once and on its own. When you drag it out for several more exchanges, the feeling of both the audience and the other user is that you're uncomfortable with arguing on new ground. You drown out their fallacy with your frequently voiced reservations, so it's _you_ who end up looking weak. It's also very boring to read.



As an alternative, think about how you could take the fight to them no matter where they go, so it looks like they've nowhere to hide. Entertain the ridiculous, and you'll implicitly show that it's ridiculous. Instead of telling them that they keep moving the goalposts, wait until they've shifted several times and then just point it out, as if it was a casual observation made in passing. Don't turn it into a personal critique or their guard will go up and you'll have wasted your time.



# Project your feelings



   * "You're angry with me because I don't agree with you."



We're all susceptible to [psychological projection](http://en.wikipedia.org/wiki/Psychological_projection), where we experience an emotion and perceive it to be shared or originate from someone else. This can often simply be the result of poor writing or unfortunate choice of words. But while your opponent may not have chosen their words with Freudian deliberation, your response will be clearly naked for what it is.



(Did I say "naked"? Oh dear, something must be on my mind.)



Earlier we mentioned that CMV is a sport for many members, and they'll not only write "walls o' text" for seemingly insignificant issues, but experiment with very flowery language and "purple prose". While they can stand back in comfort and confidence, you'll have everyone thinking that it's you who's blood pressure rose.



# The "if you think X, then you're an idiot" trap



**[Remove-worthy]**



   * "If you think this matters in the grand scheme of things, then you're stupid."



   * "I think that people who believe in what you just said are retarded."



Its the "Emperor's New Clothes" in a different guise, where you describe your opponent's position and then rely on the word "if" to absolve you of delivering an insult. But you'll be seen as the fraudulent tailor, not the child calling the Emperor naked.



The mods of CMV see this form of argument on a daily basis, and after some discussion we decided that it is indeed a trap, and the use of "if" is unfair. It's an attempt to force someone to agree with you in order to avoid being labelled stupid.



# Imply a lack of experience when they don't agree with you



   * "It's plainly obvious that you have zero experience with X and are not in a position to spout glib advice."



Obviously, if someone disagrees with you it must mean they lack a key life experience. But the more common the experience (such as having children, going to college, being interviewed for a job, joining the army, etc.) the more likely the accusation will backfire badly.



The first way it backfires is that you're essentially saying that you are so common and un-unique that the experience could only affect you one way. You are an automaton, a clockwork toy, ready to be manipulated puppet-like with the appropriate application of certain experiences and sensations.



The second way it backfires is when you use it to establish a position of authority, when it may in fact establish irrational bias.



The third way is when your opponent has, in fact, gone through the same experience but learned more or different from it than you. They will pounce on this and happily respond with both their credentials and their "wiser" conclusions.



# Portray Atlas as Joe Schmoe



   * "An adult would be able to deal with this."



   * "Real men don't have a problem putting up with X."



   * "What a competent person would do is Y."



   * "Normal human beings aren't bothered by such trivialities."



Few discussions on CMV are about what a hypothetical sensible person would do in a given situation, but idealised versions of humanity are often brought up as if they were the norm.



What we tend to see is a series of unsolicited responses that say, in effect, that they consider themselves to be normal folk but don't do what you say they ought to do. A dangerous reaction to this kind of response is the "No True Scottsman" argument, which doesn't fly far on diverse forums like ours.



# "Tough Love" or "shocking them into sanity" is the only way to change their view



**[Remove-worthy]**



This is a guise for being rude and hostile, where you "talk street smarts" down to the OP or other user as if you were a parent, hardened police officer, seen-it-all social worker, thousand-yard-stare Marine and so-on. It might work in physical contact, where you can also speak in body language, but it fails badly on the _Internet_, especially on reddit where most users are anonymous. Rather than being cowed, your target will just respond with hostile sarcasm, and it then snowballs into a mess. The OP or user is a stranger: you don't know them, and they don't know you. This means the crucial elements to make this strategy work just aren't there.



The "listen, son" speech occasionally works if you can establish substantial credentials, and yet keep the language level and calm. But unless you actually went to 'Nam and have a Purple Heart to prove it, you're better off avoiding this technique, otherwise we tend to see that the Wise Man figure is mocked and ridiculed, with the goal of making you look pretentious and incompetent, a rookie who didn't know he wasn't yet a pro. The discussion then turns into a squabble centred on one party trying to maintain their pride, and the other party intending to burn it further.



Consider [this article](http://www.indyweek.com/indyweek/a-judge-calls-people-idiots-and-a-melee-erupts-in-durham-family-court/Content?oid=4242294) about a District Court Judge who lectured two parents over a child custody case, including calling them "idiots" and belabouring the details of physically conceiving a child. The result was an outburst from the mother rejecting the judge's _moral_ superiority, then a 30-day jail sentence. Nobody's view was changed and the tactic backfired nastily, since even the Judge made himself look condescending and unprofessional on a video that has gone viral. 



# Be verbally astonished



**[Remove-worthy]**



   * "Wow, you are an ignorant fuck, aren't you?"



   * "Holy crap, I had no idea you were so naive."



This is rather low quality antagonism, but very easy to stumble into if you're talking to someone with a very different perspective from yours, especially when your difference with them covers a lot of assumed/unspoken ground. If you're putting in a lot of time and effort into changing someone's mind, and you drop something as smelly as this into your comments, you're going to flush any progress you might have made in the meantime. More fool you.



Since it's an expression of personal contempt, we consider it hostile and rude and will remove these comments.



# Accuse them of being shills



**[Borderline, depending on how blatant and if combined with other rule violations]**



   * "How much are you being paid to say this?"



   * "You are obviously a paid shill for the government" (Bonus points: "big pharma" or Monsanto)



**If you have good reasons, and especially verifiable evidence that someone is being paid to advocate a position, or has not disclosed financial conflicts of interest, PLEASE [message the mods](http://www.reddit.com/message/compose?to=%2Fr%2Fchangemyview) with a link to the comment and your evidence, but DO NOT RESPOND TO THEM.** We will and have banned users that we have good reason to think are pushing an agenda and are not on CMV in good faith.



If you're active on Internet forums for long enough, you'll probably have this accusation thrown at you eventually. The first time it happens you'll realise that the other person is wrong (after all, you should know), and immediately after that you'll be even more suspicious of it when you see it happening to someone else.



Since CMV entertains many topics that go against political and business interests, many of our regulars have been on the receiving end of this a few times. You're going to lose them on the spot if you try this.



However, it's also used rhetorically as a kind of taunt or insult, so comments of that nature are typically removed for Rule 2. If you're honestly suspicious, please report it to the mods. We understand that reddit's admins aren't particularly fond of "astroturf" campaigns either, depending on the degree to which they target reddit.



# If you'd read X, you'd know Y



Spoken and written languages used by humans are foggy playgrounds of interpretation. It's better to make an _informed reply_, starting with phrases such as "I'll try again this way…" or "I might have misunderstood that, so I'll put it differently as…"



Were they on mobile, and on a bumpy train? Are they dyslexic but don't believe that should matter? Are they bombarded with many other replies and trying their best? Would you win a delta if you assumed any of the above and more were true, and just took it in stride and tried again another way?



Human languages aren't computer languages, you can't expect another human being to act like a compiler and interpret things the same way you do. Inferring that they didn't understand what you said doesn't incriminate them, it merely exposes a problem that you and everyone else has when they're misunderstood.



Give them the time of day, and they'll be much more receptive.
