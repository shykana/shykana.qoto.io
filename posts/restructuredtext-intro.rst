.. title: reStructuredText Intro
.. slug: restructuredtext-intro
.. date: 2020-12-31 10:07:31 UTC+08:00
.. category: 笔记
.. tags: reStructuredText, 笔记, cheatsheet
.. category: 
.. link: 
.. description: 
.. type: text

.. sectnum::

我看着 reStructuredText 的文档绝望极了，真的比 markdown 复杂多了 T_T。我尝试着在这里做点笔记。

.. caution:: 推荐几个好得多的博文，
    都来自于同一个博客：

    《从 Markdown 到 reStructuredText》： https://macplay.github.io/posts/cong-markdown-dao-restructuredtext/

    《从 Markdown 到 reStructuredText（二）》： https://macplay.github.io/posts/cong-markdown-dao-restructuredtexter/

    《从 Markdown 到 reStructuredText（三）》： https://macplay.github.io/posts/cong-markdown-dao-restructuredtextsan/

    写得比我详细得多得多得多，还讲到了 reStructuredText 的其它很多很厉害的功能（例如方便的 CSV 表格、Admonition 等等）

==================
与 markdown 的比较
==================

::

    reStructuredText 对中文的体验极其不友好，
    因为它很多结构（表格/标题）要求宽度对应，但是中文毕竟和英文跨度不同，
    很多编辑器甚至处理器可能都有不同的处理方式……
    不爽。
    所以下面的表格用英文了。

相邻的两个块之间要用空行隔开。

+---------------+---------------------+-------------------------------+
| /             |  Markdown           |  reStructuredText             |
+===============+=====================+===============================+
| Title         || ``# h1``           | ::                            |
|               || ``## h2``          |                               |
|               || ``### h3``         |     ==========                |
|               || etc.               |       h1                      |
|               |                     |     ==========                |
|               |                     |                               |
|               |                     | or::                          |
|               |                     |                               |
|               |                     |      h2                       |
|               |                     |     ----------                |
|               |                     |                               |
|               |                     | Must be wider than the text   |
|               |                     |                               |
|               |                     | # * = - ^ " could be used [1]_|
+---------------+---------------------+-------------------------------+
| Bold          | ``**bold**``        | ``**bold**``                  |
+---------------+---------------------+-------------------------------+
| Italic        | ``*italic*``        | ``*italic*``                  |
+---------------+---------------------+-------------------------------+
| Blockquote    | ::                  | ::                            |
|               |                     |                               |
|               |                     |     not blockquote here       |
|               |     > quote1        |         indent and you get one|
|               |     > quote2        |         another line          |
|               |                     |             nested another    |
+---------------+---------------------+-------------------------------+
| Inline Code   | ```code```          | ````code````                  |
+---------------+---------------------+-------------------------------+
| Block Code    | ::                  | ::                            |
|               |                     |                               |
|               |      ```            |     ::                        |
|               |                     |                               |
|               |      code           |         code                  |
|               |      ```            |                               |
|               |                     |                               |
+---------------+---------------------+-------------------------------+
| Footnote 2_   |                     | ``[1]_``                      |
|               |                     |                               |
|               |                     | and later the page::          |
|               |                     |                               |
|               |                     |     .. [1] footnote           |
|               |                     |          could be indented    |
+---------------+---------------------+-------------------------------+
| Link 2_       | ``[name](link)``    | ``link``                      |
|               |                     | or ```name <link>`_``         |
+---------------+---------------------+-------------------------------+
| In-page links |                     | ``target_``                   |
| 2_            |                     | or ```target`_``              |
|               |                     | where target could be titles, |
|               |                     | footnote, definitions, etc.   |
+---------------+---------------------+-------------------------------+
| Lists         | ::                  | same, same.                   |
|               |     1. one          |                               |
|               |     2. two          |                               |
|               |                     |                               |
|               |     - bullet        |                               |
|               |     - and - * +     |                               |
|               |     - could be used |                               |
+---------------+---------------------+-------------------------------+
| Images        | ``![alttext](link)``| ::                            |
|               |                     |                               |
|               |                     |     .. image:: link           |
|               |                     |         :alt: alttext         |
+---------------+---------------------+-------------------------------+

.. image:: https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Green_eyes_kitten.jpg/120px-Green_eyes_kitten.jpg
    :alt: A cat goes here

.. [1] Python 传统上的习惯是：

    - 一级标题（书的一部/一集/part）::

         ########
           part
         ########

    - 二级标题（书的一章/chapter）::

        ***********
          chapter
        ***********

    - 三/四/五/六级标题（一节/一小节/一小小节/段落）::

         title
        =======

      三/四/五/六级分别使用：``=`` / ``-`` / ``^`` / ``"``

.. [2] 超链接由两部分构成，一个是显示的文字，一个是链接地址。
    reStructuredText 里这个可以拆开，比如：``Python_`` 这样是定义了显示的文字，
    而对应的链接地址可以在另外的地方这样给出::

        .. _Python: http://www.python.org/

    另外，脚注也可以看作是这种格式的延伸。脚注有一些其它格式： ``[#name]_`` 这样配合::

        :: [#name] 自动编号的脚注

    会生成自动编号的脚注。

====
其它
====

生成目录
========

使用：

.. code:: rst

    .. contents:: 目录标题
        :depth: 2

表格
====

一个方便的编辑表格的方法是使用 CSV 表格：

.. code:: rst

    .. csv-table::

        "第一行第一列","示范一个内嵌代码块：

        .. code:: python

            a = [1, 2, 3]
            print(""No Python"")"

显示出来就是这个样子：

.. csv-table::

    "第一行第一列","示范一个内嵌代码块：

    .. code:: python

        a = [1, 2, 3]
        print(""No Python"")"

自动章节编号
============

在文章里找个地方放下：

.. code::

    .. sectnum::

这么一行就可以了。不要放在最前面和 meta 信息混在一起，会无效。至少空一行。

Admonition
==========

支持的有 ``attention, caution, danger, error, hint, important, note, tip, warning`` ，例如：

.. code::

    .. note::

        这样渲染起来会是这个样子。

.. note::

    这样渲染起来会使这个样子。

====================
在 Nikola 的特殊用法
====================

因为我用的是 Nikola_ 这个静态博客生成系统，而在博客相关的功能方面它提供了一些特殊语法。

TEASER_END
==========

.. code:: rst

    .. TEASER_END

博客对每个页面的预览默认是不会缩短页面内容的，所以在主页等页面看到的页面预览就是完整的页面。有时页面太长了，预览最好缩短一下，这个语法标志着预览的结束（预览从页面开头一路到这个标志）。

.. _`Nikola`: https://getnikola.com/
