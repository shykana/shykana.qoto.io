---
title: Pleroma-fe 的 emoji 放大配置（？）
category: 服务器
date: 2020-09-19 23:26:18
updated: 2020-11-11 23:26:18
tags:
---
（虽然我也不知道具体是怎么样的
（Pleroma 有 OTP 安装以及从源码安装，我的是 OTP ，我不知道源码安装的会怎样

[TOC]

# 更新！

可以使用 [pleroma-mod-loader](https://git.pleroma.social/absturztaube/pleroma-mod-loader) 以及搭配的 [pleroma-mod-custom-styles](https://git.pleroma.social/absturztaube/pleroma-mod-custom-styles) ！会省事一些，而且功能会多一些（例如用户可以方便地自行取消 css）。（但是要找 `index.html` 还是要按下面方法？）

# 修改的准备
## 方法一：找到 pleroma-fe，然后在 static 目录里修改
我印象中，源码安装的 pleroma-fe 是在安装目录中一个名为 `private` 的目录里的。在 OTP 里，是在安装目录里 `lib/pleroma-{版本号}/priv/static` 目录下。由于一般来说最好不要直接更改这里的内容，所以我们可以把需要更改的文件（`index.html`）复制到 pleroma 安装时指定的 `static/static` 目录下，然后再更改 `static/static` 目录里的文件。后面创建的 `custom` 目录以及 `custom/custom.css` 也在这里创建。

这种方法要注意后续 pleroma 升级的时候要重新再复制、改动一遍 `index.html` 文件，不然可能会有白屏等情况。

## 方法二：使用手动安装的 pleroma-fe
（用于找不到 pleroma-fe在哪儿的情况
### 手动安装 pleroma-fe
（而且我实在是找不到 pleroma-fe 原来存放在哪里 T_T ，只好再安装一次。如果可以找到原来 pleroma-fe 的存放位置的可以尝试直接在原处改动。
（本教程需要 pleroma 版本 2.1.0 或以上，因为需要 frontend 安装功能

注意，这种方法在 pleroma 更新时可能需要手动更新前端（特别是 admin-fe），更新方法是把：
```
sudo -Hu pleroma ./bin/pleroma_ctl frontend install pleroma-fe
sudo -Hu pleroma ./bin/pleroma_ctl frontend install admin-fe
```
这两个命令再执行一遍就行了。执行完命令之后要把 `index.html` 再修改一次。不需要再创建 `custom.css` 了。

```
# 进入 pleroma 的目录，如果你安装到了其他目录请更改为相应的目录
cd /opt/pleroma
# 安装管理界面的前端，如果你使用网页端的管理界面的话一定要这一步（血的教训 T_T
sudo -Hu pleroma ./bin/pleroma_ctl frontend install admin-fe
# 接下来安装 pleroma-fe
sudo -Hu pleroma ./bin/pleroma_ctl frontend install pleroma-fe
```

要注意上面的安装 pleroma-fe 时命令输出的结果，比如可能输出为 `Frontend pleroma-fe (develop) installed to /var/lib/pleroma/static/frontends/pleroma-fe/develop`，这里要注意最后一部分的 `/pleroma-fe/develop`，这部分信息在下面设置 pleroma 时要用到。整段的路径在最后修改时要用到： `/var/lib/pleroma/static/frontends/pleroma-fe/develop`。

### 设置 pleroma 前端为手动安装的 pleroma-fe
如果是可以使用网页段控制界面的话，可以进入如图的 tab ，**根据上面的命令输出结果**在如图的输入框内填入相应信息。**确认 Admin 部分填写正确后**点击 submit 按钮提交。
![Frontend Tab](/images/frontend_tab.png)
![Frontend Settings](/images/frontend_settings.png)

# 开始修改 pleroma-fe

## 修改 `index.html`

根据上面的输出结果执行命令：
```
cd /var/lib/pleroma/static/frontends/pleroma-fe/develop
```

这个时候目录下有一个 index.html ，修改 index.html：
```
sudo -Hu pleroma nano index.html
```

然后往后找，一直找到 `</head>` ，在 `</head>` 前加入：
```
<!-- 加入下面这行 -->
<link href=/static/custom/custom.css rel=stylesheet>
```

保存并退出。

## 创建 `custom.css`

现在创建上面用到的 `custom.css`，下面用到了安装时指定的 `static_dir` 目录，默认为 `/var/lib/pleroma/static/`，在路径之后再加上一个 `static/` 变成下面使用的路径：
```
cd /var/lib/pleroma/static/static
sudo -Hu pleroma mkdir -p custom
sudo -Hu pleroma nano custom/custom.css
```

用 nano 往 custom.css 里加入：
```
.emoji:hover {
    transform: scale(2);
    transition: all .2s ease;
}
.emoji {
    transition: all .2s ease;
}
```
（这个是抄自 mastodon 站点 acg.mn 的样式）
（上面的 `scale(2)` 是放大的倍数，如果觉得太大了可以尝试一下 `scale(1.2)` 等其它的数字； `.2s` 是放大动画的时间，可以改成其他的值，如： `0.15s` `1.0s` 等）

然后保存并退出可能就可以了？
