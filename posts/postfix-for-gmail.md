---
title: 用Postfix配置Google邮箱邮件送信
category: 服务器
tags:
  - Email
id: '180'
date: 2020-07-17 09:39:21
updated: 2020-07-17 09:39:21
---

（因为链接是 [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0) 的，所以就不翻译了，直接贴链接） [Configure Postfix to Send Mail Using Gmail and Google Apps on Debian or Ubuntu](https://www.linode.com/docs/email/postfix/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu/)

找了另一篇许可证友善的，总结在 [pleroma-using-postfix-for-email](../pleroma-using-postfix-for-email/) 这里。
