.. title: Pleroma 使用 Postfix 提供邮件服务
.. slug: pleroma-using-postfix-for-email
.. date: 2021-08-11 15:56:02 UTC+08:00
.. updated: 2021-08-11 19:22:45 UTC+08:00
.. tags: Email
.. category: 
.. link: 
.. description: 
.. type: text
.. license: cc-by-sa

Pleroma 的邮件的配置有很多方案。 [#pleroma-mail]_ 其中最通用的是 SMTP 以及 Sendmail 。就我们用到的功能而言，其实这两个没有什么区别：

.. csv-table::
    :header: "SMTP","Sendmail"

    "::

        Pleroma
        ↓ (SMTP)
        邮件提供商
        ↓
        收件者
    ","::

        Pleroma
        ↓
        Postfix/Sendmail
        ↓ (SMTP)
        邮件提供商
        ↓
        收件者
    "

.. [#pleroma-mail] 去看了一下，有 AmazonSES, Dyn, Gmail, Mailgun, Mailjet, Mandrill, Postmark, SMTP, Sendgrid, Sendmail, SocketLabs, SparkPost 等。真的好多啊。

但总之，毕竟 postfix 在其它服务（如 mastodon, php 相关的 flarum 或者 wordpress）里都可以用，所以还是介绍一下 postfix 的配置。

.. TEASER_END

.. contents:: 目录

.. sectnum::

######################
例子：Outlook 获取配置
######################

`Outlook`_ 的注册限制相对少一点

.. caution:: 但是这种大公司的个人隐私肯定是个问题。特别微软在国内还有挺多业务的吧。

注册后设置里查找 SMTP 找到这种信息：

    Server name: smtp.office365.com

    Port: 587

    Encryption method: STARTTLS

对于比较好一点的服务，我们应该可以为不同应用配置专门的密码，以确保我们的主密码的安全。Outlook 的话需要到 `Microsoft 帐号安全` 里去从各种栏目里找到双重验证（two factor authentication），开启之后去创建新应用密码。

下面假设我们帐号和密码分别是 ``registered.account@example.com`` 和 ``GeneratedPassword`` 。

谷歌邮箱的可以参考 `Configure Postfix to Send Mail Using Gmail and Google Workspace on Debian or Ubuntu`_ 。

.. _`Outlook`: https://outlook.live.com
.. _`Microsoft 帐号安全`: https://account.microsoft.com/security
.. _`Configure Postfix to Send Mail Using Gmail and Google Workspace on Debian or Ubuntu`: https://github.com/linode/docs/blob/develop/docs/guides/email/email-services/configure-postfix-to-send-mail-using-gmail-and-google-apps-on-debian-or-ubuntu/index.md

############
Postfix 配置
############

这部分参考了 `Configure Postfix to Send Email Using External SMTP Servers`_ 以及 `How To Install and Configure Postfix as a Send-Only SMTP Server on Ubuntu 20.04`_ 以及 `Postfix sender_dependent_relayhost_maps`_ 。

.. _`Configure Postfix to Send Email Using External SMTP Servers`: https://github.com/linode/docs/blob/develop/docs/guides/email/email-services/postfix-smtp-debian7/index.md
.. _`How To Install and Configure Postfix as a Send-Only SMTP Server on Ubuntu 20.04`: https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-ubuntu-20-04
.. _`Postfix sender_dependent_relayhost_maps`: https://tapoueh.org/blog/2010/09/postfix-sender_dependent_relayhost_maps/

============
Postfix 安装
============

不同发行版不同。 `Configure Postfix to Send Email Using External SMTP Servers`_ 这里有 Debian 系的安装方法。可以尝试一下 ``postfix`` 和 ``mailutils`` 以及 ``libsasl2-modules`` 和 ``libsasl`` 这几个包名。如：

.. code:: console

    $ # Ubuntu 20.04
    $ sudo apt install mailutils libsasl2-modules postfix

    $ # Arch Linux
    $ sudo pacman -S postfix

安装过程要填内容的话， `mail configuration` 选项相关的选择 `Internet Site` ，后面域名 `System mail name` 的填空直接随便填就可以了。

============
身份信息配置
============

把密码等信息放在 ``/etc/postfix/sasl/passwords`` 这里。（也可以使用其它路径，但是后面的配置需要相应变更。）

.. code:: console

    $ sudo mkdir -p /etc/postfix/sasl

创建目录后新建 ``/etc/postfix/sasl/passwords`` 文件，并编辑为：

.. code::

    [smtp.office365.com]:587 registered.account@example.com:GeneratedPassword

这里面的内容需要根据 `例子：Outlook 获取配置`_ 具体获取的值来确定。然后使用：

.. code:: console

    $ sudo postmap /etc/postfix/sasl/passwords
    $ sudo chown root:root /etc/postfix/sasl/passwords*
    $ sudo chmod 0600 /etc/postfix/sasl/passwords*

来生成其它配套文件并配置安全权限。

最后需要修改 ``/etc/postfix/main.cf`` 文件，这里的配置请见 `服务器连接配置`_ 。

==============
服务器连接配置
==============

服务器连接配置文件
==================

新建 ``/etc/postfix/relaymap`` 文件，内容为：

.. code::

    registered.account@example.com [smtp.office365.com]:587

使用命令 ``sudo postmap /etc/postfix/relaymap`` 生成配套文件。

编辑 ``main.cf``
================

我们需要编辑 ``main.cf`` 这个主配置文件，来使用上面的 ``/etc/postfix/sasl/passwords`` 以及 ``/etc/postfix/relaymap`` 文件，并进行加密相关的配置。

编辑 ``/etc/postfix/main.cf`` 文件，在文件最后加入：

.. code::

    smtp_sasl_auth_enable = yes
    smtp_sasl_security_options = noanonymous
    smtp_use_tls = yes
    smtp_tls_security_level = may
    smtp_tls_CApath = /etc/ssl/certs
    smtp_sasl_password_maps = hash:/etc/postfix/sasl/passwords

    smtp_sender_dependent_authentication = yes
    sender_dependent_relayhost_maps = hash:/etc/postfix/relaymap

重启 postfix：

.. code:: console

    $ sudo systemctl restart postfix

================
Postfix 开机启动
================

.. code::

    $ sudo systemctl enable postfix

============
测试 postfix
============

.. code:: console

    $ sendmail your.other.email@other.domain
    From: registered.account@example.com
    Subject: Testing sending email from postfix
    This is a test email from possibly postfix.
    .

==================
多个帐号要怎么办？
==================

有时候我们想要让同一台服务器上不同服务使用不同的发信邮箱，例如让 mastodon 使用一个邮箱，让 pleroma 使用另外一个同样域名的甚至不同域名的邮箱。配置完以上内容后，这些就比较轻松了。

从 `服务器连接配置文件`_ 以及 `身份信息配置`_ 我们可以看出来，如果需要添加其它的邮件帐号，我们只需要在这两个文件里依次添加信息即可，例如：

在 ``/etc/postfix/sasl/passwords`` 这里添加一行信息。添加后看起来是：

.. code::

    [smtp.office365.com]:587 registered.account@example.com:GeneratedPassword
    [smtp.gmail.com]:587 SomeGoogleAccount@gmail.com:GmailPassword

然后在 ``/etc/postfix/relaymap`` 这里添加连接信息：

.. code::

    registered.account@example.com [smtp.office365.com]:587
    SomeGoogleAccount@gmail.com    [smtp.gmail.com]:587

最后，不要忘了使用 ``postmap`` 命令再次生成配套文件：

.. code:: console

    $ sudo postmap /etc/postfix/sasl/passwords
    $ sudo postmap /etc/postfix/relaymap
    $ sudo systemctl restart postfix

############
Pleroma 配置
############

首先，因为一个旧 bug `Email via sendmail was not working probably due to NoNewPrivileges`_ ，虽然现在已经修复，但是从老版本更新来的实例还是会受到影响，所以请先到 `Pleroma SystemD 配置文件`_ 这里去检查一遍配置。

.. _`Email via sendmail was not working probably due to NoNewPrivileges`: https://git.pleroma.social/pleroma/pleroma/-/issues/2259

========================
Pleroma SystemD 配置文件
========================

由于 `Email via sendmail was not working probably due to NoNewPrivileges`_ 这个旧 bug，pleroma 使用 postfix/sendmail 是会失败的。因为这个 bug 是由 SystemD 配置文件引起的，而对于老版本的 pleroma，SystemD 配置文件又不会随着 pleroma 更新而更新，所以我们还是需要手动解决。

方法很简单，打开 ``/etc/systemd/system/pleroma.service`` 这个文件，检查里面是否有包含着 ``NoNewPrivileges`` 的这么一行。如果有的话，整行删掉。

例子：原来的：

.. code::

    ...
    ; Sets up a new /dev mount for the process ...
    PrivateDevices=false
    ; Ensures that the service process and all ...
    NoNewPrivileges=true
    ; Drops the sysadmin capability from the daemon.
    CapabilityBoundingSet=~CAP_SYS_ADMIN
    ...

删掉！

.. code::

    ...
    ; Sets up a new /dev mount for the process ...
    PrivateDevices=false
    ; Drops the sysadmin capability from the daemon.
    CapabilityBoundingSet=~CAP_SYS_ADMIN
    ...

===============
从 AdminFE 配置
===============

（如果用的是配置文件的话，我猜应该也不需要教程？）

.. image:: ../../images/pleroma-adminfe-sidebar.png

首先从管理面板的侧边栏点击 `Instance` 进入实例设置，把 `Sender Email Address` 设置为相应的邮件地址：

.. image:: ../../images/pleroma-sender-email.png

`Submit` 保存。（印象中需要重启实例，会弹出一个按钮重启实例。）

重启后，还是侧边栏，点击 `Mailer` 进入发信服务配置：

.. image:: ../../images/pleroma-mailer.png

直接 `Mailer Enabled` 开启， `Adapter` 设为 `Sendmail` 。其它的默认就可以了。

`Submit` 保存。

=====================
测试 pleroma 邮件发送
=====================

请见 `EMail administration tasks - Pleroma Documentation`_ 。

.. _`EMail administration tasks - Pleroma Documentation`: https://docs-develop.pleroma.social/backend/administration/CLI_tasks/email/

.. code:: console

    $ cd /opt/pleroma # pleroma 的安装目录
    $ sudo -Hu pleroma ./bin/pleroma_ctl email test --to personal.account@example.org

####
其它
####

==========
Mastodon？
==========

我不清楚啦。稍微查了一下似乎直接是在 ``.env.production`` 之类的文件里把 ``SMTP_DELIVERY_METHOD`` 设置成 ``sendmail`` 就可以了？

:del:`文档不是很全的样子……大概还是跟着官方的 SMTP 设置为好。(观察了一下 mastodon 的 systemd 文件，一部分也有 NoNewPrivileges 的样子，希望那一部分不会涉及到 postfix/sendmail。）`

另外一种更容易使用的方法是通过 postfix 提供的 SMTP 接口来发送邮件，这个时候的发送路径就变成了::

   Mastodon
   ↓ (SMTP)
   postfix
   ↓ (SMTP)
   邮件提供商
   ↓ (?)
   收件者

大致按如下设置：（未经检验，敬请指正）

.. csv-table::
    :header: "域","值"

    "``SMTP_SERVER``","``127.0.0.1``"
    "``SMTP_PORT``","``25``"
    "``SMTP_FROM_ADDRESS``","postfix 里对应的邮箱"
    "``SMTP_AUTH_METHOD``","``none``"
    "``SMTP_OPENSSL_VERIFY_MODE``","``none``"
    "``SMTP_ENABLE_STARTTLS_AUTO``","``false``"
    "其它的","无需设置"

.. note:: 这里可能需要 postfix 里有 ``smtpd_tls_cert_file`` 和 ``smtpd_tls_key_file`` 两项设置。但我的发行版（Ubuntu）里默认的配置文件里默认带有这两行，所以可以直接这么写。如果没有的话，可能可以尝试把 mastodon 设置里的 ``SMTP_TLS`` 和 ``SMTP_SSL`` 都设为 ``false`` 。
