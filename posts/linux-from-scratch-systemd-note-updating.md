---
title: Linux From Scratch SystemD 笔记 (不定期缓慢更新)
category: 工具
tags:
  - Linux
id: '77'
date: 2019-08-11 22:59:34
updated: 2020-07-31 22:59:34
---

LFS 地址: [http://www.linuxfromscratch.org/lfs/](http://www.linuxfromscratch.org/lfs/)
本文原来是基于 LFS Systemd 8.4 版的，但拖着拖着 9.1 版就出来了，所以以 Systemd 9.1 版为准。

### 创建用于编译及安装系统的磁盘镜像

官方是直接用物理硬盘, 创一个分区来专门弄 LFS. 但是对我这种[硬盘乱糟糟](/posts/some-curious-partitioning/)的, 还是弄一个镜像文件放会省事一点.

<style>
  blockquote {
    font-family: monospace;
  }
</style>


> $ mkdir LFS
> $ cd LFS
> $ truncate -s 16G lfs.img # 创建 16G 大小的虚拟硬盘镜像
> $ fdisk lfs.img
> Command (m for help): o # 这里输入 o 回车, 创建 MBR 分区表 (GPT 可能也行? 但好像要创多个 boot 分区)
> Command (m for help): n # 输入 n 回车, 之后一路回车, 创建分区项
> Command (m for help): w # w 回车, 写入
> $ fdisk -l lfs.img
> Disk lfs.img: 16 GiB, 17179869184 bytes, 33554432 sectors
> Units: sectors of 1 \* 512 = _**512**_ bytes
> Sector size (logical/physical): 512 bytes / 512 bytes
> I/O size (minimum/optimal): 512 bytes / 512 bytes
> Disklabel type: dos
> Disk identifier: 0x14bfb794
> 
> Device     Boot Start      End  Sectors Size Id Type
> lfs.img1   _**2048** **33554431**_ 33552384 16G 83 Linux
> $ losetup --offset $((_**512**_ \* _**2048**_)) --sizelimit $((_**512**_ \* _**33554431**_)) /dev/loop0 lfs.img
>  _**\# 请照应 fdisk 输出, 相应更改对应的数字**_
> $ mkfs.ext4 /dev/loop0
> $ mkdir mnt
> $ mount /dev/loop0 mnt # 挂载
> $ umount /dev/loop0 # 卸载
> $ losetup --detach /dev/loop0 # 卸载 loop 设备

怕麻烦, 弄两个脚本:

> $ cat > mountup.sh <<EOF
> #!/bin/sh
> losetup --offset $((_**512**_ \* _**2048**_)) --sizelimit $((_**512**_ \* _**33554431**_)) /dev/loop0 lfs.img
> mount /dev/loop0 mnt
> export LFS="\`pwd\`/mnt"
> EOF


> $ cat > umount.sh <<EOF
> #!/bin/sh
> umount /dev/loop0
> losetup --detach /dev/loop0
> EOF

最后:

>$ chmod a+x \*.sh

之后就可以 `sudo ./mountup.sh` 和 `sudo ./umount.sh` 了.

### 所需文件的下载

大概跟着书里走就行了。但是国内的网络不一定很好，这个还请自己解决……
提示：wget支持使用代理：
`http_proxy=PROXY https_proxy=PROXY ftp_proxy=PROXY wget ...`
把`PROXY`替换为自己的代理就行了，例如替换为`http://127.0.0.1:8080`之类的。
算了，我把我下载的传到了坚果云上：
* LFS Systemd 8.4: [https://www.jianguoyun.com/p/DWbL-y8Q-dnWCBiVurAD](https://www.jianguoyun.com/p/DWbL-y8Q-dnWCBiVurAD)
* LFS Systemd 9.1: [https://www.jianguoyun.com/p/Deir9HEQ-dnWCBjGv7AD](https://www.jianguoyun.com/p/Deir9HEQ-dnWCBjGv7AD)
实在下不了的也可以到这里下载。
无论从什么途径下载，不要忘了进到下载目录然后校验：
`$ md5sum -c /path/to/md5sums`
后面一个`/path/to/md5sums`在书里有下载地址。

### 准备环境

书里基本是搭了一个新环境，创了新用户、改了权限、设了一般的环境变量，按着操作可以学一点东西。但我懒，就直接用自己的用户了。

#### 一句 ln 命令的解释
看到官方文档里特地标出来了, `ln -s $LFS/tool /` 不是写错了. 其实就是在 / 文件夹里创建与 tool 同名的指向 tool 文件夹的软链接而已. 不是把 / 变成软链接. (怕不是很多人都报 bug 了才特地指出...)

##### 教训1
~~然后呢最开始我觉得弄乱根目录不太好，将书里的`/tools`链接改到了`./tools`。~~
~~我也相应改了一点，比如下面编译的时候要把相关的`/tools`全部改成`$ROOT/tools`，其中`$ROOT`是我设的变量。但最后编译完 glibc 之后没有能通过校验。~~
~~当时的错误是这样：~~
~~`.../ld: cannot find $ROOT/tools/lib/libc.so.6 inside $LFS`~~
~~（稍微修改了一下）总之路径有点问题啦。重来一遍。~~
其实，书里也说了：
>  The created symlink enables the toolchain to be compiled so that it always refers to /tools, meaning that the compiler, assembler, and linker will work both in Chapter 5 (when we are still using some tools from the host) and in the next (when we are “chrooted” to the LFS partition). 
基本上在没有进一步的理解时还是得跟着教程走。

### 搭建临时编译环境

#### 注意事项
书里有一段话需要注意，说的是每一小节的编译，除非特别说明，默认读者都重新做了一次一下动作：
1. 解压相应的压缩文件：`$ tar -xf __压缩文件__`
2. 进入压缩文件解压出来的目录：`$ cd __解压出来的目录__`

并且，在进行完一小节的编译后，默认读者应该将本小节解压出来的目录删除，即：
3. `$ cd ..`
4. `$ rm -Rf __解压出来的目录__`

例如在开始的 GCC Pass 1 编译完之后，如果把文件夹留到 Pass 2 继续用，就会出现问题，因为 Pass 2 的配置已经不同了。避免这种情况最简单的方法就是删掉重来。

##### 关于补丁（patch）
之前下载的除了源码压缩包外，还有补丁（`.patch`文件)。需要打补丁的时候文中会有说明，只要按着原文一步一步走下来，就不用担心自己忘了做什么。（漏了倒有可能 :P）

#### zsh 下可能需要的改动
编译的时候，书里有一个提示说在 bash 下可以用`time { ../configure ... && .. && make install; }`来计时。
在 zsh 下不行，要稍微改动一下变成`time ( ../configure ... && ... && make install )`，也就是把大括号改为括号。
（我稍微记录一下，我编译 binutils 的编译时间：`170.20s user 23.73s system 278% cpu 1:09.72 total`（使用了`MAKEFLAGS="-j 4"`））
