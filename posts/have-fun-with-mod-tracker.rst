.. title: MOD tracker 游玩（？）
.. slug: have-fun-with-mod-tracker
.. date: 2021-01-01 18:31:24 UTC+08:00
.. tags: 折腾, 音乐
.. category: 工具
.. link: 
.. description: 
.. type: text
.. license: ccbysa
.. license_mod: 部分来源: https://wiki.openmpt.org/Tutorial:_Getting_Started#Introduction

最开始是因为 absturztaube 写的 `pleroma 的 mod`_ 了解到 `MOD`_ 以及 `tracker`_ 这种东西的。

    Module file (MOD music, tracker music) is a family of music file formats originating from the MOD file format on Amiga systems used in the late 1980s.
    --Module file, Wikipedia

大致说来就是一种比较古来的音乐文件形式，而 tracker 是用来创作这样的音乐的。

似乎还挺有趣的样子，后来有空了就去下了个 tracker `OpenMPT`_ 来玩一玩。

.. _`pleroma 的 mod`: https://git.pleroma.social/absturztaube/pleroma-mod-tracker
.. _`MOD`: https://en.wikipedia.org/wiki/Module_file
.. _`tracker`: https://en.wikipedia.org/wiki/Music_tracker
.. _`OpenMPT`: https://openmpt.org/

.. TEASER_END

##################
OpenMPT 使用
##################

安装？
==================

`OpenMPT`_ 官网是只有 Windows 版本的，但是 ``.exe`` 文件下载页面明显写了 ``Windows xx / Wine 1.x`` 所以应该是专门针对 Linux / MacOS 的 wine 测试过的了。

其实在 AUR 下也直接有 OpenMPT 的包，Manjaro 下可以直接 ``pamac build openmpt`` 安装。

没有用过 wine 的话可能字体大小有点不正常，用 ``winecfg`` 命令进入 wine 设置后到 `Graphics` 栏的 `Screen resolution` 把 dpi 调大一点就可以了。

使用！
==================

OpenMPT 提供了 `新手教程`_ ，可以说是非常有用，这里把一些要点摘录下来。

MOD 音乐的组成
------------------

一些示例音乐
""""""""""""""""""

https://modarchive.org
https://modarchive.org/module.php?60750

OpenMPT 自带有一些示例音乐。菜单栏的 `Help -> Example Modules`

.. _`新手教程`: https://wiki.openmpt.org/Tutorial:_Getting_Started
