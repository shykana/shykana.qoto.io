.. title: Org Mode 笔记
.. slug: org-mode-notes
.. date: 2021-03-29 08:58:08 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Org mode 是好文明，但是我每次都是学到一半就停下来了。现在做点笔记。

########
 快捷键
########

- ``C-c C-t`` : `TODO` 相关，在普通条目与 ``TODO`` 与 ``DONE`` 状态中切换， ``DONE`` 时会自动添加上 ``CLOSED`` 的时间；
- ``M-Shift-RET`` : 在下方添加同级的 ``TODO`` 条项，添加后在进行其它编辑之前可以按 ``TAB`` 调整条目级别；
- ``C-c C-o`` : 打开超链接；
- ``C-c C-l`` : 插入链接，可以手动输入，也可以预先在另外一个文件里通过 ``org-store-link`` 保存一个指向该文件该行的连接，再回到 org 文件里 ``C-c C-l`` ； ``org-store-link`` 默认没有快捷键，可以在配置文件里配置一个（例中为 ``C-c l`` ）：

  .. code:: lisp

      (define-key global-map "\C-cl" 'org-store-link)

- ``Shift-TAB`` : 缩起所有项目至一级标题，或展示所有标题但缩起内容，或展示所有；

##########
 文件格式
##########

- ``[[link][description]]`` : 超链接的格式；显示出来只会有下划线的 ``description`` ，但是可以把光标放到链接最后，按删除键，整个 ``[[link][description]]`` 就会显示出来；
- ``* title`` : 条目/标题，二级、三级等的条目分别 ``** title`` 和 ``*** title`` 以此类推；
- ``** TODO something to do`` : 待做事项，还没做，这是个标题所以可以有一级二级等的分别（按星号的多少）；
- ``*** DONE something done`` : 待做事项（？，已做事项）；一般是：

  .. code::

      *** DONE something done
          CLOSED: [2021-03-29 Mon 14:03]

- 普通文本 : 就随便起一段放着就行；
