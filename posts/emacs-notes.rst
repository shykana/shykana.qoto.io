.. title: Emacs Notes
.. slug: emacs-notes
.. date: 2021-06-11 14:43:54 UTC+08:00
.. updated: 2021-07-30 18:59:53 UTC+08:00
.. tags: emacs
.. category: software
.. link: 
.. description: 
.. type: text

随便记录一下自己的一些 emacs 配置。

系统是 linux，所以 windows 不一定适用。

.. TEASER_END

##############
Emacs 输入中文
##############

请使用 Emacs 28 及以上版本。中文输入问题似乎已经在新版本中解决。请见 `官方的记录`_ 。

当前（2021.07） Arch 最新版本仍为 emacs 27，若想要使用 28 版本的除了自行编译以外，还可以使用 `ArchLinuxCN`_ 源安装 `emacs-git` 包。

.. _`ArchLinuxCN`: https://www.archlinuxcn.org/archlinux-cn-repo-and-mirror/

.. _`官方的记录`: https://debbugs.gnu.org/cgi/bugreport.cgi?bug=10867

##################
按中文字词移动光标
##################

Emacs 的 :kbd:`Alt-f` 和 :kbd:`Alt-b` 是很常见的移动光标的快捷键，分别是跳过一个词以及跳回上一个词的开始。但是这一点对于中文其实是不适用的——emacs 会把连在一起的中文字符全部看作一个词。利用一些分词手法我们可以让 emacs 也能够顺利找到中文的词的开头和结尾。

可见这里 https://github.com/kanglmf/emacs-chinese-word-segmentation

之后有空我再试着转述一遍。

#################################
把 emacs 弄成开机启动以及后台常驻
#################################

开启了上面说的中文分词后你大概会发现 emacs 的每次启动都卡顿一两秒。这不方便，我们可以让 emacs 开机启动并后台常驻，让分词不需要每次都初始化。

.. code::

    [Unit]
    Description=Emacs text editor
    Documentation=info:emacs man:emacs(1) https://gnu.org/software/emacs/

    [Service]
    Type=notify
    ExecStart=/usr/bin/emacs --fg-daemon
    ExecStop=/usr/bin/emacsclient --eval "(kill-emacs)"
    Restart=on-failure

    [Install]
    WantedBy=default.target

将上面的配置保存到 `~/.config/systemd/user/emacs-zh.service` 里，命令行开启：

.. code:: console

   $ systemctl --user daemon-reload
   $ systemctl --user start emacs-zh.service
   $ systemctl --user enable emacs-zh.service

再到 `.bashrc` 或者是 `.zshrc` 或者是你自己用的 shell 的配置文件里配置平时的 emacs 使用后台的 emacs，添加下面一行：

.. code:: shell

    alias emacs='LC_CTYPE=zh_CN.UTF-8 emacsclient -c'

如果有什么启动器或者快捷方式之类的改过来：（如果只想要复制的话下图那句是 ``env LC_CTYPE=zh_CN.UTF-8 emacsclient -c %F`` ）

.. image:: /images/emacs-launcher.png

Emacsclient 固定初始窗口位置大小
##################################

来自 https://stackoverflow.com/a/11008029 以及 http://emacs.1067599.n8.nabble.com/Window-too-big-td370995.html 。这部分 license 是 `CC BY-SA 3.0`_ 。

.. _`CC BY-SA 3.0`: https://creativecommons.org/licenses/by-sa/3.0/

.. code:: lisp

    (add-hook 'before-make-frame-hook
              #'(lambda ()
                  (add-to-list 'default-frame-alist '(user-position . t))
                  (add-to-list 'default-frame-alist '(user-size     . t))
                  (add-to-list 'default-frame-alist '(left   . 0))
                  (add-to-list 'default-frame-alist '(top    . 0))
                  (add-to-list 'default-frame-alist '(height . 38))
                  (add-to-list 'default-frame-alist '(width  . 96))))

###########################
Emacs 打开 SSH 另一头的文件
###########################

见 https://stackoverflow.com/a/20624538

简单来说就是 :kbd:`Ctrl+x` :kbd:`Ctrl+f` 打开的文件名可以是 SSH 另一头的文件，语法是：

.. code::

    /ssh:username@hostnameORip#port:/filepath

例如：

.. code::

    /ssh:root@127.0.0.1#22:/etc/resolv.conf
    /ssh:root@127.0.0.1:/etc/resolv.conf

#####
Modes
#####

Minor Mode
##########

wc-mode
=======

字数统计，在下方的 mode line 显示字数、词数等的统计。

请见 https://github.com/bnbeckwith/wc-mode

安装方法可以直接 :kbd:`M-x` ``package-install wc-mode`` 。

用 :kbd:`M-x` ``customize`` 查找 ``wc-mode`` 之后可以按网页里的 `Modline string` 来定制显示格式。因为没有对中文的优化，对于中文一般可以设置为 ``"WC[%tw/%tc]"`` ，显示效果是 `WC[词数/字数]` 。

############
输入特殊字符
############

可以 :kbd:`M-x` ``set-input-method TeX`` ，设置了之后可以通过 TeX 的方法输入一些对应的 Unicode 字符。例如，（一口气）输入 ``\rightarrow`` 输入最后一个 ``w`` 之后整个会被替换成 ``→`` ，但是如果输入到一半发现打错了然后删了字符或是移动了光标，那就输入失败了。必须一口气连续不出错地输入完整的 TeX 命令才能成功替换。

并不是所有的 TeX 命令都有对应。

################
一些快捷（？）键
################

- :kbd:`C-u` ``4`` :kbd:`C-x` :kbd:`Tab` : 整体缩进选中部分 4 个空格，当然 4 可以改成其它数字（负数亦可）。
