---
title: TunnelBroker的问题？
category: 服务器
tags:
  - 笔记
id: '118'
date: 2019-12-21 13:36:43
updated: 2019-12-21 13:36:43
---

之前用 TunnelBroker 给 VPS 弄了个 IPv6 地址，毕竟教育网可以走 IPv6，挺方便的还省流量。但是有时隔段时间没有用 IPv6 来访问网站后，就直接连不上网站了，ping 也 ping 不通。不知道是不是 TunnelBroker 会自动会把没流量的地址冻结掉。 走 IPv4 登上 SSH 用服务器 ping 了一下其他的 IPv6 地址似乎就可以了。。。现在总之先设了个 cron 隔段时间就 ping 一下，看看怎么样。
