---
title: 搭建此 WordPress 博客的备忘录
category: 服务器
tags:
  - WordPress
id: '5'
date: 2019-08-03 22:26:37
updated: 2019-08-03 22:26:37
---

终于下定决心买个 VPS 搭个博客了. 虽然起因是 OneNote 用不了, 想自己搭个笔记服务器就是了. 把主要过程记录下来.

#### 选购 VPS

如果想用下文的 TCP BBR 算法加速网络的话, 基本要选 KVM, OpenVZ 没法升级内核. 本人没钱, 所以用的是相对便宜的 [VirMach](https://billing.virmach.com/aff.php?aff=8238). 由于国内网络有时不好, 不论什么服务器, 可能最好先测试一下各个数据中心的网络状况, 英文叫做 _looking glass_ ([VirMach 的](https://billing.virmach.com/knowledgebase.php?action=displayarticle&id=11)), 多线程 100M 测试一下, ping 的话看看丢包率就行了吧.

#### ssh 安全性

创建新的用户, 不使用默认的 root 帐号:

    # # 默认遵循 Arch Wiki 的规范, # 开头的行使用 root 权限
    # useradd -m -s bash -G sudo <name> # 添加正常权限的用户
    # passwd <name> # 设置密码
    # visudo
    # # 允许 sudo 组使用 sudo, 取消注释或者添加一行 "%sudo ALL=(ALL) ALL"
    # vim /etc/ssh/sshd_config # vim 换成 nano, vi 都行
    # # PermitRootLogin no
    # # AllowUsers <name>
    # # 以下可选
    # # Port <喜欢的端口> # 以后用 ssh 后面加个 "-p <端口>"

换用 SSH Key, 不用密码登陆  
(其实还是有个密码的, 要记住生成 SSH Key 的密码, 但是简单点(或者留空)安全性也一样)

    $ # 以下为在客户端 (自己电脑) 操作
    $ ssh-keygen -t ed25519 # 个人喜新厌旧, 其实 ed25519 换成 rsa 什么的也没什么问题
    $ scp -P <端口> ~/.ssh/id_ed25519.pub <name>@<服务器地址>:
    $ # 以下在服务器
    $ mkdir ~/.ssh
    $ chmod 700 ~/.ssh/
    $ cat id_ed25519.pub >> .ssh/authorized_keys
    $ rm id_ed25519.pub
    $ chmod 600 .ssh/authorized_keys
    $ sudo vim /etc/ssh/sshd_config
    $ # 加上 "PubkeyAuthentication yes"
    $ # 最后加上一行 "PasswordAuthentication no" <- 在这之前确认好可以用 Pubkey 登陆先

顺便一提看到 Arch Wiki 上还有启用两步验证的方法, 这里就懒的弄了. 有兴趣的看[这里](https://wiki.archlinux.org/index.php/OpenSSH#Two-factor_authentication_and_public_keys), 虽然里面提到了 Google, 但是理论上应该是不用访问 Google 的, 随便的两步验证软件都可以.

#### BBR 算法开启 (需要KVM)

不同网络加速效果不同啦. 自己上网查介绍.  
首先, 内核版本要 4.9 以上 (`uname -a` 查看版本). 基本上可以 GitHub 上找个自动安装脚本. 以下是手工开启:

    # 升级内核不详细说, 自己查相应发行版的方法. 如果是直接安装软件包的话, 旧的发行版可能会不兼容最新的内核包(如我的 Ubuntu 16 装 5.2 内核会报缺少 libssl1.1, 换成旧一点的如 4.20 就行); 麻烦一点可以自己编译, 可惜太懒.
    # 升级内核记得更新 grub.cfg, 重启
    # # 临时开启, 重启失效
    # modprobe tcp_bbr
    # sysctl net.ipv4.tcp_congestion_control=bbr
    # sysctl net.core.default_qdisc=fq
    # # 开机自动开启
    # echo "tcp_bbr" >> /etc/modules-load.d/modules.conf
    # echo "net.core.default_qdisc = fq" >> /etc/sysctl.conf
    # echo "net.ipv4.tcp_congestion_control = bbr" >> /etc/sysctl.conf

#### WordPress 安装

我的选择是 Nginx + PHP 7.3 + MariaDB + WordPress. 方法各个发行版不同, 要自己上网查, 提供一个 Ubuntu 的.

    # add-apt-repository ppa:ondrej/nginx
    # add-apt-repository ppa:ondrej/php
    # apt update
    # apt install nginx php7.3-fpm php7.3-mysql php7.3-curl php7.3-gd php7.3-mbstring php7.3-xml php7.3-xmlrpc php7.3-zip php7.3-opcache php7.3-bcmath php7.3-imagick mariadb-server

MariaDB

    # 跟着提示走行了
    # mysql_install_db # 这个可能没用啦
    # mysql_secure_installation
    # mysql --user=root --password="密码这里"
    MariaDB> CREATE DATABASE wordpress;
    MariaDB> GRANT ALL PRIVILEGES ON wordpress.* TO "wp-user"@"localhost" IDENTIFIED BY "choose_db_password";
    MariaDB> FLUSH PRIVILEGES;
    MariaDB> EXIT

Nginx(比较新版的 Nginx 默认配置都挺好的了, 略)

PHP 7.3

    # # *** /etc/php/7.3/fpm/php.ini ***
    # # 没有什么好说的吧, 基本默认配置就挺好了, 可以上网再查一查
    # # 唯一 php.ini 里 "opcache.enable=1" 前的分号去掉取消注释可以提升性能.

    # # *** /etc/nginx/sites-enabled/default ***
    # # 这里的 php7.3 不同 PHP 版本不同
    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.3-fpm.sock;
    }
    

WordPress

    # # *** /etc/nginx/sites-enabled/default ***
    # # 先在这里认准 root 后面的网站目录, 一般是 "/var/www/html" 之类的.
    # # 下面这里因为我不想把 WordPress 放网站根目录, 也没想好根目录还能放啥, 就直接跳转了.
    location = / {
        rewrite / /wordpress/;
    }

    # cd /var/www/html # 按照相应的 root 目录
    # # 中文英文选个, 其实仪表盘里设置也可以更改语言
    # wget https://cn.wordpress.org/latest-zh_CN.tar.gz # 中文版
    # wget https://wordpress.org/latest.tar.gz # 英文版
    # tar -zxf latest.tar.gz

    # # 其实要注意的还有权限问题
    # useradd -M nginx
    # # 更改 nginx.conf, 改为 "user nginx;"
    # # 更改 php-fpm.conf, 改为 "listen.owner = nginx" 和 "listen.group = nginx"
    # chown -R nginx:nginx /var/www

开启各个服务

    # systemctl start nginx
    # systemctl start php7.3-fpm
    # systemctl start mysql
    # systemctl enable nginx
    # systemctl enable php7.3-fpm
    # systemctl enable mysql

然后到网站上去初始化 WordPress 就行了.

#### 绑定域名

一般我会直接把没有 www 前缀的跳转到 www 的域名. 如果想禁止其他方法访问的话, 注意要先把 WordPress 设置里的地址更改成相应的域名.

    # cp /etc/nginx/sites-enabled/default /etc/nginx/sites-enabled/domain
    # vim /etc/nginx/sites-enabled/default
    # # 改成只剩这些, 禁止 ip 访问, 或者假 DNS
    server {
    	listen 80 default_server;
    	listen [::]:80 default_server;
    
    	server_name _;
    	location / {
    		deny all;
    	}
    }
    # vim /etc/nginx/sites-enabled/domain
    # # 加入
    server {
            listen 80;
            server_name 没有www的域名(如liberty.gensokyoradio.moe);
            location / {
                    rewrite ^/(.*)$ http://www.liberty.gensokyoradio.moe/$1 permanent;
            }
    }
    # # server_name _; 改成
    server_name www.liberty.gensokyoradio.moe;

#### 参考链接

1.  [https://wiki.archlinux.org/index.php/OpenSSH](https://wiki.archlinux.org/index.php/OpenSSH)
2.  [https://wiki.archlinux.org/index.php/Nginx](https://wiki.archlinux.org/index.php/Nginx)
3.  [https://wiki.archlinux.org/index.php/Wordpress](https://wiki.archlinux.org/index.php/Wordpress)

话说孤陋寡闻 WordPress 编辑器怎么变成这个样子了?
