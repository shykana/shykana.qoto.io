.. title: Awareness
.. slug: awareness
.. date: 2021-05-11 12:03:30 UTC+08:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

自勉，记录一些对自己的要求吧。

==============
发文、对话用语
==============

这里也尝试着根据 `Anti-delta approach`_ 给出一些理由。

.. _`Anti-delta approach`: /posts/changemyview-zh-antidelta

避免使用以下的所谓流行句式：
1. 热/温/凉/冷知识：Use hostile sarcasm
2. 不是吧不是吧/不是吧不是吧：Insult their intelligence
3. PTSD

一些格式
========

1. 引用/针对别人的发言的发文给出链接，转图给出链接（以及 license）
2. 少用截图
3. 对于较难 fackcheck 并且没有来源的言论，要么自己 fackcheck 要么不转
4. （Fedi）在不影响别人的情况下，尽量使用 Public 或 Unlisted 的可见范围发文，包括对别人的回复

==========
其它的一些
==========

1. 不要比烂不要比烂不要比烂，没有任何意义
2. 不要否定别人的经历，不要比较人的苦难
3. 骂人就骂，不要在装出来的人模样子或是所谓逻辑里面再夹藏了

