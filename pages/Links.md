---
title: 一些链接！
date: 2020-07-30 13:21:57
---

# 友链友链！

<style>
div.friends {
    display: flex;
    flex-wrap: wrap;
}

div.friends div {
    border: 0.2em var(--primary) solid;
    padding: 0.5em;
    width: 7em;
}

div.friends img {
    width: 5em;
    display: block !important;
    image-rendering: crisp-edges;
}

div.friends a {
    line-height: 1.4;
    display: inline;
}
</style>

<div class="friends">

<div>
<img alt="Konata" src="/images/friends/konata.png"></img>
此方的：
<a href="https://blog.konata.co/">此方方有限公司</a>
</div>

<div>
<img alt="Michaelis" src="/images/friends/michaelis.png"></img>
米茶 Michaelis：
<a href="https://mindpalace.michaelis.website/">Michaelis's Mindpalace</a>
</div>

<div>
<img alt="Suica" src="/images/friends/suica.png"></img>
Suica：
<a href="https://suicablog.cobaltkiss.blue/">SuicaBlue's blog</a>
</div>

</div>

# 以及某些链接

[历年诺贝尔奖女性获奖者](/pages/girls/)

## 备忘链接

LaTeX 创建流程图等的图形化工具： https://q.uiver.app/
