---
title: About
layout: About
---
Unknown yet.

## 主题！

现在的博客使用 [Nikola](https://getnikola.com/) 搭建！主题改自 古川政良 matrixk 桑的 [Bitcron主题「莉莉」](https://github.com/matrixk/bitcron-theme-lily/)。
移植后的主题可以在 [https://git.qoto.org/shykana/shykana.qoto.io/-/tree/master/themes/lily](https://git.qoto.org/shykana/shykana.qoto.io/-/tree/master/themes/lily) 查看。

> 原主题的使用声明：主题的所有模板及其源代码，一律禁止肖战团队及其粉丝使用。

## 我？

Fediverse Account: <del>[@ShyKana@wuppo.allowed.org](https://wuppo.allowed.org/ShyKana)</del> 实例 down 掉了呢……现在在 [@kana@f.iroiro.party](https://f.iroiro.party/profile/kana/profile)

GitLab (Qoto): [https://git.qoto.org/shykana/](https://git.qoto.org/shykana/)

TODO: [TODO](/pages/todo/)

自勉: [Awareness](/pages/awareness/)

