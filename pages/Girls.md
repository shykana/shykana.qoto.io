---
layout: girls
title: girls
date: 2020-07-30 13:22:08
---

## 历年诺贝尔奖女性获奖者

（抓取于 2020-07-30）

<span title="大佬大佬">（按<a href="/posts/girls-page-setup/">JS抓取</a>延迟随机排序）</span>

<style>
.nobel {
    display: flex;
    flex-wrap: wrap;
    font-family: sans-serif;
}

.nobel li {
    display: inline-flex;
    flex-direction: column;
    width: 8rem;
    margin: 0.3rem;
    padding-top: 1rem;
    text-align: center;
    word-wrap: break-word;
}

.nobel pre {
    z-index: -1;
    display: none;
    opacity: 0;
    background-color: #ceb1e1d0;
    transition: all 0.5s;
    font-size: small;
    display: block;
    position: fixed;
    left: 20%;
    top: 60%;
    width: 60%;
    word-wrap: break-word;
    white-space: pre-wrap;
    border-radius: 1em;
    box-shadow: 1px 1px 0.5rem grey;
}

.nobel a:hover ~ pre {
    z-index: 9999;
    opacity: 1;
}

.nobel .prize {
    font-size: small;
}

.nobel li img {
    border-radius: 100%;
    object-fit: cover;
    padding: 0.2rem;
    background-color: white;
    box-shadow: 0 0 1rem #8e71c180 !important;
    width: 6rem;
    height: 6rem;
    object-position: center top;
}

.nobel li:nth-child(odd) {
    background-color: #c0c0c040;
}
</style>

<ul class="nobel">
  <li><a href="https://www.nobelprize.org/prizes/chemistry/2018/arnold/facts/">
      <img src="https://www.nobelprize.org/images/arnold-57918-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Frances H. Arnold</span>
    <pre>
      The Nobel Prize in Chemistry 2018
      Frances H. Arnold
      “for the directed evolution of enzymes”</pre>
    <span class="prize">The Nobel Prize in Chemistry 2018</span>
  </li>
  <li><a href="https://www.nobelprize.org/prizes/peace/2018/murad/facts/">
      <img src="https://www.nobelprize.org/images/murad-57913-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Nadia Murad</span>
    <pre>
      The Nobel Peace Prize 2018
      Nadia Murad
      “for their efforts to end the use of sexual violence as a weapon of war and armed conflict”</pre>
    <span class="prize">The Nobel Peace Prize 2018</span>
  </li>
  <li><a href="https://www.nobelprize.org/prizes/physics/2018/strickland/facts/">
      <img src="https://www.nobelprize.org/images/strickland-57920-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Donna Strickland</span>
    <pre>
      The Nobel Prize in Physics 2018
      Donna Strickland
      “for groundbreaking inventions in the field of laser physics”
      “for their method of generating high-intensity, ultra-short optical pulses.”</pre>
    <span class="prize">The Nobel Prize in Physics 2018</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1966/sachs-facts.html">
      <img src="https://www.nobelprize.org/images/sachs-13868-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Nelly Sachs</span>
    <pre>
      The Nobel Prize in Literature 1966
      Nelly Sachs
      “for her outstanding lyrical and dramatic writing, which interprets Israel’s destiny with touching strength”</pre>
    <span class="prize">The Nobel Prize in Literature 1966</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1991/gordimer-facts.html">
      <img src="https://www.nobelprize.org/images/gordimer-13432-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Nadine Gordimer</span>
    <pre>
      The Nobel Prize in Literature 1991
      Nadine Gordimer
      “who through her magnificent epic writing has – in the words of Alfred Nobel – been of very great benefit to humanity”</pre>
    <span class="prize">The Nobel Prize in Literature 1991</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1945/mistral-facts.html">
      <img src="https://www.nobelprize.org/images/mistral-13035-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Gabriela Mistral</span>
    <pre>
      The Nobel Prize in Literature 1945
      Gabriela Mistral
      “for her lyric poetry which, inspired by powerful emotions, has made her name a symbol of the idealistic aspirations of the entire Latin American world”</pre>
    <span class="prize">The Nobel Prize in Literature 1945</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2011/karman-facts.html">
      <img src="https://www.nobelprize.org/images/karman-15166-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Tawakkol Karman</span>
    <pre>
      The Nobel Peace Prize 2011
      Tawakkol Karman
      “for their non-violent struggle for the safety of women and for women’s rights to full participation in peace-building work”</pre>
    <span class="prize">The Nobel Peace Prize 2011</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2014/yousafzai-facts.html">
      <img src="https://www.nobelprize.org/images/yousafzai-15198-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Malala Yousafzai</span>
    <pre>
      The Nobel Peace Prize 2014
      Malala Yousafzai
      “for their struggle against the suppression of children and young people and for the right of all children to education”</pre>
    <span class="prize">The Nobel Peace Prize 2014</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2004/maathai-facts.html">
      <img src="https://www.nobelprize.org/images/maathai-13700-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Wangari Muta Maathai</span>
    <pre>
      The Nobel Peace Prize 2004
      Wangari Muta Maathai
      “for her contribution to sustainable development, democracy and peace”</pre>
    <span class="prize">The Nobel Peace Prize 2004</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2003/ebadi-facts.html">
      <img src="https://www.nobelprize.org/images/ebadi-13688-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Shirin Ebadi</span>
    <pre>
      The Nobel Peace Prize 2003
      Shirin Ebadi
      “for her efforts for democracy and human rights. She has focused especially on the struggle for the rights of women and children”</pre>
    <span class="prize">The Nobel Peace Prize 2003</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1997/williams-facts.html">
      <img src="https://www.nobelprize.org/images/williams-13500-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Jody Williams</span>
    <pre>
      The Nobel Peace Prize 1997
      Jody Williams
      “for their work for the banning and clearing of anti-personnel mines”</pre>
    <span class="prize">The Nobel Peace Prize 1997</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1992/tum-facts.html">
      <img src="https://www.nobelprize.org/images/tum-13442-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Rigoberta Menchú Tum</span>
    <pre>
      The Nobel Peace Prize 1992
      Rigoberta Menchú Tum
      “in recognition of her work for social justice and ethno-cultural reconciliation based on respect for the rights of indigenous peoples”</pre>
    <span class="prize">The Nobel Peace Prize 1992</span>
  </li>
  <li><a href="https://www.nobelprize.org/prizes/literature/2018/tokarczuk/facts/">
      <img src="https://www.nobelprize.org/images/tokarczuk-100427-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Olga Tokarczuk</span>
    <pre>
      The Nobel Prize in Literature 2018
      Olga Tokarczuk
      “for a narrative imagination that with encyclopedic passion represents the crossing of boundaries as a form of life”</pre>
    <span class="prize">The Nobel Prize in Literature 2018</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1976/williams-facts.html">
      <img src="https://www.nobelprize.org/images/williams-13285-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Betty Williams</span>
    <pre>
      The Nobel Peace Prize 1976
      Betty Williams</pre>
    <span class="prize">The Nobel Peace Prize 1976</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1976/corrigan-facts.html">
      <img src="https://www.nobelprize.org/images/corrigan-13284-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Mairead Corrigan</span>
    <pre>
      The Nobel Peace Prize 1976
      Mairead Corrigan</pre>
    <span class="prize">The Nobel Peace Prize 1976</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1979/teresa-facts.html">
      <img src="https://www.nobelprize.org/images/teresa-13873-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Mother Teresa</span>
    <pre>
      The Nobel Peace Prize 1979
      Mother Teresa</pre>
    <span class="prize">The Nobel Peace Prize 1979</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1946/balch-facts.html">
      <img src="https://www.nobelprize.org/images/balch-13046-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Emily Greene Balch</span>
    <pre>
      The Nobel Peace Prize 1946
      Emily Greene Balch</pre>
    <span class="prize">The Nobel Peace Prize 1946</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1991/kyi-facts.html">
      <img src="https://www.nobelprize.org/images/kyi-13435-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Aung San Suu Kyi</span>
    <pre>
      The Nobel Peace Prize 1991
      Aung San Suu Kyi
      “for her non-violent struggle for democracy and human rights”</pre>
    <span class="prize">The Nobel Peace Prize 1991</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1982/myrdal-facts.html">
      <img src="https://www.nobelprize.org/images/myrdal-13349-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Alva Myrdal</span>
    <pre>
      The Nobel Peace Prize 1982
      Alva Myrdal</pre>
    <span class="prize">The Nobel Peace Prize 1982</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1938/buck-facts.html">
      <img src="https://www.nobelprize.org/images/buck-13015-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Pearl Buck</span>
    <pre>
      The Nobel Prize in Literature 1938
      Pearl Buck
      “for her rich and truly epic descriptions of peasant life in China and for her biographical masterpieces”</pre>
    <span class="prize">The Nobel Prize in Literature 1938</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2011/johnson_sirleaf-facts.html">
      <img src="https://www.nobelprize.org/images/johnson_sirleaf-15167-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Ellen Johnson Sirleaf</span>
    <pre>
      The Nobel Peace Prize 2011
      Ellen Johnson Sirleaf
      “for their non-violent struggle for the safety of women and for women’s rights to full participation in peace-building work”</pre>
    <span class="prize">The Nobel Peace Prize 2011</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/2011/gbowee-facts.html">
      <img src="https://www.nobelprize.org/images/gbowee-15168-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Leymah Gbowee</span>
    <pre>
      The Nobel Peace Prize 2011
      Leymah Gbowee
      “for their non-violent struggle for the safety of women and for women’s rights to full participation in peace-building work”</pre>
    <span class="prize">The Nobel Peace Prize 2011</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1931/addams-facts.html">
      <img src="https://www.nobelprize.org/images/addams-12977-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Jane Addams</span>
    <pre>
      The Nobel Peace Prize 1931
      Jane Addams</pre>
    <span class="prize">The Nobel Peace Prize 1931</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1928/undset-facts.html">
      <img src="https://www.nobelprize.org/images/undset-12958-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Sigrid Undset</span>
    <pre>
      The Nobel Prize in Literature 1928
      Sigrid Undset
      “principally for her powerful descriptions of Northern life during the Middle Ages”</pre>
    <span class="prize">The Nobel Prize in Literature 1928</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/peace/laureates/1905/suttner-facts.html">
      <img src="https://www.nobelprize.org/images/suttner-12846-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Baroness Bertha Sophie Felicita von Suttner, née Countess Kinsky von Chinic
      und Tettau</span>
    <pre>
      The Nobel Peace Prize 1905
      Baroness Bertha Sophie Felicita von Suttner, née Countess Kinsky von Chinic und Tettau</pre>
    <span class="prize">The Nobel Peace Prize 1905</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/economic-sciences/laureates/2009/ostrom-facts.html">
      <img src="https://www.nobelprize.org/images/ostrom-15149-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Elinor Ostrom</span>
    <pre>
      The Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel 2009
      Elinor Ostrom
      “for her analysis of economic governance, especially the commons”</pre>
    <span class="prize">The Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel
      2009</span>
  </li>
  <li><a href="https://www.nobelprize.org/prizes/economics/2019/duflo/facts/">
      <img src="https://www.nobelprize.org/images/duflo-99998-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Esther Duflo</span>
    <pre>
      The Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel 2019
      Esther Duflo
      “for their experimental approach to alleviating global poverty”</pre>
    <span class="prize">The Sveriges Riksbank Prize in Economic Sciences in Memory of Alfred Nobel
      2019</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/chemistry/laureates/1911/marie-curie-facts.html">
      <img src="https://www.nobelprize.org/images/marie-curie-12879-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Marie Curie, née Sklodowska</span>
    <pre>
      The Nobel Prize in Chemistry 1911
      Marie Curie, née Sklodowska
      “in recognition of her services to the advancement of chemistry by the discovery of the elements radium and polonium, by the isolation of radium and the study of the nature and compounds of this remarkable element”</pre>
    <span class="prize">The Nobel Prize in Chemistry 1911</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2009/blackburn-facts.html">
      <img src="https://www.nobelprize.org/images/blackburn-15146-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Elizabeth H. Blackburn</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2009
      Elizabeth H. Blackburn
      “for the discovery of how chromosomes are protected by telomeres and the enzyme telomerase”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2009</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2014/may-britt-moser-facts.html">
      <img src="https://www.nobelprize.org/images/may-britt-moser-15191-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">May-Britt Moser</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2014
      May-Britt Moser
      “for their discoveries of cells that constitute a positioning system in the brain”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2014</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2015/tu-facts.html">
      <img src="https://www.nobelprize.org/images/tu-13648-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Tu Youyou</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2015
      Tu Youyou
      “for her discoveries concerning a novel therapy against Malaria”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2015</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2009/greider-facts.html">
      <img src="https://www.nobelprize.org/images/greider-15145-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Carol W. Greider</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2009
      Carol W. Greider
      “for the discovery of how chromosomes are protected by telomeres and the enzyme telomerase”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2009</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2004/buck-facts.html">
      <img src="https://www.nobelprize.org/images/buck-13699-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Linda B. Buck</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2004
      Linda B. Buck
      “for their discoveries of odorant receptors and the organization of the olfactory system”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2004</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1983/mcclintock-facts.html">
      <img src="https://www.nobelprize.org/images/mcclintock-13355-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Barbara McClintock</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1983
      Barbara McClintock
      “for her discovery of mobile genetic elements”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1983</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1996/szymborska-facts.html">
      <img src="https://www.nobelprize.org/images/szymborska-13484-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Wislawa Szymborska</span>
    <pre>
      The Nobel Prize in Literature 1996
      Wislawa Szymborska
      “for poetry that with ironic precision allows the historical and biological context to come to light in fragments of human reality”</pre>
    <span class="prize">The Nobel Prize in Literature 1996</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1988/elion-facts.html">
      <img src="https://www.nobelprize.org/images/elion-13403-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Gertrude B. Elion</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1988
      Gertrude B. Elion
      “for their discoveries of important principles for drug treatment”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1988</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1993/morrison-facts.html">
      <img src="https://www.nobelprize.org/images/morrison-13448-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Toni Morrison</span>
    <pre>
      The Nobel Prize in Literature 1993
      Toni Morrison
      “who in novels characterized by visionary force and poetic import, gives life to an essential aspect of American reality”</pre>
    <span class="prize">The Nobel Prize in Literature 1993</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/2004/jelinek-facts.html">
      <img src="https://www.nobelprize.org/images/jelinek-13697-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Elfriede Jelinek</span>
    <pre>
      The Nobel Prize in Literature 2004
      Elfriede Jelinek
      “for her musical flow of voices and counter-voices in novels and plays that with extraordinary linguistic zeal reveal the absurdity of society’s clichés and their subjugating power”</pre>
    <span class="prize">The Nobel Prize in Literature 2004</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1995/nusslein-volhard-facts.html">
      <img src="https://www.nobelprize.org/images/nusslein-volhard-13473-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Christiane Nüsslein-Volhard</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1995
      Christiane Nüsslein-Volhard
      “for their discoveries concerning the genetic control of early embryonic development”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1995</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1986/levi-montalcini-facts.html">
      <img src="https://www.nobelprize.org/images/levi-montalcini-13382-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Rita Levi-Montalcini</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1986
      Rita Levi-Montalcini
      “for their discoveries of growth factors”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1986</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1926/deledda-facts.html">
      <img src="https://www.nobelprize.org/images/deledda-12945-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Grazia Deledda</span>
    <pre>
      The Nobel Prize in Literature 1926
      Grazia Deledda
      “for her idealistically inspired writings which with plastic clarity picture the life on her native island and with depth and sympathy deal with human problems in general”</pre>
    <span class="prize">The Nobel Prize in Literature 1926</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/1909/lagerlof-facts.html">
      <img src="https://www.nobelprize.org/images/lagerlof-12868-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Selma Ottilia Lovisa Lagerlöf</span>
    <pre>
      The Nobel Prize in Literature 1909
      Selma Ottilia Lovisa Lagerlöf
      “in appreciation of the lofty idealism, vivid imagination and spiritual perception that characterize her writings”</pre>
    <span class="prize">The Nobel Prize in Literature 1909</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/physics/laureates/1963/mayer-facts.html">
      <img src="https://www.nobelprize.org/images/mayer-13168-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Maria Goeppert Mayer</span>
    <pre>
      The Nobel Prize in Physics 1963
      Maria Goeppert Mayer
      “for their discoveries concerning nuclear shell structure”</pre>
    <span class="prize">The Nobel Prize in Physics 1963</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/chemistry/laureates/1935/joliot-curie-facts.html">
      <img src="https://www.nobelprize.org/images/joliot-curie-12995-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Irène Joliot-Curie</span>
    <pre>
      The Nobel Prize in Chemistry 1935
      Irène Joliot-Curie
      “in recognition of their synthesis of new radioactive elements”</pre>
    <span class="prize">The Nobel Prize in Chemistry 1935</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/chemistry/laureates/1964/hodgkin-facts.html">
      <img src="https://www.nobelprize.org/images/hodgkin-13170-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Dorothy Crowfoot Hodgkin</span>
    <pre>
      The Nobel Prize in Chemistry 1964
      Dorothy Crowfoot Hodgkin
      “for her determinations by X-ray techniques of the structures of important biochemical substances”</pre>
    <span class="prize">The Nobel Prize in Chemistry 1964</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/physics/laureates/1903/marie-curie-facts.html">
      <img src="https://www.nobelprize.org/images/marie-curie-12835-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Marie Curie, née Sklodowska</span>
    <pre>
      The Nobel Prize in Physics 1903
      Marie Curie, née Sklodowska
      “in recognition of the extraordinary services they have rendered by their joint researches on the radiation phenomena discovered by Professor Henri Becquerel”</pre>
    <span class="prize">The Nobel Prize in Physics 1903</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/2008/barre-sinoussi-facts.html">
      <img src="https://www.nobelprize.org/images/barre-sinoussi-13561-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Françoise Barré-Sinoussi</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 2008
      Françoise Barré-Sinoussi
      “for their discovery of human immunodeficiency virus”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 2008</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1947/cori-gt-facts.html">
      <img src="https://www.nobelprize.org/images/cori-gt-13052-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Gerty Theresa Cori, née Radnitz</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1947
      Gerty Theresa Cori, née Radnitz
      “for their discovery of the course of the catalytic conversion of glycogen”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1947</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/2015/alexievich-facts.html">
      <img src="https://www.nobelprize.org/images/alexievich-13645-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Svetlana Alexievich</span>
    <pre>
      The Nobel Prize in Literature 2015
      Svetlana Alexievich
      “for her polyphonic writings, a monument to suffering and courage in our time”</pre>
    <span class="prize">The Nobel Prize in Literature 2015</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/chemistry/laureates/2009/yonath-facts.html">
      <img src="https://www.nobelprize.org/images/yonath-15150-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Ada E. Yonath</span>
    <pre>
      The Nobel Prize in Chemistry 2009
      Ada E. Yonath
      “for studies of the structure and function of the ribosome”</pre>
    <span class="prize">The Nobel Prize in Chemistry 2009</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/medicine/laureates/1977/yalow-facts.html">
      <img src="https://www.nobelprize.org/images/yalow-13294-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Rosalyn Yalow</span>
    <pre>
      The Nobel Prize in Physiology or Medicine 1977
      Rosalyn Yalow
      “for the development of radioimmunoassays of peptide hormones”</pre>
    <span class="prize">The Nobel Prize in Physiology or Medicine 1977</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/2007/lessing-facts.html">
      <img src="https://www.nobelprize.org/images/lessing-15133-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Doris Lessing</span>
    <pre>
      The Nobel Prize in Literature 2007
      Doris Lessing
      “that epicist of the female experience, who with scepticism, fire and visionary power has subjected a divided civilisation to scrutiny”</pre>
    <span class="prize">The Nobel Prize in Literature 2007</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/2013/munro-facts.html">
      <img src="https://www.nobelprize.org/images/munro-15184-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Alice Munro</span>
    <pre>
      The Nobel Prize in Literature 2013
      Alice Munro
      “master of the contemporary short story”</pre>
    <span class="prize">The Nobel Prize in Literature 2013</span>
  </li>
  <li><a href="https://www.nobelprize.org/nobel_prizes/literature/laureates/2009/muller-facts.html">
      <img src="https://www.nobelprize.org/images/muller-15147-portrait-mini-2x.jpg"></img>
    </a>
    <span class="name">Herta Müller</span>
    <pre>
      The Nobel Prize in Literature 2009
      Herta Müller
      “who, with the concentration of poetry and the frankness of prose, depicts the landscape of the dispossessed”</pre>
    <span class="prize">The Nobel Prize in Literature 2009</span>
  </li>
</ul>

以上人物图片版权详见各人物对应页面（点击人物图片打开的页面）。

[Girls Page Setup](/posts/girls-page-setup)
