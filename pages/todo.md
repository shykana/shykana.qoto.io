<!--
.. title: Todo
.. slug: todo
.. date: 2020-12-30 22:16:07 UTC+08:00
.. tags: flag
.. category: 
.. link: 
.. description: 
.. type: text
-->

## TODO

### 学习相关
* 学期结束后不管炸得怎么样，该学扎实的还得会去再学一遍
* 看完量化
* 测度论笔记整理一下？

### Flarum 相关
* flarum-deltabot
* 找找 flarum 的 RSS 插件
* fedirum
* 配置 flarum-redis 的队列

### Fediverse 相关
* Pleroma 中文全文搜索
* Pleroma 安装教程（文字版？）
* 写个 sciencerag 的退稿机器人！
* ipv4.vpnbanzai.eu.org 准备删站跑路（不）换个地方放 bot
* Ebbtide 把 lotide OpenAPI 里的都给实现了

### 其它
* 进一步了解LOGO/POOL的语法，自己实现一下？
* 笔记：https://github.com/toadjaune/pulseaudio-config
